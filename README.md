# BiFFT - a python package for fast estimation of the bispectrum

BiFFT uses Fourier transforms to implement the Dirac-Delta function that enforces a closed triangle of three k-vectors. This allows very fast calculations of the bispectrum. 

Once the C-code associated with the package is compiled and the source folder directed to the location of the C-code, the user can run the code using the python wrapper. 

The binning in each function has been tested over the course of many years and the user can use it out of the box without ever touching the underlying C-code. However, the cylindrical bispectrum calculation is much more sensitive to sample variance and so it's default binning is quite coarse and might need adjusting (and testing) for some datasets.

If you use this code, please cite [Watkinson et al. 2017](https://ui.adsabs.harvard.edu/abs/2017MNRAS.472.2436W/abstract) where this package is described in detail. Please also cite [Scoccimarro 2015](https://ui.adsabs.harvard.edu/abs/2015PhRvD..92h3532S/abstract) and [Sefusatti et al. 2015](https://ui.adsabs.harvard.edu/abs/2016MNRAS.460.3624S/abstract) in which this FFT approach is first discussed.

If you use the cylindrically-averaged bispectrum function please also cite [Watkinson, Trott & Hothi 2020](https://ui.adsabs.harvard.edu/abs/2020arXiv200205992W/abstract)


--------------

## Installation Instructions

#### Update source folder

Adjust SOURCE_FOLDER in bifft/src/bispec_analysis.py to be the path containing bifft/src on your machine

#### Ensure the C-code will run on your machine
Make sure you have the relevant C-compiler and libraries installed, namely a C compiler (e.g. gcc or icc), openmp, gsl, cblas and fftwf (with openmp enabled, you might also need to enable shared depending on your setup).

In the makefile check the paths for the various flags are appropriate for your system. There are compile options for intel compilers, gcc on a Mac OS (where shell name is Darwin as appropriate for at least 10.14 and 10.15, I also recommend installing the standard gcc compiler rather than using Clang on the Mac), and gcc on a generic linux OS. 

You will have to adjust this if you are not using any of these setups and/or the UNAME definition on L1 of the Makefile (it is, by default, setup for a Bash shell). 

```
cd bifft/src/

Make
```

#### pip install

cd to the main folder of BiFFT (the one containing setup.py)
```
pip install . 
```
You should now be able to use all of the functions in bispec_analysis.py using
```
import bifft

output_dict = bifft.thefuntionyouwant()
```
--------------

## BiFFT usage info

bispec_analysis.py contains wrappers for calling various bispectrum and
power spectrum code packaged with BiFFT.

Each one returns a dictionary containing the associated arrays,
all take a complex or real datasets as input. 
There is a description at the top of each function in src/bispec_analysis.py that explains the usage of each function;
you can also look at the test modules in Tests/
to see how they are called and their output used.

Each function also allows one to ignore modes below a particular klos.
This is useful as there is always a klos limit below which most foreground
removal methods will inherently fail (as for very large modes FG and signal
are indistinguishable).

If you make any changes to the code, run pytest from main folder to run the
tests in Test/. You can override the hardcoded test code by setting REGEN_TEST_DATA = True
in the relevant test script if your changes purposely change the codes output.

--------------

## Functions menu

calculate_equi_bipec() -> equilateral bispec as fn of k

calculate_isoc_bipec() -> isoceles bispec for passed k1 as fn of angle (in radians/pi)
						  between k1 and k2 when added (i.e. internal to the triangle)
						  
calculate_general_bipec() -> bispec for passed k1 and k2 as fn of angle (in radians/pi)
							 between k1 and k2 when added (i.e. internal to the triangle)
							 
calculate_cylin_bipec() -> cylindrically-averaged bispectrum for a particular k1perp, k1los and list of k2perp, k2los bins.
						   Nb. This parametrisation is not as stable to sample variance as the rest, the binwidth used in ComputeBS_cylindrically_averaged.c
						   have been set to minimise sample variance using tests with theoretical simulations. In reality your simulation k-resolution
						   might mean that the default is too coarse for your application.
						   If you do adjust this binning (L552-560 of ComputeBS_cylindrically_averaged.c) please test its stability.

calculate_pspec() -> power spectrum as a function of k

noise_err_bispec() -> calculates the bispectrum noise error given the noise is Gaussian
				  and so depends only on the noise power spec.
