#import py21cmfast as p21
import numpy as np
import pickle

#import bispec_analysis as bifft
import bifft

BOX_DIM = 64
BOX_LEN = BOX_DIM*2.0

REGEN_TEST_DATA = False # If you change anything in the code and need to regenerate hard coded stats for testing

TEST_DATA_FNAME = 'Tests/brightness_temp_test_data.pkl'

'''
def test_regenerate_test_data():
    if (REGEN_TEST_DATA):
        UParams = p21.UserParams(HII_DIM=BOX_DIM, BOX_LEN=BOX_LEN)
        CParams = p21.CosmoParams() # Here we are setting to defaults, could equally pass nothing as function does the same internally
        FOptions = p21.FlagOptions(USE_MASS_DEPENDENT_ZETA=False, USE_TS_FLUCT=False) # Also default, but defining to highlight which parameters control this
        AParams = p21.AstroParams(HII_EFF_FACTOR = 30.0, ION_Tvir_MIN = 4.69897) # Note ION_Tvir_MIN is log so here Tvir = 5 x 10^4

        coevals = p21.run_coeval( redshift = 6.32167913284, user_params = UParams, cosmo_params = CParams, astro_params=AParams, flag_options=FOptions, random_seed=12414, write=False)
        size = UParams.HII_DIM * UParams.HII_DIM * UParams.HII_DIM
        dT_box = coevals.brightness_temp
        f = open(TEST_DATA_FNAME , 'wb' )
        pickle.dump( dT_box, f )
        f.close()
    else:
        print("Using hard coded test data for all tests")'''

def test_gen_bispec():

    f = open(TEST_DATA_FNAME, 'rb')
    dT_box = np.array( pickle.load(f) )
    f.close()
    #dT_complex = dT_box.astype(np.complex)

    #assert isinstance(dT_complex[19][19][19], np.complex)

    BS_gen_dict = bifft.calculate_general_bipec(data=dT_box, DIM=[BOX_DIM], LEN=[BOX_LEN], k_1=0.15, k_2=(2*0.15), normalise=True)

    '''out_str = "Tests/test_out_bispec_gen.txt"
    fout = open(out_str,"a+")

    for i in range( len(BS_gen_dict["theta_over_pi"]) ):
        fout.write( "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(BS_gen_dict["theta_over_pi"][i], BS_gen_dict["k3"][i], BS_gen_dict["BS"][i], BS_gen_dict["BSimag"][i], BS_gen_dict["k1"][i], BS_gen_dict["k2"][i], BS_gen_dict["P1"][i], BS_gen_dict["P2"][i], BS_gen_dict["P3"][i], BS_gen_dict["Ntri"][i] ) )
    fout.close()'''

    out_str = "Tests/bispec_gen_hard_coded.pkl"
    if (REGEN_TEST_DATA):
        f = open(out_str, 'wb' )
        pickle.dump( BS_gen_dict, f )
        f.close()

    f = open(out_str, 'rb')
    BS_test_dict = pickle.load(f)
    f.close()

    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["k3"] )
    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["BS"] )
    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["BSimag"] )
    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["k1"] )
    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["k2"] )
    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["P1"] )
    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["P2"] )
    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["P3"] )
    assert len( BS_gen_dict["theta_over_pi"] ) == len( BS_gen_dict["Ntri"] )

    for i in range( len(BS_test_dict["k3"]) ):
        assert BS_gen_dict["theta_over_pi"][i] == BS_test_dict["theta_over_pi"][i]
        assert ( np.abs(BS_gen_dict["k3"][i]/BS_test_dict["k3"][i]-1.0) < 1e-4 )
        assert ( np.abs(BS_gen_dict["BS"][i]/BS_test_dict["BS"][i]-1.0) < 1e-4 )
        if (np.abs(BS_gen_dict["BSimag"][i])>0.0 and np.abs(BS_test_dict["BSimag"][i])>0.0 ):
            assert ( np.abs(BS_gen_dict["BSimag"][i]/BS_test_dict["BSimag"][i] - 1.0) < 1e-4 )
        else:
            assert ( np.abs(BS_gen_dict["BSimag"][i]-BS_test_dict["BSimag"][i]) < 100 )
        assert ( np.abs(BS_gen_dict["k1"][i]/BS_test_dict["k1"][i]-1.0) < 1e-4 )
        assert ( np.abs(BS_gen_dict["k2"][i]/BS_test_dict["k2"][i]-1.0) < 1e-4 )
        assert ( np.abs(BS_gen_dict["P1"][i]/BS_test_dict["P1"][i]-1.0) < 1e-4 )
        assert ( np.abs(BS_gen_dict["P2"][i]/BS_test_dict["P2"][i]-1.0) < 1e-4 )
        assert ( np.abs(BS_gen_dict["P3"][i]/BS_test_dict["P3"][i]-1.0) < 1e-4 )
        assert ( np.abs(BS_gen_dict["Ntri"][i]/BS_test_dict["Ntri"][i]-1.0) < 1e-4 )
