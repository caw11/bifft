#import py21cmfast as p21
import numpy as np
import pickle

#import bispec_analysis as bifft
import bifft

# Import the stuff required to make a seaborn plots
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

BOX_DIM = 64
BOX_LEN = 128.0

KPERP1 = 0.1 # These define the fixed k1 vector to use
KLOS1 = 0.1
KPERP2 = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5]
KLOS2 = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5]

KMINlos = 0.001
KMAXlos = 1.6
KMINperp = 0.001
KMAXperp = 1.6

theta_fac = 0.05

REGEN_TEST_DATA = False
PLOT_BISPEC = False
TEST_DATA_FNAME = 'Tests/brightness_temp_test_data.pkl'

if (PLOT_BISPEC is True):
    XY_POS1 = (0.9, -0.12) # (1.16, -0.44)
    XY_POS2 = (-0.11, -0.12)#(-0.35, -0.38)
    XY_POS3 = (0.85, 1.1) #(1.16, 1.365)
    XY_POS4 = (-0.35, 1.1) #(-0.65, 1.435)
    FWIDTH = 12.5
    FHEIGHT = 5
    SHRINK=0.92

def test_cylin_bispec():
    f = open(TEST_DATA_FNAME, 'rb')
    dT_box = np.array( pickle.load(f) )
    f.close()
    #dT_complex = dT_box.astype(np.complex) # Now converts internally

    #assert isinstance(dT_complex[19][19][19], np.complex)

    BS_cylin_dict = bifft.calculate_cylin_bipec(data=dT_box,
                                                DIM=[BOX_DIM, BOX_DIM, BOX_DIM],
                                                LEN=[BOX_LEN, BOX_LEN, BOX_LEN],
                                                K1_PERP_LOS=[KPERP1, KLOS1],
                                                K2_PERP=KPERP2,
                                                K2_LOS=KLOS2,
                                                theta_fac=theta_fac,
                                                klos_min_max=[KMINlos, KMAXlos],
                                                kperp_min_max=[KMINperp, KMAXperp],
                                                normalise=True)

    out_str = "Tests/bispec_cylin_hard_coded.pkl"
    if (REGEN_TEST_DATA):
        f = open(out_str, 'wb' )
        pickle.dump( BS_cylin_dict, f )
        f.close()

    f = open(out_str, 'rb')
    BS_test_dict = pickle.load(f)
    f.close()

    for i in range( len(BS_test_dict["B3"]) ):
        if not( np.isnan(BS_test_dict["B3"][i]) ):
            assert ( np.abs(BS_cylin_dict["B3"][i]/BS_test_dict["B3"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["B4"][i]) ):
            assert ( np.abs(BS_cylin_dict["B4"][i]/BS_test_dict["B4"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["B5"][i]) ):
            assert ( np.abs(BS_cylin_dict["B5"][i]/BS_test_dict["B5"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["B6"][i]) ):
            assert ( np.abs(BS_cylin_dict["B6"][i]/BS_test_dict["B6"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["P1"][i]) ):
            assert ( np.abs(BS_cylin_dict["P1"][i]/BS_test_dict["P1"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["P2"][i]) ):
            assert ( np.abs(BS_cylin_dict["P2"][i]/BS_test_dict["P2"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["P3"][i]) ):
            assert ( np.abs(BS_cylin_dict["P3"][i]/BS_test_dict["P3"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["P4"][i]) ):
            assert ( np.abs(BS_cylin_dict["P4"][i]/BS_test_dict["P4"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["P5"][i]) ):
            assert ( np.abs(BS_cylin_dict["P5"][i]/BS_test_dict["P5"][i]-1.0) < 1e-4 )
        if not( np.isnan(BS_test_dict["P6"][i]) ):
            assert ( np.abs(BS_cylin_dict["P6"][i]/BS_test_dict["P6"][i]-1.0) < 1e-4 )

    if (PLOT_BISPEC is True):
        frac_x_arr = (KPERP2.index(KPERP1)+0.5)/len(KPERP2)
        frac_y_arr = (KLOS2.index(KLOS1)+0.5)/len(KLOS2)
        Bispec_k3_num=BS_cylin_dict["B3"]
        Bispec_k4_num=BS_cylin_dict["B4"]
        Bispec_k5_num=BS_cylin_dict["B5"]
        Bispec_k6_num=BS_cylin_dict["B6"]
        Bispec_n3_num=BS_cylin_dict["triCt3"]
        Bispec_n4_num=BS_cylin_dict["triCt4"]
        Bispec_n5_num=BS_cylin_dict["triCt5"]
        Bispec_n6_num=BS_cylin_dict["triCt6"]
        kperp2_cat=BS_cylin_dict["k2perp"]
        klos2_cat=BS_cylin_dict["k2los"]
        kperp3=BS_cylin_dict["k3perp"]
        klos3=BS_cylin_dict["k3los"]
        kperp4=BS_cylin_dict["k4perp"]
        klos4=BS_cylin_dict["k4los"]
        kperp5=BS_cylin_dict["k5perp"]
        klos5=BS_cylin_dict["k5los"]
        kperp6=BS_cylin_dict["k6perp"]
        klos6=BS_cylin_dict["k6los"]
        P1_num=BS_cylin_dict["P1"]
        P2_num=BS_cylin_dict["P2"]
        P3_num=BS_cylin_dict["P3"]
        P4_num=BS_cylin_dict["P4"]
        P5_num=BS_cylin_dict["P5"]
        P6_num=BS_cylin_dict["P6"]

        PS_cbar_str = r"$\Delta^2(k)$ [mK$^2$]"
        BS_cbar_str = r"$B_{\dimless}(k_1, k_2, k_3)$ [mK$^3$]"
        pspec_dim = r"[mK$^2$]"
        bspec_dim = r"[mK$^3$]"
        norm_str="Bicoherence"

        fig, (ax1) = plt.subplots( 1, 1, figsize=(FWIDTH, FHEIGHT) )

        fig_str="Bispectrum (normalised; k5) for k1 = ( -"+str(KPERP1)+", "+str(KLOS1)+") "
        save_as='bispec_k5_normed.png'

        fig.suptitle(fig_str, y=0.92, fontsize=14)
        # ----- k1 = -kperp1, klos1 -----
        df = pd.DataFrame(dict(k_los_2=klos2_cat,
                          k_perp_2=kperp2_cat,
                          bispec=Bispec_k5_num))
        pivot_table = df.pivot("k_los_2", "k_perp_2", "bispec")

        sns.heatmap(data=pivot_table,
                    cmap="Spectral", center=0.0,
                    ax=ax1, square=True, linewidths=0.03, linecolor='k',
                    cbar_kws={"shrink": SHRINK, 'label': BS_cbar_str},
                    annot=True)
        ax1.invert_xaxis()
        ax1.invert_yaxis()
        ax1.annotate(r"$\vec{k}_1 = (-k^{\mathrm{perp}}_1, k^{\mathrm{los}}_1)$",
                     xy=(1.0-frac_x_arr, frac_y_arr), xycoords='axes fraction',
                     xytext=XY_POS1,
                     size=15,
                     textcoords='axes fraction',
                     arrowprops=dict(arrowstyle='simple,head_length=0.5,head_width=0.3,tail_width=0.05'))
        fig.savefig(save_as)
        plt.close(fig)

        fig2, (ax2) = plt.subplots( 1, 1, figsize=(FWIDTH, FHEIGHT) )
        fig_str="Bispectrum (normalised; k3) for k1 = ( "+str(KPERP1)+", "+str(KLOS1)+")"
        save_as = 'bispec_k3_normed.png'

        fig2.suptitle(fig_str, y=0.92, fontsize=14)

        # ----- k1 = kperp1, klos1 -----
        df = pd.DataFrame(dict(k_perp_2=kperp2_cat,
                          k_los_2=klos2_cat,
                          bispec=Bispec_k3_num))
        pivot_table = df.pivot("k_los_2", "k_perp_2", "bispec")

        sns.heatmap(data=pivot_table,
                    cmap="Spectral", center=0.0,
                    ax=ax2, square=True, linewidths=0.03, linecolor='k',
                    cbar_kws={"shrink": SHRINK, 'label': BS_cbar_str},
                    annot=True)
        ax2.invert_yaxis()
        #  ADD AND ARROW CORRESPONDING TO K1
        ax2.annotate(r"$\vec{k}_1$", xy=(frac_x_arr, frac_y_arr), xycoords='axes fraction', xytext=XY_POS2, size=15, textcoords='axes fraction', arrowprops=dict(arrowstyle='simple,head_length=0.5,head_width=0.3,tail_width=0.1'))
        # = (k^{\mathrm{perp}}_1, k^{\mathrm{los}}_1)

        fig2.savefig(save_as)
        plt.close(fig2)

        fig3, (ax3) = plt.subplots( 1, 1, figsize=(FWIDTH, FHEIGHT) )
        fig_str="Bispectrum (normalised; k6) for k1 = ( -"+str(KPERP1)+", -"+str(KLOS1)+")"
        save_as='bispec_k6_normed.png'

        fig3.suptitle(fig_str, y=0.92, fontsize=14)

        # ----- k1 = -kperp1, -klos1 -----
        df = pd.DataFrame(dict(k_los_2=klos2_cat,
                          k_perp_2=kperp2_cat,
                          bispec=Bispec_k6_num))
        pivot_table = df.pivot("k_los_2", "k_perp_2", "bispec")

        sns.heatmap(data=pivot_table,
                    cmap="Spectral", center=0.0,
                    ax=ax3, square=True, linewidths=0.03, linecolor='k',
                    cbar_kws={"shrink": SHRINK, 'label': BS_cbar_str},
                    annot=True)
        ax3.invert_xaxis()
        ax3.annotate(r"$\vec{k}_1 = (-k^{\mathrm{perp}}_1, -k^{\mathrm{los}}_1)$", xy=(1.0-frac_x_arr, 1.0-frac_y_arr), xycoords='axes fraction', xytext=XY_POS3, size=15, textcoords='axes fraction', arrowprops=dict(arrowstyle='simple,head_length=0.5,head_width=0.3,tail_width=0.07'))

        fig3.savefig(save_as)
        plt.close(fig3)

        fig4, (ax4) = plt.subplots( 1, 1, figsize=(FWIDTH, FHEIGHT) )

        fig_str="Bispectrum (normalised; k4) for k1 = ( "+str(KPERP1)+", -"+str(KLOS1)+")"
        save_as='_bispec_k4_normed.png'

        fig4.suptitle(fig_str, y=0.92, fontsize=14)

        # k1 = kperp1, -klos1
        df = pd.DataFrame( dict(k_los_2=klos2_cat,
                          k_perp_2=kperp2_cat,
                          bispec=Bispec_k4_num) )
        pivot_table = df.pivot("k_los_2", "k_perp_2", "bispec")

        sns.heatmap(data=pivot_table,
                    cmap="Spectral", center=0.0,
                    ax=ax4, square=True, linewidths=0.03, linecolor='k',
                    cbar_kws={"shrink": SHRINK, 'label': BS_cbar_str},
                    annot=True)
        #ax4.invert_yaxis()
        #  ADD AND ARROW CORRESPONDING TO K1
        ax4.annotate(r"$\vec{k}_1 = (k^{\mathrm{perp}}_1, -k^{\mathrm{los}}_1)$", xy=(frac_x_arr, 1.0-frac_y_arr), xycoords='axes fraction', xytext=XY_POS4, size=15, textcoords='axes fraction', arrowprops=dict(arrowstyle='simple,head_length=0.5,head_width=0.3,tail_width=0.08'))
        fig4.savefig(save_as)
        plt.close(fig4)

def main():
	"Main() -> BiFFT can be used to calculate various bispectrum and power spectrum code packaged with BiFFT. \
	See bispec_analysis.py for details on how to use."
	test_cylin_bispec()

if __name__ == '__main__':
	main()
