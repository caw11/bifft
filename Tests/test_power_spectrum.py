#import py21cmfast as p21
import numpy as np
import pickle
import bifft


BOX_DIM = 64
BOX_LEN = BOX_DIM*2.0

REGEN_TEST_DATA = False

TEST_DATA_FNAME = 'Tests/brightness_temp_test_data.pkl'

'''def test_regenerate_test_data():
    if (REGEN_TEST_DATA):
        UParams = p21.UserParams(HII_DIM=BOX_DIM, BOX_LEN=BOX_LEN)
        CParams = p21.CosmoParams() # Here we are setting to defaults, could equally pass nothing as function does the same internally
        FOptions = p21.FlagOptions(USE_MASS_DEPENDENT_ZETA=False, USE_TS_FLUCT=False) # Also default, but defining to highlight which parameters control this
        AParams = p21.AstroParams(HII_EFF_FACTOR = 30.0, ION_Tvir_MIN = 4.69897) # Note ION_Tvir_MIN is log so here Tvir = 5 x 10^4

        coevals = p21.run_coeval( redshift = 6.32167913284, user_params = UParams, cosmo_params = CParams, astro_params=AParams, flag_options=FOptions, random_seed=12414, write=False)
        size = UParams.HII_DIM * UParams.HII_DIM * UParams.HII_DIM
        dT_box = coevals.brightness_temp
        f = open(TEST_DATA_FNAME , 'wb' )
        pickle.dump( dT_box, f )
        f.close()
    else:
        print("Using hard coded test data for all tests")'''

def test_power_spec():

    f = open(TEST_DATA_FNAME, 'rb')
    dT_box = np.array( pickle.load(f) )
    f.close()
    #dT_complex = dT_box.astype(np.complex) # Original implementation took complex arguement. Now does conversion internally

    #assert isinstance(dT_complex[19][19][19], np.complex)

    pspec_dict = bifft.calculate_pspec(data=dT_box, DIM=[BOX_DIM], LEN=[BOX_LEN], normalise=True)

    assert len( pspec_dict["k"] ) == len( pspec_dict["Pk"] )
    assert len( pspec_dict["k"] ) == len( pspec_dict["Pkerr"] )

    '''out_str = "Tests/test_out_pspec_new.txt"
    fout = open(out_str,"a+")
    for i in range( len(pspec_dict["k"]) ):
        fout.write( "{}\t{}\t{}\n".format(pspec_dict["k"][i], pspec_dict["Pk"][i], pspec_dict["Pkerr"][i]) )
    fout.close()'''

    out_str = "Tests/pspec_hard_coded.pkl"
    if (REGEN_TEST_DATA):
        f = open(out_str, 'wb' )
        pickle.dump( pspec_dict, f )
        f.close()

    f = open(out_str, 'rb')
    pspec_test_dict = pickle.load(f)
    f.close()

    for i in range( len(pspec_test_dict["k"]) ):
        assert pspec_dict["k"][i] == pspec_test_dict["k"][i]
        assert ( np.abs(pspec_dict["Pk"][i]/pspec_test_dict["Pk"][i]-1.0) < 1e-4)
        assert ( np.abs(pspec_dict["Pkerr"][i]/pspec_test_dict["Pkerr"][i]-1.0) < 1e-4)
