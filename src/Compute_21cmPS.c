#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <omp.h>
#include <complex.h>
#include <fftw3.h>

// A module for constructing the 21cm power spectrum using C code directly accessible from Python
// Written by: Brad Greig (3rd December 2018)

#define PI (double) (3.14159265358979323846264338327)
#define TWOPI (double) (2.0*PI)

// Taken directly from 21cmFAST. For indexing boxes etc.

/* INDEXING MACROS */
//replaced as now dealing with DFT not FFT
//#define DIM_C_INDEX(x,y,z)((unsigned long long)((z)+(DIM_MID+1llu)*((y)+DIM_BOX*(x)))) // for 3D complex array
//#define DIM_R_FFT_INDEX(x,y,z)((unsigned long long)((z)+2llu*(DIM_MID+1llu)*((y)+DIM_BOX*(x)))) // for 3D real array with the FFT padding

#define DIM_C_INDEX(x,y,z)((unsigned long long)((z)+DIM_BOX*((y)+DIM_BOX*(x)))) // for 3D complex array
#define DIM_R_FFT_INDEX(x,y,z)((unsigned long long)((z)+DIM_BOX*((y)+DIM_BOX*(x)))) // for 3D real array with the FFT padding
#define DIM_R_INDEX(x,y,z)((unsigned long long)((z)+DIM_BOX*((y)+DIM_BOX*(x)))) // for 3D real array with no padding



void init_21cmPS_arrays(int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z);
void free_21cmPS_arrays();
unsigned long long real_index(int x, int y, int z, int res_y, int res_z);
unsigned long long complex_index(int x, int y, int z, int res_y, int res_z);

// Data structure to be returned to Python using Ctypes
struct ReturnData{
    int PSbins;
    float *PSData_k;
    float *PSData;
    float *PSData_error;
};

void init_21cmPS_arrays(int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z) {


    //fprintf(stderr, "Compute_21cmPS::init_21cmPS_arrays -> number of bins = %d.\n", NUM_BINS);

}

// Data passed from Python to C
void Compute_21cmPS(struct ReturnData *DataToBeReturned, float *box_real, float *box_imag, double K_LOS_CUT, int LIGHTCONE, int NORMALISE, int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z) {

    // box_real/imag the data to be analysed. NOTE: last axis should correspond to the LOS or frequency axis
    // (int) Lightcone: Whether the cubic box is co-eval (0) or light-cone (1)
    // (int) OBS_SIM: If simulation has been uv-sampled (1), if std 21cmMC output (0)
    // (int) NORMALISE: Whether the 21cm power spectrum is dimensionless (0) or dimensional (i.e. mK^2) (1)
    // (int) DIM_X, Y, Z: Number of voxels on each side length
    // (float) L_X, Y, Z: Length of box on each side in Mpc (comoving)

    double *p_box, *k_ave;
    unsigned long long *in_bin_ct;
    fftwf_complex *deldel_T;

    unsigned long long DIM_TOT_NUM_PIXELS, DIM_TOT_FFT_NUM_PIXELS;
    unsigned long long midd_x, midd_y, midd_z;

    // For initialising/constructing the power spectrum data
    int NUM_BINS;
    float KF_X, KF_Y, KF_Z, KX_NYQ, KY_NYQ, KZ_NYQ;

    float k_floor, k_ceil, k_max, k_first_bin_ceil, k_factor, Delta_k, Volume;

    unsigned long long DIM_KSPACE_NUM_PIXELS;
    // initialise binning of the power spectrum

    Volume = L_X*L_Y*L_Z;

    KF_X = ( TWOPI/L_X );
    KF_Y = ( TWOPI/L_Y );
    KF_Z = ( TWOPI/L_Z );
    //fprintf(stderr, "Compute_21cmPS::init_21cmPS_arrays -> kf_x = %.2f, kf_y = %.2f, kf_z = %.2f.\n", KF_X, KF_Y, KF_Z);

    Delta_k = fmin( fmin(KF_X, KF_Y), fmin(KF_X, KF_Z) );//(TWOPI/L_BOX);

    k_factor = 1.15;
    k_first_bin_ceil = Delta_k*k_factor;

    KX_NYQ = 1.0/2.0*DIM_X*KF_X;
    KY_NYQ = 1.0/2.0*DIM_Y*KF_Y;
    KZ_NYQ = 1.0/2.0*DIM_Z*KF_Z;
    k_max = fmin( fmin(KX_NYQ, KY_NYQ),  fmin(KY_NYQ, KZ_NYQ) );//Delta_k*(int)DIM_BOX;
    DIM_KSPACE_NUM_PIXELS = ( (unsigned long long)( DIM_X*DIM_Y*DIM_Z ) );

    // initialize arrays
    // ghetto counting (lookup how to do logs of arbitrary bases in c...)
    NUM_BINS = 0;
    k_floor = 0;
    k_ceil = k_first_bin_ceil;
    while (k_ceil < k_max){
        NUM_BINS++;
        k_floor=k_ceil;
        k_ceil*=k_factor;
    }

    DIM_TOT_NUM_PIXELS = ( (unsigned long long)( DIM_X*DIM_Y*DIM_Z ) );


    double ave;
    int counter, n_x, n_y, n_z, i, j, k, cnt;
    float k_x, k_y, k_z, deltak_x, deltak_y, deltak_z, k_mag;
    unsigned long long ct;

    // Initialise the power spectrum arrays
    init_21cmPS_arrays(DIM_X, DIM_Y, DIM_Z, L_X, L_Y, L_Z);

    deldel_T = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*DIM_KSPACE_NUM_PIXELS);
    p_box = calloc(NUM_BINS,sizeof(double));
    k_ave = calloc(NUM_BINS,sizeof(double));
    in_bin_ct = (unsigned long long *)calloc(NUM_BINS,sizeof(unsigned long long));

    // Initialise FFT arrays
    fftwf_plan plan;

    deltak_x = ( (2.0*M_PI)/L_X );
    deltak_y = ( (2.0*M_PI)/L_Y );
    deltak_z = ( (2.0*M_PI)/L_Z );

    midd_x = (DIM_X+0.0)/2.0;
    midd_y = (DIM_Y+0.0)/2.0;
    midd_z = (DIM_Z+0.0)/2.0;

    // Fill FFT-array with the fluctuation data
    for (i=0; i<DIM_X; i++){
        for (j=0; j<DIM_Y; j++){
            for (k=0; k<DIM_Z; k++){
                //fprintf(stderr, "Compute_21cmPS::%d, %d, %d and the 1D index = %d.\n", i, j, k, DIM_C_INDEX(i,j,k));
                deldel_T[complex_index(i, j, k, DIM_Y, DIM_Z)] = box_real[complex_index(i, j, k, DIM_Y, DIM_Z)]/(DIM_TOT_NUM_PIXELS+0.0)+box_imag[complex_index(i, j, k, DIM_Y, DIM_Z)]*I/(DIM_TOT_NUM_PIXELS+0.0); //cw converted to interpret input box and complex
                // Note: we include the V/N factor for the scaling after the fft
            }
        }
    }

    // Perform FFT
    // The noise can be complex so we can no longer just deal with real FFTs, hence we now use:
    //fprintf(stderr, "Compute_21cmPS:: performing DFT.\n");
    plan = fftwf_plan_dft_3d(DIM_X, DIM_Y, DIM_Z, (fftwf_complex *)deldel_T, (fftwf_complex *)deldel_T, FFTW_FORWARD, FFTW_ESTIMATE);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);
    fftwf_cleanup();

    // Compute 21cm power spectrum
    // Logic for light-cone or co-eval box
    if (LIGHTCONE) {

        for (n_x=0; n_x<DIM_X; n_x++) {
            if (n_x>midd_x)
                k_x =(n_x-DIM_X) * deltak_x;  // wrap around for FFT convention
            else
                k_x = n_x * deltak_x;

            for (n_y=0; n_y<DIM_Y; n_y++) {
                // avoid the k(k_x = 0, k_y = 0, k_z) modes (pure line-of-sight modes)
                if(n_x != 0 && n_y != 0) {

                    if (n_y>midd_y) {
                        k_y =(n_y-DIM_Y) * deltak_y;
                    }
                    else
                        k_y = n_y * deltak_y;
                    for (n_z=0; n_z<DIM_Z; n_z++) {
                        //k_z = n_z * Delta_k;
                        if (n_z>midd_z) {
                            k_z =(n_z-DIM_Z) * deltak_z;
                        }
                        else
                            k_z = n_z * deltak_z;

                        k_mag = sqrt(k_x*k_x + k_y*k_y + k_z*k_z);

                        // now go through the k bins and update
                        //std::complex<double> x(0,0);
                        //fftw_complex fx;
                        //memcpy( &fx, &x, sizeof( fftw_complex ) );

                        // add pixel contribution if it's LOS is greater than k_los_cut (which we use to exclude small los modes that always get removed with the FG's)
                        if ( fabs(k_z) >= K_LOS_CUT ) {

                          ct = 0;
                          k_floor = Delta_k;
                          k_ceil = k_first_bin_ceil;
                          while (k_ceil < k_max) {
                            // check if we fal in this bin
                            if ((k_mag>=k_floor) && (k_mag < k_ceil)){
                                //if (  ( cabs(creal(deldel_T[complex_index(n_x, n_y, n_z, DIM_Y, DIM_Z)])) > 0.0 ) && ( cabs(cimag(deldel_T[complex_index(n_x, n_y, n_z, DIM_Y, DIM_Z)])) > 0.0  ) ) {
                                in_bin_ct[ct]++;
                                if (NORMALISE==1) // note the 1/VOLUME factor, which turns this into a power density in k-space
                                    p_box[ct] += pow(k_mag,3)*pow(cabs(deldel_T[complex_index(n_x, n_y, n_z, DIM_Y, DIM_Z)]), 2)*Volume/(2.0*PI*PI); //replaced , 2)/(2.0*PI*PI*Volume);
                                else
                                    p_box[ct] += ( pow(cabs(deldel_T[complex_index(n_x, n_y, n_z, DIM_Y, DIM_Z)]), 2)*Volume ); // replaced , 2)/Volume );

                                k_ave[ct] += k_mag;
                                break;

                            }
                            ct++;
                            k_floor=k_ceil;
                            k_ceil*=k_factor;
                          }

                      }
                      else
                      {
                        fprintf(stderr, "Compute_21cmPS:: kz = %.2f, k_los = %.2f.\n", k_z, K_LOS_CUT);
                      }

                    } // nz loop end
                } // ny loop end
            } // nx loop end
        }
    }
    else {
        //fprintf(stderr, "Compute_21cmPS:: looping through k-box to generate power spectrum. \n");
        for (n_x=0; n_x<DIM_X; n_x++){
            if (n_x>midd_x)
                k_x =(n_x - (int)DIM_X) * deltak_x;  // wrap around for FFT convention
            else
                k_x = n_x * deltak_x;

            for (n_y=0; n_y<DIM_Y; n_y++){
                if (n_y > midd_y) {
                    k_y =(n_y - (int)DIM_Y) * deltak_y;
                }
                else
                    k_y = n_y * deltak_y;

                for (n_z=0; n_z<DIM_Z; n_z++){
                    //k_z = n_z * Delta_k;
                    if (n_z > midd_z) {
                      k_z =(n_z - (int)DIM_Z) * deltak_z;
                      }
                    else
                      k_z = n_z * deltak_z;

                    k_mag = sqrt(k_x*k_x + k_y*k_y + k_z*k_z);

                    if ( fabs(k_z) >= K_LOS_CUT ) {
                      // now go through the k bins and update
                      ct = 0;
                      k_floor = Delta_k;
                      k_ceil = k_first_bin_ceil;
                      while (k_ceil < k_max){
                          // check if we fal in this bin
                          if ((k_mag>=k_floor) && (k_mag < k_ceil)){
                              //if (  ( cabs(creal(deldel_T[complex_index(n_x, n_y, n_z, DIM_Y, DIM_Z)])) > 0.0) && ( cabs(cimag(deldel_T[complex_index(n_x, n_y, n_z, DIM_Y, DIM_Z)])) > 0.0 ) )
                              //{
                                  in_bin_ct[ct]++;
                                  if (NORMALISE==1) // note the 1/VOLUME factor, which turns this into a power density in k-space
                                    p_box[ct] += pow(k_mag,3)*pow( cabs(deldel_T[complex_index(n_x, n_y, n_z, DIM_Y, DIM_Z)]), 2)*Volume/(2.0*PI*PI); // replaced , 2)/(2.0*PI*PI*Volume);
                                  else
                                    p_box[ct] += ( pow(cabs(deldel_T[complex_index(n_x, n_y, n_z, DIM_Y, DIM_Z)]), 2)*Volume );// replaced , 2)/Volume );
                                  k_ave[ct] += k_mag;
                                  break;
                              /*}
                              else{
                                  break;
                              }*/

                          }

                          ct++;
                          k_floor=k_ceil;
                          k_ceil*=k_factor;
                      }
                    }
                    else
                    {
                      fprintf(stderr, "Compute_21cmPS:: kz = %.2f, k_los = %.2f.\n", k_z, K_LOS_CUT);
                    }
                }
            }
        }
    }

    counter = 0;
    //fprintf(stderr, "Compute_21cmPS:: Finished calculation, outputting results.\n");
    // Bin the data
    for (cnt=0; cnt<NUM_BINS; cnt++){
        if (in_bin_ct[cnt]>0) {
            DataToBeReturned->PSData_k[counter] = k_ave[cnt]/(in_bin_ct[cnt]+0.0);
            DataToBeReturned->PSData[counter] = p_box[cnt]/(in_bin_ct[cnt]+0.0);
            DataToBeReturned->PSData_error[counter] = p_box[cnt]/(in_bin_ct[cnt]+0.0)/sqrt(in_bin_ct[cnt]+0.0);
            counter += 1;
        }
    }

    DataToBeReturned->PSbins = counter;

    // Clean-up
    fftwf_free(deldel_T);
    free(p_box);
    free(k_ave);
    free(in_bin_ct);
    //free_21cmPS_arrays();
}

unsigned long long real_index(int x, int y, int z, int res_y, int res_z)
  {
    // Returns the 1D index storage location of a 3D indexed box element: Unpadded real box.
    return ( (z) + res_z*((y) + res_y*x) );
  }

unsigned long long complex_index(int x, int y, int z, int res_y, int res_z)
  {
    // returns 1D index for a 3D complex array
    /*int MID;
    MID = ((res_z+0.0)/2.0);
    return ( (z)+(MID+1llu)*((y)+res_y*(x)) );*/ //cw adjusted as now we are using DFT's so no longer work in half plane only
    return ( (z) + res_z*((y) + res_y*x) );
  }

void free_21cmPS_arrays()
  {
    fftwf_cleanup_threads();
    fftwf_cleanup();

  }
