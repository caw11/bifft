#include <omp.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <complex.h>
#include <fftw3.h>

#include <gsl/gsl_integration.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_errno.h>

// A module for constructing the 21cm bispectrum using C code directly accessible from Python
// To be written by: Catherine Watkinson (still contains power spectrum 7th March 2019)

#define PI (double) (3.14159265358979323846264338327)
#define TWOPI (double) (2.0*PI)
#define DEBUG (int) (0)

// For initialising/constructing the power spectrum data
int NUM_BINS, i, j, k;
unsigned int n_x, n_y, n_z, x, y, z;

int X_DIM, Y_DIM, Z_DIM;
unsigned long long X_MID, Y_MID, Z_MID;
int NUM_TH, SCALE_FAC, USE_IND;
unsigned long long TOT_NUM_PIXELS, KSPACE_NUM_PIXELS;
float X_LEN, Y_LEN, Z_LEN;
double VOLUME, DELTA_K_X, DELTA_K_Y, DELTA_K_Z, KF_X, KF_Y, KF_Z, KX_NYQ, KY_NYQ, KZ_NYQ, KF, K_NYQ;

fftwf_complex *delta_fft1, *I_fft1, *delta_fft2, *I_fft2, *delta_fft3, *I_fft3;
fftwf_complex *delta_fft4, *I_fft4, *delta_fft5, *I_fft5, *delta_fft6, *I_fft6;
fftwf_complex *delta_n1, *delta_n2, *delta_n3, *delta_n4, *delta_n5, *delta_n6;

float *I_n1,  *I_n2, *I_n3, *I_n4, *I_n5, *I_n6;

fftwf_complex *box_fft;
double *B, *k3, *Ntri, *pspec;

void init_21cmBS_arrays(int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z, int num_th);
int nbins_isoceles(float K1);
int bispec_perp_los(fftwf_complex *BOX_FFT1, fftwf_complex *BOX_FFT2, fftwf_complex *BOX_FFT3, double KPERP1, double KLOS1, double KPERP2, double KLOS2, double K_FAC, double *PS, double *BS, double *N, double *k3_avg);

double k3_from_theta(double a, double b, double theta);
double theta_from_tri_sides(double a, double b, double c);
unsigned long long real_index(int x, int y, int z, int res_y, int res_z);
unsigned long long real_complex_index(int x, int y, int z, int res_y, int res_z);
unsigned long long complex_index(int x, int y, int z, int res_y, int res_z);

void free_21cmBS_arrays();

/* Data structure to be returned to Python using Ctypes.
Note these are pointers to allow binning over k's to be managed within this
C algorithm or externally by the wrapper.
*/
struct ReturnData{
    int BSbins;
    float *BSData_kperp1;
    float *BSData_klos1;
    float *BSData_kperp2;
    float *BSData_klos2;
    double *BSData_k3; // avg k3 closing triangles formed by k1 = (k1perp, k1los) and k2 = (k2perp, k2los)
    double *BSData_k4; // avg k4 closing triangles formed by k1' = (k1perp, -k1los) and k2 = (k2perp, k2los)
    double *BSData_k5; // avg k5 closing triangles formed by k1'' = (-k1perp, k1los) and k2 = (k2perp, k2los)
    double *BSData_k6; // avg k6 closing triangles formed by k1''' = (-k1perp, -k1los) and k2 = (k2perp, k2los)
    double *BSData_Bk3; // B(k1, k2, k3)
    double *BSData_Bk4; // B(k1', k2, k4)
    double *BSData_Bk5; // B(k1'', k2, k5)
    double *BSData_Bk6; // B(k1''', k2, k5)
    double *BSData_P1; // P(k1)
    double *BSData_P2; // P(k2)
    double *BSData_P3; // P(k3)
    double *BSData_P4; // P(k4)
    double *BSData_P5; // P(k5)
    double *BSData_P6; // P(k6)
    double *BSData_Ntri_k3; // # of triangles that contributed to B(k1, k2, k3)
    double *BSData_Ntri_k4; // # of triangles that contributed to B(k1', k2, k4)
    double *BSData_Ntri_k5; // # of triangles that contributed to B(k1'', k2, k5)
    double *BSData_Ntri_k6; // # of triangles that contributed to B(k1''', k2, k6)
};

void init_21cmBS_arrays(int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z, int num_th) {

  /******************* simulated cube details *********************/
  X_DIM = DIM_X;
  Y_DIM = DIM_Y;
  Z_DIM = DIM_Z;

  X_LEN = L_X;
  Y_LEN = L_Y;
  Z_LEN = L_Z;

  if (DEBUG>0)
    fprintf(stderr, "init_21cmBS_arrays: X_SIM = %d, Y_SIM = %d, Z_SIM = %d, X_LEN = %.0f, Y_LEN = %.0f, Z_LEN = %.0f\n", X_DIM, Y_DIM, Z_DIM, X_LEN, Y_LEN, Z_LEN);

  if (DEBUG>0)
    fprintf(stderr, "init_21cmBS_arrays: number of threads = %d\n", NUM_TH);

  NUM_TH = num_th; // Number of threads to be used

  /************ Indexing definitions (replacing macros from 21cmFAST) *************/
  X_MID = ((X_DIM+0.0)/2.0);
  Y_MID = ((Y_DIM+0.0)/2.0);
  Z_MID = ((Z_DIM+0.0)/2.0);

  TOT_NUM_PIXELS = ( X_DIM*Y_DIM*Z_DIM );
  KSPACE_NUM_PIXELS = ( X_DIM*Y_DIM*(Z_MID+1llu) );

  VOLUME = ( ((double) X_LEN)*((double) Y_LEN)*((double) Z_LEN));

  KF_X = ( (2.0*M_PI)/X_LEN );
  KF_Y = ( (2.0*M_PI)/Y_LEN );
  KF_Z = ( (2.0*M_PI)/Z_LEN );
  KF = fmin( fmin(KF_X, KF_Y), fmin(KF_X, KF_Z) );//( (2.0*M_PI)/BOX_LEN );

  if (DEBUG>0) {
    fprintf(stderr, "init_21cmBS_arrays: KF_X = %.6f, KF_Y = %.6f, KF_Z = %.6f\n", KF_X, KF_Y, KF_Z);
    fprintf(stderr, "init_21cmBS_arrays: VOLUME = %.6f\n", ((float) VOLUME) );
  }

  // K_NYQ: largest K-mode that can be reasonably sampled according to Nyqvuist theorm/limit
  KX_NYQ = 1.0/2.0*X_DIM*KF_X;
  KY_NYQ = 1.0/2.0*Y_DIM*KF_Y;
  KZ_NYQ = 1.0/2.0*Z_DIM*KF_Z;
  K_NYQ = fmin( fmin(KX_NYQ, KY_NYQ),  fmin(KY_NYQ, KZ_NYQ) );//1.0/2.0*HII_DIM*KF;

  pspec = (double *) malloc(sizeof(double)*6);
  Ntri = (double *) malloc(sizeof(double)*10);
  B = (double *) malloc(sizeof(double)*4);
  k3 = (double *) malloc(sizeof(double)*4);

  box_fft = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);

  // For KPERP1, KLOS1 pixels
  delta_fft1 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_fft1 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*KSPACE_NUM_PIXELS );

  delta_n1 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n1 = (float *) malloc(sizeof(float)*TOT_NUM_PIXELS);

  // For KPERP2, KLOS2 pixels
  delta_fft2 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_fft2 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*KSPACE_NUM_PIXELS);

  delta_n2 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n2 = (float *) malloc(sizeof(float)*TOT_NUM_PIXELS);

  // For pixels of k3 completing KPERP1, KPERP2, LOS configurations
  delta_fft3 = (fftwf_complex *)fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_fft3 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*KSPACE_NUM_PIXELS);

  delta_n3 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n3 = (float *) malloc(sizeof(float)*TOT_NUM_PIXELS);

  // For pixels of k4 completing  KPERP1, KPERP2, -LOS configurations
  delta_fft4 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_fft4 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*KSPACE_NUM_PIXELS);

  delta_n4 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n4 = (float *) malloc(sizeof(float)*TOT_NUM_PIXELS);

  // For pixels of k5 completing -KPERP1, KPERP2, LOS configurations
  delta_fft5 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_fft5 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*KSPACE_NUM_PIXELS);

  delta_n5 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n5 = (float *) malloc(sizeof(float)*TOT_NUM_PIXELS);

  // For pixels of k6 completing -KPERP1, KPERP2, -LOS configurations
  delta_fft6 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_fft6 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*KSPACE_NUM_PIXELS);

  delta_n6 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n6 = (float *) malloc(sizeof(float)*TOT_NUM_PIXELS);

}

void ComputeBS_cylindrically_averaged(struct ReturnData *DataToBeReturned, float *box_real, float *box_imag, int NORM, double KPERP1, double KLOS1, double KPERP2, double KLOS2, double K_FAC, int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z, int num_th)
{
    /*
     Calculates the bispectrum for the isoceles k-triangle configuration as a function of angle betweek k1 and k2
     (float) *box_real, _imag: real and imag parts of 3D flattened dataset of data from which the 21cm BS is to be constructed. NOTE: last axis should correspond to the LOS or frequency axis
     <int NORM>  Whether to calculate normalised bispectrum (1) or not (0)
                (see "normalised bispectrum" of Watkinson+ 2018
                https://academic.oup.com/mnras/advance-article/doi/10.1093/mnras/sty2740/5129150)
     <KPERP1/2>, <KLOS1/2>    magnitudes of k on sky (KPERP) and in los (KLOS) for k_1 and k_2 in positive quadrant only
     <int DIM_X/Y/Z>          Number of voxels per side length for x, y, z axis
     <float L_X/Y/Z>          Length of box in Mpc (comoving) for x, y, z axis
     <int num_th>             Number of threads to use for fftws
    */

    unsigned long long RSPACE_NUM_PIXELS, KSPACE_NUM_PIXELS;

    RSPACE_NUM_PIXELS = ( (unsigned long long)( DIM_X*DIM_Y*DIM_Z ) );
    KSPACE_NUM_PIXELS = ( (unsigned long long) (DIM_X*DIM_Y*DIM_Z) );

    if (DEBUG>0)
      {
        fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: dim x, y, z = %d, %d, %d \n", DIM_X, DIM_Y, DIM_Z);
        fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: total pixels for real and kspace = %d, %d \n", RSPACE_NUM_PIXELS, KSPACE_NUM_PIXELS);
      }

    double ave;
    int n_x, n_y, n_z;
    float k_x, k_y, k_z, k_mag;
    unsigned long long ct;

    // ------ INITIALISATION -------
    if (DEBUG>0)
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: commencing initialisation.\n");
    fftwf_plan plan;
    double theta, k3min, k3max, k_min, k_nyq;
    float k1, k2, k1mag, k2mag;
    char filename[1000];
    int N_bins, counter;


    // Initialise the power spectrum arrays and the other variables we will use
    init_21cmBS_arrays(DIM_X, DIM_Y, DIM_Z, L_X, L_Y, L_Z, num_th);

    k_min = (KF+0.0);
    k_nyq = (K_NYQ+0.0);
    if (DEBUG>0)
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: Kmin = %.2f and Knyq = %.2f\n", k_min, k_nyq);

    //fprintf(stderr, "BinBispec::bispec_from_file -> Fourier transforming data to k-space.\n");
    // Set fftw threading parameters
    if (fftwf_init_threads()==0){
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: ERROR: problem initializing fftwf threads\nAborting\n.");
    }

    omp_set_num_threads(NUM_TH);
    //omp_set_dynamic(false);
    fftwf_plan_with_nthreads(NUM_TH);//*/
    if (DEBUG>0)
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: FT dataset.\n");

    // Fill complex FFT-array using the real and imaginary parts of the dataset that has been passed
    //fprintf(stderr, "Compute_21cmBS_isoceles: Loading ffted box\n");
    for (i=0; i<DIM_X; i++){
        for (j=0; j<DIM_Y; j++){
            for (k=0; k<DIM_Z; k++){

                box_fft[complex_index(i, j, k, DIM_Y, DIM_Z)] = box_real[complex_index(i, j, k, DIM_Y, DIM_Z)]/(RSPACE_NUM_PIXELS+0.0) + box_imag[complex_index(i, j, k, DIM_Y, DIM_Z)]*I/(RSPACE_NUM_PIXELS+0.0); //cw converted to interpret input box and complex
                // Note: we include a 1/N factor for the scaling after the fft, the V contribution is taken care of in the final normalisation of B and P to avoid numerical noise stopping unsampled pixels being excluded
            }
        }
    }

    //fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: box contents (i=0 pixel) = %e,  %e.\n", box_real[20], box_imag[20]);
    // ------ FOURIER TRANSFORM DATA -------
    // Fourier transform real data cuboids
    plan = fftwf_plan_dft_3d(DIM_X, DIM_Y, DIM_Z, (fftwf_complex *)box_fft, (fftwf_complex *)box_fft, FFTW_FORWARD, FFTW_ESTIMATE);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);

    // CHECK THE SPHERICALLY AVERAGED VERSION FOR HOW TO DEAL WITH DOING ALL THE BINNING FROM WITHIN C which ultimately makes the most sense.
    // call algorithm for cylindrically averaged bispectrum
    if (DEBUG>0)
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: calling bispectrum calculation function.\n");
    bispec_perp_los( box_fft, box_fft, box_fft, KPERP1, KLOS1, KPERP2, KLOS2, K_FAC, pspec, B, Ntri, k3 );
    if (DEBUG>0) {
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: bispec returned from bispec_perp_los B(k3) = %e, B(k4) = %e, B(k5) = %e, B(k6) = %e.\n", B[0], B[1], B[2], B[3] );
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: power spec returned from bispec_perp_los P(k1) = %e, P(k2) = %e, P(k3) = %e, P(k4) = %e, P(k5) = %e, P(k6) = %e.\n", pspec[0], pspec[1], pspec[2], pspec[3], pspec[4], pspec[5] );
    }
    if (DEBUG>0)
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: loading returned data from bispec function to the structure interfacing wrapper and this C bispec code.\n");
    // load into the data structure that provides our interface with the Python/C wrapper
    DataToBeReturned->BSbins = 1;
    DataToBeReturned->BSData_kperp1[0] = KPERP1; // seems pointless to return a passed arguement
    DataToBeReturned->BSData_klos1[0] = KLOS1; // but leaves the code flexible for dealing with binning
    DataToBeReturned->BSData_kperp2[0] = KPERP2; // internally to this code rather than in the wrapper
    DataToBeReturned->BSData_klos2[0] = KLOS2;
    DataToBeReturned->BSData_k3[0] = k3[0]; // avg k3 closing triangles formed by k1 = (k1perp, k1los) and k2 = (k2perp, k2los)
    DataToBeReturned->BSData_k4[0] = k3[1]; // avg k4 closing triangles formed by k1' = (k1perp, -k1los) and k2' = (k2perp, k2los)
    DataToBeReturned->BSData_k5[0] = k3[2]; // avg k5 closing triangles formed by k1'' = (-k1perp, k1los) and k2 = (k2perp, k2los)
    DataToBeReturned->BSData_k6[0] = k3[3]; // avg k5 closing triangles formed by k1''' = (-k1perp, -k1los) and k2 = (k2perp, k2los)

    if (NORM>0)
      {
        if (DEBUG>0)
          fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: normalising power spectrum.\n");
        DataToBeReturned->BSData_P1[0] = pspec[0]*pow( sqrt( KPERP1*KPERP1 + KLOS1*KLOS1 ), 3 )/(2.0*M_PI*M_PI); // P(k1)
        DataToBeReturned->BSData_P2[0] = pspec[1]*pow( sqrt( KPERP2*KPERP2 + KLOS2*KLOS2 ), 3 )/(2.0*M_PI*M_PI); // P(k2)
        DataToBeReturned->BSData_P3[0] = pspec[2]*pow( k3[0], 3)/(2.0*M_PI*M_PI); // P(k3)
        DataToBeReturned->BSData_P4[0] = pspec[3]*pow( k3[1], 3)/(2.0*M_PI*M_PI); // P(k4)
        DataToBeReturned->BSData_P5[0] = pspec[4]*pow( k3[2], 3)/(2.0*M_PI*M_PI); // P(k5)
        DataToBeReturned->BSData_P6[0] = pspec[5]*pow( k3[3], 3)/(2.0*M_PI*M_PI); // P(k6)
      }
    else
      {
        DataToBeReturned->BSData_P1[0] = pspec[0]; // P(k1)
        DataToBeReturned->BSData_P2[0] = pspec[1]; // P(k2)
        DataToBeReturned->BSData_P3[0] = pspec[2]; // P(k3)
        DataToBeReturned->BSData_P4[0] = pspec[3]; // P(k4)
        DataToBeReturned->BSData_P5[0] = pspec[4]; // P(k5)
        DataToBeReturned->BSData_P6[0] = pspec[5]; // P(k6)
      }
    if (NORM==1)
      {
        if (DEBUG>0)
          fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: normalising bispectrum.\n");
        k1mag = sqrt( KPERP1*KPERP1 + KLOS1*KLOS1 );
        k2mag = sqrt( KPERP2*KPERP2 + KLOS2*KLOS2 );
        DataToBeReturned->BSData_Bk3[0] = B[0]/sqrt( pspec[0]*pspec[1]*pspec[2]/(k1mag*k2mag*k3[0]) ); // B(k1, k2, k3)
        DataToBeReturned->BSData_Bk4[0] = B[1]/sqrt( pspec[0]*pspec[1]*pspec[3]/(k1mag*k2mag*k3[1]) ); // B(k1, k2', k4)
        DataToBeReturned->BSData_Bk5[0] = B[2]/sqrt( pspec[0]*pspec[1]*pspec[4]/(k1mag*k2mag*k3[2]) ); // B(k1'', k2, k5)
        DataToBeReturned->BSData_Bk6[0] = B[3]/sqrt( pspec[0]*pspec[1]*pspec[5]/(k1mag*k2mag*k3[3]) ); // B(k1''', k2, k6)
      }
    else if (NORM==2)
      {
        if (DEBUG>0)
          fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: calculating absolute bispectrum (weird choice, but okay then...).\n");
        DataToBeReturned->BSData_Bk3[0] = sqrt(B[0]*B[0]); // Fairly non-standard, but the flip from positive to negative B(k) at large k is suppressed and this allows us to evaluate the impact on parameter estimation of this feature
        DataToBeReturned->BSData_Bk4[0] = sqrt(B[1]*B[1]);
        DataToBeReturned->BSData_Bk5[0] = sqrt(B[2]*B[2]);
        DataToBeReturned->BSData_Bk6[0] = sqrt(B[3]*B[3]);
      }
    else
      {
        if (DEBUG>0)
          fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: calculating UNnormalised bispectrum.\n");
        DataToBeReturned->BSData_Bk3[0] = B[0]; // B(k1, k2, k3)
        DataToBeReturned->BSData_Bk4[0] = B[1]; // B(k1', k2, k4)
        DataToBeReturned->BSData_Bk5[0] = B[2]; // B(k1'', k2, k5)
        DataToBeReturned->BSData_Bk6[0] = B[3]; // B(k1''', k2, k5)
      }
    if (DEBUG>0)
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: calculating Ntri = %.2f,  %.2f,  %.2f.\n", Ntri[5], Ntri[6], Ntri[7]);
    DataToBeReturned->BSData_Ntri_k3[0] = Ntri[6]; // # of triangles that contributed to B(k1, k2, k3)
    DataToBeReturned->BSData_Ntri_k4[0] = Ntri[7]; // # of triangles that contributed to B(k1, k2', k4)
    DataToBeReturned->BSData_Ntri_k5[0] = Ntri[8]; // # of triangles that contributed to B(k1', k2, k5)
    DataToBeReturned->BSData_Ntri_k6[0] = Ntri[9]; // # of triangles that contributed to B(k1', k2, k5)

    if (DEBUG>0)
      {
        fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: Bispec after norm B(k3) = %e, B(k4) = %e, B(k5) = %e, B(k6) = %e.\n", DataToBeReturned->BSData_Bk3[0], DataToBeReturned->BSData_Bk4[0], DataToBeReturned->BSData_Bk5[0], DataToBeReturned->BSData_Bk6[0] );
        fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: power spec after norm P(k1) = %e, P(k2) = %e, P(k3) = %e, P(k4) = %e, P(k5) = %e, P(k6) = %e.\n", DataToBeReturned->BSData_P1[0], DataToBeReturned->BSData_P2[0], DataToBeReturned->BSData_P3[0], DataToBeReturned->BSData_P4[0], DataToBeReturned->BSData_P5[0], DataToBeReturned->BSData_P6[0] );
      }

    if (DEBUG>0)
      fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: cleaning up.\n");
    // ------ CLEAN-UP -------
    free_21cmBS_arrays();

}


int bispec_perp_los(fftwf_complex *BOX_FFT1, fftwf_complex *BOX_FFT2, fftwf_complex *BOX_FFT3, double KPERP1, double KLOS1, double KPERP2, double KLOS2, double K_FAC, double *PS, double *BS, double *N, double *k3_avg)
{
  /*
    Calculates the bispectrum for k1 and k2 defined in 2D plane, cylindrically avged (around los axis).
    To do so k1 and k2 vectors have restricted binning of +/- one pixel (taking projection effects into account)
    and k3 is allowed to vary by some angle and los range set by K_FAC.
    Returns B[ (kperp1, klos1), (kperp2, klos2) ], B[ (kperp1, klos1), (kperp2, -klos2) ],
    and B[ (kperp1, -klos1), (kperp2, klos2) ]

    INPUT VARIABLES:
    <fftwf_complex BOX_FFT1>, <fftwf_complex BOX_FFT2>, <fftwf_complex BOX_FFT3> are Fourier datasets
                                                                      (for auto bispec, pass all the same)
    <double KPERP1/2>, <double KLOS1/2> are magnitudes of k on sky (KPERP) and in los (KLOS) for k_1 and k_2 in positive quadrant
    <float K_FAC> this decides the binning of k3 and k4 (\Delta\,\theta_{12} = K_FAC*\theta_{12})
                      where theta is angle between k1 and k2 (or -k2)

    OUTPUT VARIABLES (these will be loaded and passed back with the outputs of module):
    <double *PS> power spectrum for P_1, P_2, P_3, P_4, P_5 (where k_4 completes triangle with -KLOS2),
                  and P_5 (where k_5 completes triangle with -KLOS1);
                  needs memalloc e.g. (double *) malloc(sizeof(double)*5);
    <double *BS> bispectrum for KLOS2 and -KLOS2;
                  needs memalloc e.g. (double *) malloc(sizeof(double)*3);
    <double *N> for number of modes or triangles in P1, P2, P3, P4, P5, Bk3, Bk4, Bk5;
                  needs memalloc e.g. (double *) malloc(sizeof(double)*8);
    <double *k3_avg> for the avg value of k3 associated with KLOS2 and -KLOS2;
                      needs memalloc e.g. (double *) malloc(sizeof(double)*3);
  */

  unsigned int k3_ct, k4_ct, k5_ct, k6_ct;
  unsigned long long indx, indx_rc;
  double kpix_mag; // pix -> pixel
  double k1mag, k2mag, k3mag, k4mag, k5mag, k6mag; //k4 completes -KPERP2 triangle
  double k3tol, k4tol, k5tol, k6tol;
  double k3_los_mag, k4_los_mag, k5_los_mag, k6_los_mag;
  double k3min, k3max, k4min, k4max, k5min, k5max, k6min, k6max;
  //double Delk3, Delk4, Delk5, Delk6, k3_los_min, k3_los_max, k4_los_min, k4_los_max, k5_los_min, k5_los_max, k6_los_min, k6_los_max; //Del -> Delta
  double theta, theta_negk1, theta_negk2; // neg -> negative
  double nx_min, ny_min, nz_min, nz3_min;
  double nx, ny, nz;
  double nxp1, nyp1, nzp1, nz3p1; // p -> plus
  double perp_mag_min, perp_mag_max, los_px_min, los_px_max, px_mag_min, px_mag_max;
  double P;

  double LL, UL, LU, UU;

  double perp_cnrs[4], px_cnrs[8], sum_I[10];
  fftwf_complex complex_bi3, complex_bi4, complex_bi5, complex_bi6, complex_P1, complex_P2, complex_P3, complex_P4, complex_P5, complex_P6;
  fftwf_plan plan;

  // ------- INITIALISATION OF VARIABLES--------
  if (DEBUG>0)
    {
      fprintf(stderr, "BinBispec::bispec_perp_los: Initialising eveything... Global variables are as follows.\n");
      fprintf(stderr, "BinBispec::bispec_perp_los: dim x, y, z = %d, %d, %d.\n", X_DIM, Y_DIM, Z_DIM);
      fprintf(stderr, "BinBispec::bispec_perp_los: number pixels = %d\n", TOT_NUM_PIXELS);
      fprintf(stderr, "BinBispec::bispec_perp_los: Volume = %.2f\n", ((float) VOLUME) );
      fprintf(stderr, "BinBispec::bispec_perp_los: KPERP1 = %.2f, KLOS1 = %.2f\n", KPERP1, KLOS1);
      fprintf(stderr, "BinBispec::bispec_perp_los: KPERP2 = %.2f, KLOS2 = %.2f\n", KPERP2, KLOS2);
      fprintf(stderr, "BinBispec::bispec_perp_los: KFAC = %.2f\n", K_FAC);
    }


  k3_avg[0] = 0.0;
  k3_avg[1] = 0.0;
  k3_avg[2] = 0.0;
  k3_avg[3] = 0.0;
  k3_ct = 0.0;
  k4_ct = 0.0;
  k5_ct = 0.0;
  k6_ct = 0.0;

  for (i=0; i<9; ++i)
      {
	       sum_I[i] = 0.0;
      }
  complex_bi3 = 0.0 + I*0.0;
  complex_bi4 = 0.0 + I*0.0;
  complex_bi5 = 0.0 + I*0.0;
  complex_bi6 = 0.0 + I*0.0;
  complex_P1 = 0.0 + I*0.0;
  complex_P2 = 0.0 + I*0.0;
  complex_P3 = 0.0 + I*0.0;
  complex_P4 = 0.0 + I*0.0;
  complex_P5 = 0.0 + I*0.0;
  complex_P6 = 0.0 + I*0.0;

  // If the user has not specified K1 and K2 los/perp in positive quadrant tell them and override so that they are
  if (KPERP1<0.0)
    {
        fprintf(stderr, "BinBispec::bispec_perp_los: forcing KPERP1 into positive quadrant\n");
        KPERP1*=-1.0;
    }
  if (KLOS1<0.0)
    {
        fprintf(stderr, "BinBispec::bispec_perp_los: forcing KLOS1 into positive quadrant\n");
        KLOS1*=-1.0;
    }
  if (KPERP2<0.0)
    {
        fprintf(stderr, "BinBispec::bispec_perp_los: forcing KPERP2 into positive quadrant\n");
        KPERP2*=-1.0;
    }
  if (KLOS2<0.0)
    {
        fprintf(stderr, "BinBispec::bispec_perp_los: forcing KLOS2 into positive quadrant\n");
        KLOS2*=-1.0;
    }


  // work out magnitudes of three k-vectors involved in default config and the angle between k1 and k2
  k1mag = sqrt( pow(KPERP1,2) + pow(KLOS1,2) );
  k2mag = sqrt( pow(KPERP2,2) + pow(KLOS2,2) );

  k3mag = sqrt( pow( (KPERP1 + KPERP2), 2) + pow( (KLOS1 + KLOS2), 2) );
  k4mag = sqrt( pow( (KPERP1 + KPERP2), 2) + pow( (-KLOS1 + KLOS2), 2) );

  k5mag = sqrt( pow( (-KPERP1 + KPERP2), 2) + pow( (KLOS1 + KLOS2), 2) );
  k6mag = sqrt( pow( (-KPERP1 + KPERP2), 2) + pow( (-KLOS1 + KLOS2), 2) );

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: magnitudes of k1 = %.4f, k2 = %.4f, k3 = %.4f, k4 = %.4f, k5 = %.4f, k6 (!!ADD!!!) = %.4f\n", k1mag, k2mag, k3mag, k4mag, k5mag, k6mag);

  //if (DEBUG>0)
    //fprintf(stderr, "BinBispec::bispec_perp_los: theta = %.4f, theta_neg_los = %.4f, theta_neg_k1 = %.4f, ", theta, theta_negk1, theta_negk2);

  // work out the allowed range of k3 and k4 vector MAGNITUDE that correspond to our choosen bin factor over theta_12 (defined by K_FAC)

  theta = theta_from_tri_sides(k1mag, k2mag, k3mag);
  k3min = k3_from_theta( k1mag, k2mag, (theta*(1.0-K_FAC)) ); //k3mag*(1.0-K_FAC);//
  k3max = k3_from_theta( k1mag, k2mag, (theta*(1.0+K_FAC)) ); //k3mag*(1.0+K_FAC);//

  theta = theta_from_tri_sides(k1mag, k2mag, k4mag);
  k4min = k3_from_theta( k1mag, k2mag, (theta*(1.0-K_FAC)) ); //k3_from_theta( k1mag, k2mag, (theta_negk1*(1.0-K_FAC)) );//
  k4max = k3_from_theta( k1mag, k2mag, (theta*(1.0+K_FAC)) ); //k3_from_theta( k1mag, k2mag, (theta_negk1*(1.0+K_FAC)) ); //

  theta = theta_from_tri_sides(k1mag, k2mag, k5mag);
  k5min = k3_from_theta( k1mag, k2mag, (theta*(1.0-K_FAC)) ); //k3_from_theta( k1mag, k2mag, (theta_negk2*(1.0-K_FAC)) ); //
  k5max = k3_from_theta( k1mag, k2mag, (theta*(1.0+K_FAC)) ); //k3_from_theta( k1mag, k2mag, (theta_negk2*(1.0+K_FAC)) ); //

  theta = theta_from_tri_sides(k1mag, k2mag, k6mag);
  k6min = k3_from_theta( k1mag, k2mag, (theta*(1.0-K_FAC)) ); //k3_from_theta( k1mag, k2mag, (theta_negk1 - (theta_neg2*THETA_FAC) ) );//k6mag*(1.0-K_FAC); //
  k6max = k3_from_theta( k1mag, k2mag, (theta*(1.0+K_FAC)) ); //k3_from_theta( k1mag, k2mag, (theta_negk1 + (theta_negk2*THETA_FAC) ) );//k6mag*(1.0+K_FAC); ////

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: magnitude min, max -> k3 = [%.4f, %.4f], k4 = [%.4f, %.4f], k5 = [%.4f, %.4f], k6 (!!ADD!!) = [%.4f, %.4f]\n", k3min, k3max, k4min, k4max, k5min, k5max, k6min, k6max);

  // Need to work out the los Delta for k3 (so that we can bin cylindriclly according by this angle)
  // Note that this is too restrictive for some theta-facs, i.e. it is less than the pix
  k3_los_mag = (KLOS1 + KLOS2); // Since klos3 = -(KLOS1 + KLOS2) but line-of-sight info only stored in positive part of axis delta(-z) = delta*(z)
  k3tol = k3mag*(1.0+K_FAC) - k3mag*(1.0-K_FAC);

  k4_los_mag = fabs(-KLOS1 + KLOS2);// Since klos4 = -(-KLOS1 + KLOS2) but los info only stored in positive part of axis delta(-z) = delta*(z)
  k4tol = k4mag*(1.0+K_FAC) - k4mag*(1.0-K_FAC);

  k5_los_mag = (KLOS1 + KLOS2); // Since klos5 = -(KLOS1 + KLOS2) but los info only stored in positive part of axis delta(-z) = delta*(z)
  k5tol = k5mag*(1.0+K_FAC) - k5mag*(1.0-K_FAC);

  k6_los_mag = fabs(-KLOS1 + KLOS2);// Since klos6 = -(-KLOS1 + KLOS2) but los info only stored in positive part of axis delta(-z) = delta*(z)
  k6tol = k6mag*(1.0+K_FAC) - k6mag*(1.0-K_FAC);
  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: k3tol = %.4f, k4tol = %.4f, k5tol = %.4f\n", k3tol, k4tol, k5tol);
    //fprintf(stderr, "BinBispec::bispec_perp_los: los min, max -> k3 = [%.2f, %.2f], k4 = [%.2f, %.2f], k5 = [%.2f, %.2f]\n", k3_los_min, k3_los_max, k4_los_min, k4_los_max, k5_los_min, k5_los_max);

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: Starting loop through fft box\n.");
  // ------- LOOP THROUGH PIXELS AND LOAD APPROPRIATE PIXELS WITH DATA VALUES WHERE MEET K1, K2, K3, K4 or K5 REQUIREMENTS --------
  for  (n_x=0; n_x<X_DIM; n_x++)
    {
      for  (n_y=0; n_y<Y_DIM; n_y++)
	     {
	        for  (n_z=0; n_z<Z_DIM; n_z++) // midd_ind because fftw only stores half the volume
	         {
	            // adjust n_x and n_y to allow for the fact that fftw
	            // stores negative k for n_x between midd_ind and res_
	            if (n_x>X_MID)
		            nx = ((double) n_x-X_DIM);
	            else
		            nx = ((double) n_x);
	            if (n_y>Y_MID)
		            ny = ((double) n_y-Y_DIM);
	            else
		            ny = ((double) n_y);
              if (n_z>Z_MID)
    		        nz = ((double) n_z-Z_DIM);
    	        else
    		        nz = ((double) n_z);

              kpix_mag = sqrt( nx*nx*pow(KF_X,2) + ny*ny*pow(KF_Y,2)  + nz*nz*pow(KF_Z,2) );

              //To make the binning match the original method, want to place a virtual pixel around this co-ordinates
              // Create a new origin and then work out the magnitudes associated with each corner
              //nx -= K_FAC; //2.051;//0.51;//
              //ny -= K_FAC; //2.051;//0.51;//
              //nz -= K_FAC; //2.051;//0.51;//
              // NOTE: The hardcoded bin sizes were chosen to be the minimum required to keep sample variance in 21cmFAST simulations to a sensible level, you may need to adjust these or even better the resolution of your simulations so that this estimator is optimally robust whilst remaining accurate.
              nx_min = (nx - 4.051)*( KF_X+0.0 );//0.51;//*(1.0-K_FAC)*( KF_X+0.0 ); //cw  binning adjustments!
              ny_min = (ny - 4.051)*( KF_Y+0.0 );//0.51;//*(1.0-K_FAC)*( KF_Y+0.0 ); //
              nz_min = (nz - 4.051)*( KF_Z+0.0 );//0.51;//*(1.0-K_FAC)*( KF_Z+0.0 ); //
              //nz3_min = (nz - 5.051)*( KF_Z+0.0 );

              // Convert into k rather than dimensionless pixel co-ordinates
              nxp1 = (nx + 4.051)*( KF_X+0.0 );//nx*(1.0+K_FAC)*( KF_X+0.0 ); //
              nyp1 = (ny + 4.051)*( KF_Y+0.0 );//ny*(1.0+K_FAC)*( KF_Y+0.0 ); //
              nzp1 = (nz + 4.051)*( KF_Z+0.0 );//nz*(1.0+K_FAC)*( KF_Z+0.0 ); //
              //nz3p1 = (nz + 5.051)*( KF_Z+0.0 );

              if (DEBUG>2)
                {
                  fprintf(stderr, "BinBispec::bispec_perp_los: nx= %.4f, ny= %.4f, nz= %.4f\n", nx, ny, nz);
                  fprintf(stderr, "BinBispec::bispec_perp_los: kf_x= %.4f, kf_y= %.4f, kf_z= %.4f, k_fac= %.4f\n", KF_X, KF_Y, KF_Z, K_FAC);
                }
              //nx *= ( KF_X+0.0 );
              //ny *= ( KF_Y+0.0 );
              //nz *= ( KF_Z+0.0 );
              nx = nx_min;
              ny = ny_min;
              nz = nz_min;

              if (DEBUG>2)
                {
                  fprintf(stderr, "BinBispec::bispec_perp_los: nx_min= %e, ny_min= %e, nz_min= %e\n", nx, ny, nz);
                  fprintf(stderr, "BinBispec::bispec_perp_los: nx_max= %e, ny_max= %e, nz_max= %e\n", nxp1, nyp1, nzp1);
                }

              /* Need to work out the max + min k_perp magnitude associated with
              kperp pixel +/-1 pixel
              (this found to be optimal fixed bin size for the fixed legs of triangle)
              Need to calculate the max and min magnitude associated with the pixel corners*/
              LL = pow(nx, 2) + pow(ny, 2);
              UL = pow(nxp1, 2) + pow(ny, 2);
              LU = pow(nx, 2) + pow(nyp1, 2);
              UU = pow(nxp1, 2) + pow(nyp1, 2);

              // Calc. maximum and minimum magnitudes assoc. with perpendicular (on-the-sky) pixel projection
              perp_cnrs[0] = sqrt( LL );
              perp_cnrs[1] = sqrt( UL );
              perp_cnrs[2] = sqrt( LU );
              perp_cnrs[3] = sqrt( UU );

              if (DEBUG>2)
                fprintf(stderr, "BinBispec::bispec_perp_los: magnitude of pixel (perp projection) corners = %e, %e, %e, %e\n", perp_cnrs[0], perp_cnrs[1], perp_cnrs[2], perp_cnrs[3]);

              perp_mag_min = 1.0e300;
              perp_mag_max = -1.0e300;
              for (i = 0; i < 4; i++)
                {
                  if (perp_cnrs[i] < perp_mag_min)
                    perp_mag_min = ((double) perp_cnrs[i]);
                  if (perp_cnrs[i] > perp_mag_max)
                    perp_mag_max = ((double) perp_cnrs[i]);
                }
              if (DEBUG>2)
                fprintf(stderr, "BinBispec::bispec_perp_los: px PERP mag min, max -> k3 = [%.2f, %.2f]\n", perp_mag_min, perp_mag_max);

              /* Calc. min and maximum magnitude assoc. with los pixel projection
              Note that whilst LOS k can be negative, the nz are only positive as
              the real FFT only stores positive half of last dimension */
              los_px_min = fmin( fabs(nz), fabs(nzp1) );
              los_px_max = fmax( fabs(nz), fabs(nzp1) );
              //los_px_min = nz; //*(1.0-K_FAC);
              //los_px_max = nzp1; //*(1.0+K_FAC);
              //cw the above (and all associated) need adjusting because we now have positive and neg los to consider
              if (DEBUG>2)
                fprintf(stderr, "BinBispec::bispec_perp_los: px LOS mag min, max = [%.2f, %.2f]\n", nz, nzp1);
              // Calc. max and minimum corner magnitudes assoc. with full 3D pixel projection
              px_cnrs[0] = sqrt( LL + pow(nz, 2) );
              px_cnrs[1] = sqrt( UL + pow(nz, 2) );
              px_cnrs[2] = sqrt( LU + pow(nz, 2) );
              px_cnrs[3] = sqrt( UU + pow(nz, 2) );

              px_cnrs[4] = sqrt( LL + pow(nzp1, 2) );
              px_cnrs[5] = sqrt( UL + pow(nzp1, 2) );
              px_cnrs[6] = sqrt( LU + pow(nzp1, 2) );
              px_cnrs[7] = sqrt( UU + pow(nzp1, 2) );

              // Work out the maximum and minimum magnitude associated with the pixel
              px_mag_min = 1.0e300;
              px_mag_max = -1.0e300;
              for (i = 0; i < 8; i++)
                {
                  if (px_cnrs[i] < px_mag_min)
                    px_mag_min = ((double) px_cnrs[i]);
                  if (px_cnrs[i] > px_mag_max)
                    px_mag_max = ((double) px_cnrs[i]);
                }
              if (DEBUG>2)
                fprintf(stderr, "BinBispec::bispec_perp_los: px mag min, max = [%.2f, %.2f]\n", px_mag_min, px_mag_max);
              //  The following expresions exclude unsampled pixels: && ( cabs(creal(BOX_FFT[DIM_C_INDEX(n_x, n_y, n_z)])) > 0.00 ) && ( cabs(cimag(BOX_FFT[DIM_C_INDEX(n_x, n_y, n_z)])) > 0.00 )
              //if (   ( (perp_mag_min<=KPERP1)&&(KPERP1<=perp_mag_max) ) &&  ( (los_px_min<=KLOS1)&&(KLOS1<=los_px_max) ) && ( cabs(creal(BOX_FFT1[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT1[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
              indx = complex_index( n_x, n_y, n_z, Y_DIM, Z_DIM );
              indx_rc = real_complex_index( n_x, n_y, n_z, Y_DIM, Z_DIM);
              if (  ( (px_mag_min<=k1mag)&&(k1mag<=px_mag_max) )
              && ( (los_px_min<=KLOS1)&&(KLOS1<=los_px_max) )  )//&& ( cabs(creal(BOX_FFT1[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT1[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
                 {
  		               //load the delta_k values into delta_fft associated with k1
  		               delta_fft1[indx]  = BOX_FFT1[indx]; //cw box update
  		               I_fft1[indx_rc]  = 1.0;
  		           }
  	          else
  		           {
  		               delta_fft1[indx] = 0.0 + I*0.0;
  		               I_fft1[indx_rc]  = 0.0;
  		           }
              //if (  ( (perp_mag_min<=KPERP2)&&(KPERP2<=perp_mag_max) ) &&  ( (los_px_min<=KLOS2)&&(KLOS2<=los_px_max) ) && ( cabs(creal(BOX_FFT2[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT2[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
              if (  ( (px_mag_min<=k2mag)&&(k2mag<=px_mag_max) )
              &&  ( (los_px_min<=KLOS2)&&(KLOS2<=los_px_max) )  ) //&& ( cabs(creal(BOX_FFT2[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT2[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
                 {
  		              //load the delta_k values into delta_fft associated with k2
  		               delta_fft2[indx] = BOX_FFT2[indx]; //cw box update
  		               I_fft2[indx_rc] = 1.0;
  		           }
              else
   		           {
   		               delta_fft2[indx] = 0.0 + I*0.0;
   		               I_fft2[indx_rc] = 0.0;
   		           }

              // NOW LOAD BOXES ASSOCIATED WITH K3, K4, K5, here condtional different as allow a larger theta_12 based binwidth
              //load the delta_k values into delta_fft associated with k3 magnitude and in k3 los bin (defined by angle bin over k1 and k2)
              //if (  ( (fabs(k3min-px_mag_min)<=k3tol) || (fabs(k3max-px_mag_min)<=k3tol) ||(fabs(k3max-px_mag_max)<=k3tol) ||(fabs(k3min-px_mag_max)<=k3tol) ) && ( (los_px_min<=k3_los_mag)&&(k3_los_mag<=los_px_max) ) && ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )   )
              //if (  (px_mag_min<=k3mag)&&(k3mag<=px_mag_max)
              if (  ( (k3min<=px_mag_min)&&(px_mag_min<=k3max)  ) || ( (k3min<=px_mag_max)&&(px_mag_max<=k3max)  )
                    || ( ( px_mag_min<=k3min)&&(k3min<=px_mag_max ) ) || ( (px_mag_min<=k3max) && (k3max<=px_mag_max) ) )
                    //&& ( (los_px_min<=k3_los_mag)&&(k3_los_mag<=los_px_max) ) //cw this makes no difference as fft enforces this conditon for us
                    //&& ( (nz3_min<=k3_los_mag)&&(k3_los_mag<=nz3p1) )
                    //&& ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )
                    //&& ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
                {
                  delta_fft3[indx] = BOX_FFT3[indx]; //cw box update
                  I_fft3[indx_rc] = 1.0;
                  k3_avg[0]+=kpix_mag;
                  k3_ct+=1;
                }
              else
                {
                  delta_fft3[indx] = 0.0 + I*0.0;
                  I_fft3[indx_rc] = 0.0;
                }
              //load the delta_k values into delta_fft associated with k4 magnitude and in k4 los bin (defined by angle bin over k1 and -k2)
              //if ( ( (fabs(k4min-px_mag_min)<=k4tol) || (fabs(k4max-px_mag_min)<=k4tol) ||(fabs(k4max-px_mag_max)<=k4tol) ||(fabs(k4min-px_mag_max)<=k4tol) ) && ( (fabs(k4_los_min-los_px_min)<=k4tol) || (fabs(k4_los_max-los_px_min)<=k4tol) || (fabs(k4_los_max-los_px_max)<=k4tol) || (fabs(k4_los_min-los_px_max)<=k4tol) ) && ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
              //if (  ( (fabs(k4min-px_mag_min)<=k4tol) || (fabs(k4max-px_mag_min)<=k4tol) || (fabs(k4max-px_mag_max)<=k4tol) || (fabs(k4min-px_mag_max)<=k4tol) ) && ( (los_px_min<=k4_los_mag)&&(k4_los_mag<=los_px_max) ) && ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )   )
              if (  ( (k4min<=px_mag_min)&&(px_mag_min<=k4max)  ) || ( (k4min<=px_mag_max)&&(px_mag_max<=k4max)  )
                    || ( ( px_mag_min<=k4min)&&(k4min<=px_mag_max ) ) || ( (px_mag_min<=k4max) && (k4max<=px_mag_max) ) )
                    //&& ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )
                    //&& ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
                {
                  delta_fft4[indx]  = BOX_FFT3[indx]; //cw box update
                  I_fft4[indx_rc]  = 1.0;
                  k3_avg[1]+=kpix_mag;
                  k4_ct+=1;
                }
              else
                {
                  delta_fft4[indx] = 0.0 + I*0.0;
                  I_fft4[indx_rc] = 0.0;
                }
                //load the delta_k values into delta_fft associated with k4 magnitude and in k4 los bin (defined by angle bin over k1 and -k2)
              //if (  ( (fabs(k5min-px_mag_min)<=k5tol) || (fabs(k5max-px_mag_min)<=k5tol) || (fabs(k5max-px_mag_max)<=k5tol) || (fabs(k5min-px_mag_max)<=k5tol) ) && ( (los_px_min<=k5_los_mag)&&(k5_los_mag<=los_px_max) ) && ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )   )
              if (  ( (k5min<=px_mag_min)&&(px_mag_min<=k5max)  ) || ( (k5min<=px_mag_max)&&(px_mag_max<=k5max)  )
                    || ( (px_mag_min<=k5min)&&(k5min<=px_mag_max) ) || ( (px_mag_min<=k5max) && (k5max<=px_mag_max) ) )
                    //&& ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )
                    //&& ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
                {
                  delta_fft5[indx]  = BOX_FFT3[indx]; //cw box update
                  I_fft5[indx_rc]  = 1.0;
                  k3_avg[2]+=kpix_mag;
                  k5_ct+=1;
                }
              else
                {
                  delta_fft5[indx]  = 0.0 + I*0.0;
                  I_fft5[indx_rc]  = 0.0;
                }
              //if (  ( (fabs(k6min-px_mag_min)<=k6tol) || (fabs(k6max-px_mag_min)<=k6tol) || (fabs(k6max-px_mag_max)<=k6tol) || (fabs(k6min-px_mag_max)<=k6tol) ) && ( (los_px_min<=k6_los_mag)&&(k6_los_mag<=los_px_max) ) && ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )   )
              if (  ( (k6min<=px_mag_min)&&(px_mag_min<=k6max)  ) || ( (k6min<=px_mag_max)&&(px_mag_max<=k6max)  )
                    || ( ( px_mag_min<=k6min)&&(k6min<=px_mag_max ) ) || ( (px_mag_min<=k6max) && (k6max<=px_mag_max) ) )
                    //&& ( cabs(creal(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )
                    //&& ( cabs(cimag(BOX_FFT3[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )  )
                  {
                    delta_fft6[indx]  = BOX_FFT3[indx]; //cw box update
                    I_fft6[indx_rc]  = 1.0;
                    k3_avg[3]+=kpix_mag;
                    k6_ct+=1;
                  }
                else
                  {
                    delta_fft6[indx]  = 0.0 + I*0.0;
                    I_fft6[indx_rc]  = 0.0;
                  }
              if (DEBUG>1)
                {
                  fprintf(stderr, "BinBispec::bispec_perp_los: KPERP1 = %.2f, KLOS1 %.2f, KPERP2 = %.2f, KLOS2 %.2f\n]", KPERP1, KLOS1, KPERP2, KLOS2);
                  fprintf(stderr, "BinBispec::bispec_perp_los: k3 min max = [%.2f, %.2f]\n", k3min, k3max);
                  fprintf(stderr, "BinBispec::bispec_perp_los: k4 min max = [%.2f, %.2f]\n", k4min, k4max);
                  fprintf(stderr, "BinBispec::bispec_perp_los: k3 min max = [%.2f, %.2f]\n", k5min, k5max);
                }

            }
        }
    }
  // Calculate average binwidth of the k3, k4, k5 triangle closing vectors.
  k3_avg[0] /= (k3_ct+0.0);
  k3_avg[1] /= (k4_ct+0.0);
  k3_avg[2] /= (k5_ct+0.0);
  k3_avg[3] /= (k6_ct+0.0);

  // Set fftw threading parameters
  if (fftwf_init_threads()==0){
    fprintf(stderr, "BinBispec::bispec_perp_los: ERROR: problem initializing fftwf threads\nAborting\n.");
    return -1;
  }

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: Initialising threads for FFTs\n.");
  omp_set_num_threads(NUM_TH);
  //omp_set_dynamic(false);
  fftwf_plan_with_nthreads(NUM_TH);//*/

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: FFT delta1 and I1\n.");
  //FFT complex to real delta_fft1
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft1, (fftwf_complex *)delta_n1, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/


  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: FFT I_fft1 to In1\n.");
  //FFT complex to real delta_fft1
  //FFT complex to real I_fft1
  //plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft1, (fftwf_complex *)I_n1, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  plan = fftwf_plan_dft_c2r_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft1, (float *)I_n1, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: FFT delta2 and I2\n.");
  //FFT complex to real delta_fft2
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft2, (fftwf_complex *)delta_n2, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  //FFT complex to real I_fft2
  //plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft2, (fftwf_complex *)I_n2, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  plan = fftwf_plan_dft_c2r_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft2, (float *)I_n2, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: FFT delta3 and I3\n.");
  //FFT complex to real delta_fft3
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft3, (fftwf_complex *)delta_n3, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  //FFT complex to real I_fft3
  //plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft3, (fftwf_complex *)I_n3, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  plan = fftwf_plan_dft_c2r_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft3, (float *)I_n3, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: FFT delta4 and I4\n.");
  //FFT complex to real delta_fft4
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft4, (fftwf_complex *)delta_n4, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  //FFT complex to real I_fft3
  //plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft4, (fftwf_complex *)I_n4, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  plan = fftwf_plan_dft_c2r_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft4, (float *)I_n4, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: FFT delta5 and I5\n.");
  //FFT complex to real delta_fft5
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft5, (fftwf_complex *)delta_n5, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  //FFT complex to real I_fft5
  //plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft5, (fftwf_complex *)I_n5, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  plan = fftwf_plan_dft_c2r_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft5, (float *)I_n5, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: FFT delta6 and I6\n.");
  //FFT complex to real delta_fft5
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft6, (fftwf_complex *)delta_n6, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  //FFT complex to real I_fft5
  //plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft6, (fftwf_complex *)I_n6, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  plan = fftwf_plan_dft_c2r_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft6, (float *)I_n6, FFTW_ESTIMATE);
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/

  fftwf_cleanup_threads();

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: Performing final summation and normalisation\n.");
  // CALCULATE SUMS IN EQN 5.23 AND 5.28 (WATKINSON 2015 THESIS)
  // 1. Make sum over delta_nr and the mode count.
  unsigned long long ind;
  for (x=0; x<X_DIM; x++)
    {
      for (y=0; y<Y_DIM; y++)
        {
          for (z=0; z<Z_DIM; z++)
            {

              ind = real_index(x, y, z, Y_DIM, Z_DIM);

	            complex_P1 += (  delta_n1[ind]*delta_n1[ind] );
	            sum_I[0] += ( I_n1[ind]*I_n1[ind] );
              /*if ( (crealf(delta_n1[ind]*delta_n1[ind] ) < 0.0) || (cimagf(delta_n1[ind]*delta_n1[ind] ) < 0.0) ) {
                fprintf( stderr, "BinBispec::ComputeBS_cylindrically_averaged: ind = %d, delta1 = %e + %e i, delta1*delta1 = %e + %e i\n", ind, crealf(delta_n1[ind]), cimagf(delta_n1[ind]), crealf(delta_n1[ind]*delta_n1[ind]), cimagf(delta_n1[ind]*delta_n1[ind]) );
              }
              else if (crealf(complex_P1) < 0.0) {
                fprintf( stderr, "BinBispec::ComputeBS_cylindrically_averaged:crealf(complex_P1) < 0.0- ind = %d, delta1 = %e + %e i, delta1*delta1 = %e + %e i\n", ind, crealf(delta_n1[ind]), cimagf(delta_n1[ind]), crealf(delta_n1[ind]*delta_n1[ind]), cimagf(delta_n1[ind]*delta_n1[ind]) );
              }

              if ( crealf(I_n1[ind])*crealf(I_n1[ind]) < 0.0) {
                fprintf( stderr, "BinBispec::ComputeBS_cylindrically_averaged: ind = %d, I_n1*I_n1 = %e\n", ind, crealf(I_n1[ind])*crealf(I_n1[ind]) );
                fprintf( stderr, "BinBispec::ComputeBS_cylindrically_averaged: I1 = %e, I_n1(real) = %e, I_n1(imag) = %e\n", sum_I[0], crealf(I_n1[ind]), cimagf(I_n1[ind]) );
              }*/


	            complex_P2 += (  delta_n2[ind]*delta_n2[ind] );
	            sum_I[1] += ( I_n2[ind]*I_n2[ind] );
              /*if ( (crealf(delta_n2[ind]*delta_n2[ind] ) < 0.0) || (cimagf(delta_n2[ind]*delta_n2[ind] ) < 0.0) ) {

                fprintf( stderr, "BinBispec::ComputeBS_cylindrically_averaged: ind = %d, delta2 = %e + %e i, delta2*delta2 = %e + %e i\n", ind, crealf(delta_n2[ind]), cimagf(delta_n2[ind]), crealf(delta_n2[ind]*delta_n2[ind]), cimagf(delta_n2[ind]*delta_n2[ind])  );
              }
              else if (crealf(complex_P2) < 0.0) {
                fprintf( stderr, "BinBispec::ComputeBS_cylindrically_averaged: crealf(complex_P2) < 0.0 - ind = %d, delta2 = %e + %e i, delta2*delta2 = %e + %e i\n", ind, crealf(delta_n2[ind]), cimagf(delta_n2[ind]), crealf(delta_n2[ind]*delta_n2[ind]), cimagf(delta_n2[ind]*delta_n2[ind])  );
              }*/

	            complex_P3 += (  delta_n3[ind]*delta_n3[ind] );
	            sum_I[2] += ( I_n3[ind]*I_n3[ind] );
              /*if ( (crealf(delta_n3[ind]*delta_n3[ind] ) < 0.0) || (cimagf(delta_n3[ind]*delta_n3[ind] ) < 0.0) ) {
                fprintf( stderr, "BinBispec::ComputeBS_cylindrically_averaged: ind = %d, delta3 = %e + %e i, delta3*delta3 = %e + %e i\n", ind, crealf(delta_n3[ind]), cimagf(delta_n3[ind]), crealf(delta_n3[ind]*delta_n3[ind]), cimagf(delta_n3[ind]*delta_n3[ind]) );
              }
              else if (crealf(complex_P3) < 0.0) {
                fprintf( stderr, "BinBispec::ComputeBS_cylindrically_averaged: crealf(complex_P3) < 0.0 - ind = %d, delta3 = %e + %e i, delta3*delta3 = %e + %e i\n", ind, crealf(delta_n3[ind]), cimagf(delta_n3[ind]), crealf(delta_n3[ind]*delta_n3[ind]), cimagf(delta_n3[ind]*delta_n3[ind]) );
              }*/

              complex_P4 += (  delta_n4[ind]*delta_n4[ind] );
	            sum_I[3] += ( I_n4[ind]*I_n4[ind] );

              complex_P5 += (  delta_n5[ind]*delta_n5[ind] );
	            sum_I[4] += ( I_n5[ind]*I_n5[ind] );

              complex_P6 += (  delta_n6[ind]*delta_n6[ind] );
	            sum_I[5] += ( I_n6[ind]*I_n6[ind] );

	            complex_bi3 += ( delta_n1[ind]*delta_n2[ind]*delta_n3[ind] );
	            sum_I[6] += ( I_n1[ind]*I_n2[ind]*I_n3[ind] );

              // For cylindrical we can also calculate the bispec for K1PERP, -K1LOS cylindrical average using
              complex_bi4 += (  delta_n1[ind]*delta_n2[ind]*delta_n4[ind] );
	            sum_I[7] += ( I_n1[ind]*I_n2[ind]*I_n4[ind] );

              // For cylindrical we can also calculate the bispec for -K1PERP, K1LOS cylindrical average using
              complex_bi5 += (  delta_n1[ind]*delta_n2[ind]*delta_n5[ind] );
	            sum_I[8] += ( I_n1[ind]*I_n2[ind]*I_n5[ind] );

              // For cylindrical we can also calculate the bispec for -K1PERP, K1LOS cylindrical average using
              complex_bi6 += (  delta_n1[ind]*delta_n2[ind]*delta_n6[ind] );
	            sum_I[9] += ( I_n1[ind]*I_n2[ind]*I_n6[ind] );
            }
        }
    }

  if (DEBUG>0)
    fprintf(stderr, "BinBispec::bispec_perp_los: P1sum = %e + %e i, P2sum = %e + %e i, P3sum = %e + %e i, I1 = %e, I2 = %e, I3 = %e\n", crealf(complex_P1), cimagf(complex_P1), crealf(complex_P2), cimagf(complex_P2), crealf(complex_P3), cimagf(complex_P3),  sum_I[0], sum_I[1], sum_I[2]);
  // MAKE NORMALISATIONS FOR SUMMATIONS AND CALCULATE 5.27 (WATKINSON 2015 THESIS)
  // 2a. perform normalisations for power spectra
  for (k=0; k<6; ++k)
    {
      N[k] = sum_I[k]/((double) TOT_NUM_PIXELS) ;
    }
  //fprintf(stderr, "BinBispec::ComputeBS_cylindrically_averaged: N(k1) = %e, N(k1)= %e, sumI = %e.\n", N[0], crealf(sum_I[0]), sum_I[0]);

  P = VOLUME/((double) TOT_NUM_PIXELS); // pow( ((double) TOT_NUM_PIXELS), 3.0 );

  PS[0] = crealf( complex_P1 )*P/N[0]; //( crealf(sum_I[0])/((double) TOT_NUM_PIXELS) ); //sum_deltan[0]
  PS[1] = crealf( complex_P2 )*P/N[1]; //( crealf(sum_I[0])/((double) TOT_NUM_PIXELS) );

  PS[2] = crealf( complex_P3 )*P/N[2]; //( crealf(sum_I[1])/((double) TOT_NUM_PIXELS) ); //sum_deltan[1]
  PS[3] = crealf( complex_P4 )*P/N[3]; //( crealf(sum_I[1])/((double) TOT_NUM_PIXELS) );

  PS[4] = crealf( complex_P5 )*P/N[4]; //( crealf(sum_I[2])/((double) TOT_NUM_PIXELS) ); //sum_deltan[2]
  PS[5] = crealf( complex_P6 )*P/N[5]; //( crealf(sum_I[2])/((double) TOT_NUM_PIXELS) );

  // MAKE NORMALISATIONS FOR SUMMATIONS AND CALCULATE 5.28 (WATKINSON 2015 THESIS)
  // 2b. perform Fourier convention normalisations for bi-spectrum
  // normalise triangle count (return) pointer
  N[6] = sum_I[6]/((double) TOT_NUM_PIXELS);
  N[7] = sum_I[7]/((double) TOT_NUM_PIXELS);
  N[8] = sum_I[8]/((double) TOT_NUM_PIXELS);
  N[9] = sum_I[9]/((double) TOT_NUM_PIXELS);

  // normalise bispectrum (return) pointer
  BS[0] = pow(VOLUME, 2)/((double) TOT_NUM_PIXELS); //pow( ((double) TOT_NUM_PIXELS), 4.0 ); // Fourier norm + 1/Npix from delta function, that cancels in the paper equations
  BS[0] /= ( sum_I[6]/((double) TOT_NUM_PIXELS) ); //number of triangles. Alternatively: //P1 /= ( 8.0*pow(PI,2)*pow(BINFAC,3)*n1_*n2_*n3_ ); //analytical est. of tri. num. (approx)
  BS[0] *= crealf( complex_bi3 ); // Factor in power (sumover delta_1*delta_2*delta_3)

  BS[1] = pow(VOLUME, 2)/((double) TOT_NUM_PIXELS); //pow( ((double) TOT_NUM_PIXELS), 4.0 ); // Fourier norm + 1/Npix from delta function, that cancels in the paper equations
  BS[1] /= ( sum_I[7]/((double) TOT_NUM_PIXELS) ); //number of triangles. Alternatively: //P1 /= ( 8.0*pow(PI,2)*pow(BINFAC,3)*n1_*n2_*n3_ ); //analytical est. of tri. num. (approx)
  BS[1] *= crealf( complex_bi4 ); // Factor in power (sumover delta_1*delta_2*delta_4)

  BS[2] = pow(VOLUME, 2)/((double) TOT_NUM_PIXELS); //pow( ((double) TOT_NUM_PIXELS), 4.0 ); // Fourier norm + 1/Npix from delta function, that cancels in the paper equations
  BS[2] /= ( sum_I[8]/((double) TOT_NUM_PIXELS) ); //number of triangles. Alternatively: //P1 /= ( 8.0*pow(PI,2)*pow(BINFAC,3)*n1_*n2_*n3_ ); //analytical est. of tri. num. (approx)
  BS[2] *= crealf( complex_bi5 ); // Factor in power (sumover delta_1*delta_2*delta_5)

  BS[3] = pow(VOLUME, 2)/((double) TOT_NUM_PIXELS); //pow( ((double) TOT_NUM_PIXELS), 4.0 ); // Fourier norm + 1/Npix from delta function, that cancels in the paper equations
  BS[3] /= ( sum_I[9]/((double) TOT_NUM_PIXELS) ); //number of triangles. Alternatively: //P1 /= ( 8.0*pow(PI,2)*pow(BINFAC,3)*n1_*n2_*n3_ ); //analytical est. of tri. num. (approx)
  BS[3] *= crealf( complex_bi6 ); // Factor in power (sumover delta_1*delta_2*delta_5)

  if (DEBUG>0)
    {
      fprintf(stderr, "BinBispec::bispec_perp_los: P(k1) = %e, P(k2) = %e, P(k3) = %e, B(k1,k2,k3) = %e (c.f. %e).\n", PS[0], PS[1], PS[2],  BS[0], (crealf( complex_bi3 )*pow(VOLUME, 2)/((double) TOT_NUM_PIXELS)/( crealf(sum_I[6])/((double) TOT_NUM_PIXELS) )) );
      fprintf(stderr, "BinBispec::bispec_perp_los: Returning to caller function\n.");
    }
  return 1;
}

double k3_from_theta(double a, double b, double theta)
{
  /*
    Calculates length of 3rd side for a triangle formed
    of sides a, b and c where a and b are separated by an angle
    of theta (in radians). 0<theta<pi
   */

   /*if ( (theta<=0.0)||(theta>=M_PI) )
    {
      fprintf(stderr,"****** ERROR in k3_from_theta !! *******\n The value of theta is outside of the physically restricted range 0<theta<PI/n Aborting........\n");
      return -1;
    }*/
  double c, c2;
  c2 = pow(a,2.0) + pow(b,2.0) - 2.0*b*a*cos(theta);
  c = sqrt(c2);
  return c;
}

double theta_from_tri_sides(double a, double b, double c)
{
  /*
    Calculates angle theta (in radians) between a and b of a triangle with sides a, b and c
    where a and b
   */

  double costheta, theta;

  costheta = (pow(a,2.0) + pow(b,2.0) - pow(c,2.0)) / (2.0*b*a);
  theta = acos(costheta);

  return theta;
}

unsigned long long real_index(int x, int y, int z, int res_y, int res_z)
  {
    // Returns the 1D index storage location of a 3D indexed box element: Unpadded real box.
    return ( (z) + res_z*((y) + res_y*x) );
  }

unsigned long long real_complex_index(int x, int y, int z, int res_y, int res_z)
    {
      // Returns the 1D index storage location of a 3D complex box element assuming data real.
      int z_mid;

      z_mid = ((res_z+0.0)/2.0);
      return ( (z)+(z_mid+1llu)*((y)+res_y*(x)) );
    }

unsigned long long complex_index(int x, int y, int z, int res_y, int res_z)
  {
    // returns 1D index for a 3D complex array
    /*int MID;
    MID = ((res_z+0.0)/2.0);
    return ( (z)+(MID+1llu)*((y)+res_y*(x)) );*/ //cw adjusted as now we are using DFT's so no longer work in half plane only
    return ( (z) + res_z*((y) + res_y*x) );
  }

void free_21cmBS_arrays()
  {
    fftwf_cleanup_threads();
    fftwf_cleanup();

    fftwf_free(box_fft);
    free(pspec), free(Ntri), free(B), free(k3);

    fftwf_free(delta_fft1); fftwf_free(I_fft1);
    fftwf_free(delta_fft2); fftwf_free(I_fft2);
    fftwf_free(delta_fft3); fftwf_free(I_fft3);
    fftwf_free(delta_fft4); fftwf_free(I_fft4);
    fftwf_free(delta_fft5); fftwf_free(I_fft5);
    fftwf_free(delta_fft6); fftwf_free(I_fft6);

    fftwf_free(delta_n1); fftwf_free(delta_n2);
    fftwf_free(delta_n3); fftwf_free(delta_n4);
    fftwf_free(delta_n5); fftwf_free(delta_n6);

    free(I_n1); free(I_n2);
    free(I_n3); free(I_n4);
    free(I_n5); free(I_n6);
  }
