UNAME:=$(shell uname) # (note you might have to make UNAME manually or change it)
GSL_HOME=/share/apps/centos7/gsl/2.4/
INTEL_HOME=/share/apps/centos7/intel/compiler/2018.3-1/

ifeq ($(CXX), icpc) # hpc or other running intel

# C compiler and flags for intel compiler using mkl fftw
CPPFLAGS = -I${GSL_HOME}/include -I${INTEL_HOME}/mkl/include/fftw -mkl=parallel -xP -Ofast -w
LDFLAGS = -L${GSL_HOME}/lib -L${INTEL_HOME}/lib/intel64/ -lgsl -lgslcblas -lstdc++
CC = icc -qopenmp

all: Compute_21cmBS_isoceles Compute_21cmBS_equi Compute_21cmBS_general Compute_21cmPS ComputeBS_cylindrically_averaged

Compute_21cmBS_isoceles: Compute_21cmBS_isoceles.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -fPIC -c Compute_21cmBS_isoceles.c ${LDFLAGS}
				${CC} -shared -Wl,-soname,Compute_21cmBS_isoceles.so.1 -o Compute_21cmBS_isoceles.so Compute_21cmBS_isoceles.o ${LDFLAGS}

Compute_21cmBS_equi: Compute_21cmBS_equi.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -fPIC -c Compute_21cmBS_equi.c ${LDFLAGS}
				${CC} -shared -Wl,-soname,Compute_21cmBS_equi.so.1 -o Compute_21cmBS_equi.so Compute_21cmBS_equi.o ${LDFLAGS}

Compute_21cmBS_general: Compute_21cmBS_general.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -fPIC -c Compute_21cmBS_general.c ${LDFLAGS}
				${CC} -shared -Wl,-soname,Compute_21cmBS_general.so.1 -o Compute_21cmBS_general.so Compute_21cmBS_general.o ${LDFLAGS}

ComputeBS_cylindrically_averaged: ComputeBS_cylindrically_averaged.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -fPIC -c ComputeBS_cylindrically_averaged.c ${LDFLAGS}
				${CC} -shared -Wl,-soname,ComputeBS_cylindrically_averaged.so.1 -o ComputeBS_cylindrically_averaged.so ComputeBS_cylindrically_averaged.o ${LDFLAGS}

Compute_21cmPS: Compute_21cmPS.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -fPIC -c Compute_21cmPS.c ${LDFLAGS}
				${CC} -shared -Wl,-soname,Compute_21cmPS.so.1 -o Compute_21cmPS.so Compute_21cmPS.o ${LDFLAGS}

else ifeq ($(UNAME), Darwin) # Mac OS 10.14
# C compiler and flags
CPPFLAGS = -I/usr/local/include
LDFLAGS = -L/usr/local/lib -lgsl -lgslcblas -lfftw3f -lfftw3f_omp -g -lm -lpthread
LDFLAGS_BS = -L/usr/local/lib -lgsl -lgslcblas -lfftw3f -lfftw3f_omp -g -lpthread -L. -lm -lstdc++
CC = gcc -fopenmp -Ofast

all: Compute_21cmBS_isoceles Compute_21cmBS_equi Compute_21cmBS_general ComputeBS_cylindrically_averaged Compute_21cmPS
#########################################################################

Compute_21cmBS_isoceles: Compute_21cmBS_isoceles.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o Compute_21cmBS_isoceles.so -shared Compute_21cmBS_isoceles.c ${LDFLAGS_BS}

Compute_21cmBS_equi: Compute_21cmBS_equi.c \
	      ${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o Compute_21cmBS_equi.so -shared Compute_21cmBS_equi.c ${LDFLAGS_BS}

Compute_21cmBS_general: Compute_21cmBS_general.c \
			  ${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o Compute_21cmBS_general.so -shared Compute_21cmBS_general.c ${LDFLAGS_BS}

ComputeBS_cylindrically_averaged: ComputeBS_cylindrically_averaged.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o ComputeBS_cylindrically_averaged.so -shared ComputeBS_cylindrically_averaged.c ${LDFLAGS_BS}

Compute_21cmPS: Compute_21cmPS.c \
	      ${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o Compute_21cmPS.so -shared Compute_21cmPS.c ${LDFLAGS}

else ifeq ($(UNAME), Darwin ) # Mac OS 10.15
# C compiler and flags
CPPFLAGS = -I/usr/local/include
LDFLAGS = -L/usr/local/lib -lgsl -lgslcblas -lfftw3f -lfftw3f_omp -g -lm -lpthread
LDFLAGS_BS = -L/usr/local/lib -lgsl -lgslcblas -lfftw3f -lfftw3f_omp -g -lpthread -L. -lm -lstdc++
CC = gcc -fopenmp -Ofast

all: Compute_21cmBS_isoceles Compute_21cmBS_equi Compute_21cmBS_general ComputeBS_cylindrically_averaged Compute_21cmPS
#########################################################################

Compute_21cmBS_isoceles: Compute_21cmBS_isoceles.c \
			${COSMO_FILES} \

			${CC} ${CPPFLAGS} -o Compute_21cmBS_isoceles.so -shared Compute_21cmBS_isoceles.c ${LDFLAGS_BS}

Compute_21cmBS_equi: Compute_21cmBS_equi.c \
			${COSMO_FILES} \

			${CC} ${CPPFLAGS} -o Compute_21cmBS_equi.so -shared Compute_21cmBS_equi.c ${LDFLAGS_BS}

Compute_21cmBS_general: Compute_21cmBS_general.c \
			${COSMO_FILES} \

			${CC} ${CPPFLAGS} -o Compute_21cmBS_general.so -shared Compute_21cmBS_general.c ${LDFLAGS_BS}

ComputeBS_cylindrically_averaged: ComputeBS_cylindrically_averaged.c \
			${COSMO_FILES} \

			${CC} ${CPPFLAGS} -o ComputeBS_cylindrically_averaged.so -shared ComputeBS_cylindrically_averaged.c ${LDFLAGS_BS}

Compute_21cmPS: Compute_21cmPS.c \
			${COSMO_FILES} \

			${CC} ${CPPFLAGS} -o Compute_21cmPS.so -shared Compute_21cmPS.c ${LDFLAGS}

else ifeq ($(UNAME), Linux) # Generic linux OS
# C compiler and flags
CPPFLAGS = -I/home/caw11/local_installs/include
LDFLAGS_BS = -I/home/caw11/local_installs/lib -lgsl -lgslcblas -lfftw3f -lfftw3f_omp -lpthread
CC = gcc -fopenmp -fPIC

all: Compute_21cmBS_isoceles Compute_21cmBS_equi Compute_21cmBS_general ComputeBS_cylindrically_averaged Compute_21cmPS
#########################################################################

Compute_21cmBS_isoceles: Compute_21cmBS_isoceles.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o Compute_21cmBS_isoceles.so -shared Compute_21cmBS_isoceles.c ${LDFLAGS_BS}

Compute_21cmBS_equi: Compute_21cmBS_equi.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o Compute_21cmBS_equi.so -shared Compute_21cmBS_equi.c ${LDFLAGS_BS}

Compute_21cmBS_general: Compute_21cmBS_general.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o Compute_21cmBS_general.so -shared Compute_21cmBS_general.c ${LDFLAGS_BS}

ComputeBS_cylindrically_averaged: ComputeBS_cylindrically_averaged.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o ComputeBS_cylindrically_averaged.so -shared ComputeBS_cylindrically_averaged.c ${LDFLAGS_BS}

Compute_21cmPS: Compute_21cmPS.c \
				${COSMO_FILES} \

				${CC} ${CPPFLAGS} -o Compute_21cmPS.so -shared Compute_21cmPS.c ${LDFLAGS_BS}
else
$(info UNAME="$(UNAME)")
endif

clean:
	rm *.o *.so
