'''
bispec_analysis.py contains wrappers for calling various bispectrum and
power spectrum code packaged with BiFFT.

Each one returns a dictionary containing the associated arrays,
all take a complex or real datasets as input. See the test modules in Tests/
and/or the description at the top of each functions for how to use..

Each function also allows one to ignore modes below a particular klos.
This is useful as there is always a klos limit below which most foreground
removal methods will inherently fail (as for very large modes FG and signal
are indistinguishable).

If you make any changes to the code, run pytest from main folder to run the
tests in Test/. You can override the hardcoded test code by setting REGEN_TEST_DATA = True
in the relevant test script if your changes purposely change the codes output.

FUNCTIONS MENU
--------------
calculate_equi_bipec() -> equilateral bispec as fn of k
calculate_isoc_bipec() -> isoceles bispec for passed k1 as fn of angle (in radians/pi)
						  between k1 and k2 when added (i.e. internal to the triangle)
calculate_general_bipec() -> bispec for passed k1 and k2 as fn of angle (in radians/pi)
							 between k1 and k2 when added (i.e. internal to the triangle)
calculate_cylin_bipec() -> cylindrically-averaged bispectrum for a particular k1perp, k1los and list of k2perp, k2los bins.
						   Nb. This parametrisation is not as stable to sample variance as the rest, the binwidth used in ComputeBS_cylindrically_averaged.c
						   have been set to minimise sample variance using tests with theoretical simulations. In reality your simulation k-resolution
						   might mean that the default is too heavy for your application.
						   However, if you do adjust this binning (L552-560 of ComputeBS_cylindrically_averaged.c) please test its stability.
						   In other words, use this function with extreme care!

calculate_pspec() -> power spectrum as a function of k

noise_err_bispec() -> calculates the bispectrum noise error given the noise is Gaussian
				  and so depends only on the noise power spec.

'''
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!!!!! USER ADJUST THE BELOW FOR YOUR SYSTEM !!!!!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SOURCE_FOLDER="/Users/caw11/_PROGRAMS/BiFFT/src/"
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#from memory_profiler import profile
import pickle

import os, sys
lib_path = os.path.abspath('./')
sys.path.append(lib_path)

import numpy as np
from decimal import *
TWOPLACES=Decimal(10)**-2 # USAGE: Decimal(your_variable).quantize(TWOPLACES)

# Setup ctypes
import ctypes
from ctypes import cdll
from ctypes import c_float,c_int,c_uint,c_double,c_char_p

DEBUG = 0 # 0 Supresses unnecessary print outs, 1 = print basic runtime output, 2 = debug mode, print everything out.


# ------------------------------------------------------------------------------
# Power Spectrum C-module initialising for shared libraries and ctype variables
SharedLibraryLocation = SOURCE_FOLDER+"Compute_21cmPS.so" #/Users/caw11/_PROGRAMS/BiFFT/
Lib_21cmPS = cdll.LoadLibrary(SharedLibraryLocation)
Function_21cmPS = Lib_21cmPS.Compute_21cmPS

# Data structure for the returned data from the C code
class PSPoint(ctypes.Structure):
	_fields_ = [('PSbins', c_int),
	('PS_k', ctypes.POINTER(c_float)), # k
	('PS', ctypes.POINTER(c_float)), # power spectrum P(k)
	('PS_error', ctypes.POINTER(c_float)) # sample variance error Perr(k)
 ];

Function_21cmPS.argtypes = [ctypes.POINTER(PSPoint), ctypes.POINTER(c_float), ctypes.POINTER(c_float), c_double, c_int, c_int, c_int, c_int, c_int, c_float, c_float, c_float]
# Define variables passed in function call
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# Bispectrum isoceles C-module initialising for shared libraries and ctype variables
SharedLibraryLocation = SOURCE_FOLDER+"Compute_21cmBS_isoceles.so" #
Lib_BS_isoc = cdll.LoadLibrary(SharedLibraryLocation)
Function_BS_isoc = Lib_BS_isoc.Compute_21cmBS_isoceles

# Data structure for the returned data from the C code
class PointBSisoc(ctypes.Structure):
	_fields_ = [('BSbins', c_int),
	('BS_k1', ctypes.POINTER(c_float)), # k1
	('BS_k2', ctypes.POINTER(c_float)), # k2
	('BS_k3', ctypes.POINTER(c_double)), # k3
	('BS_theta', ctypes.POINTER(c_float)), # angle in radians/pi (only really useful Compute_21cmBS_isoceles)
	('BS', ctypes.POINTER(c_double)), # real part of the bispec
	('BSimag', ctypes.POINTER(c_double)), # imag part of the bispec. Nb. this  this is always passed back unnormalised as normalised version misbehaves as P~0 for a real field
	('BS_P1', ctypes.POINTER(c_double)), # PS for k1
	('BS_P2', ctypes.POINTER(c_double)), # PS for k2
	('BS_P3', ctypes.POINTER(c_double)), # PS for k3
	('BS_Ntri', ctypes.POINTER(c_double)) # Number of triangles
 ];

# Define variables passed in function call
Function_BS_isoc.argtypes = [ctypes.POINTER(PointBSisoc), ctypes.POINTER(c_float), ctypes.POINTER(c_float), c_int, c_int, c_float, c_double, c_int, c_int, c_int, c_float, c_float, c_float, c_int]
# ------------------------------------------------------------------------------

# Bispectrum equilateral C-module initialising for shared libraries and ctype variables
SharedLibraryLocation = SOURCE_FOLDER+"Compute_21cmBS_equi.so" #/Users/caw11/_PROGRAMS/BiFFT/
Lib_BS_equi = cdll.LoadLibrary(SharedLibraryLocation)
Function_BS_equi = Lib_BS_equi.Compute_21cmBS_equilateral

# Data structure for the returned data from the C code
class PointBSequi(ctypes.Structure):
	_fields_ = [('BSbins', c_int),
	('BS_k1', ctypes.POINTER(c_float)), # k1
	('BS_k2', ctypes.POINTER(c_float)), # k2
	('BS_k3', ctypes.POINTER(c_double)), # k3
	('BS_theta', ctypes.POINTER(c_float)), # angle in radians/pi (only really useful Compute_21cmBS_isoceles)
	('BS', ctypes.POINTER(c_double)), # real part of the bispec
	('BSimag', ctypes.POINTER(c_double)), # imag part of the bispec. Nb. this  this is always passed back unnormalised as normalised version misbehaves as P~0 for a real field
	('BS_P1', ctypes.POINTER(c_double)), # PS for k1
	('BS_P2', ctypes.POINTER(c_double)), # PS for k2
	('BS_P3', ctypes.POINTER(c_double)), # PS for k3
	('BS_Ntri', ctypes.POINTER(c_double)) # Number of triangles
 ];

# Define variables passed in function call
Function_BS_equi.argtypes = [ctypes.POINTER(PointBSequi), ctypes.POINTER(c_float), ctypes.POINTER(c_float), c_int, c_int, c_float, c_double, c_int, c_int, c_int, c_float, c_float, c_float, c_int]
# ------------------------------------------------------------------------------

# Bispectrum general C-module initialising for shared libraries and ctype variables
SharedLibraryLocation = SOURCE_FOLDER+"Compute_21cmBS_general.so" #/Users/caw11/_PROGRAMS/BiFFT/
Lib_BS_general = cdll.LoadLibrary(SharedLibraryLocation)
Function_BS_general = Lib_BS_general.Compute_21cmBS_general

# Data structure for the returned data from the C code
class PointBSgen(ctypes.Structure):
	_fields_ = [('BSbins', c_int),
	('BS_k1', ctypes.POINTER(c_float)), # k1
	('BS_k2', ctypes.POINTER(c_float)), # k2
	('BS_k3', ctypes.POINTER(c_double)), # k3
	('BS_theta', ctypes.POINTER(c_float)), # angle in radians/pi (only really useful Compute_21cmBS_isoceles)
	('BS', ctypes.POINTER(c_double)), # real part of the bispec
	('BSimag', ctypes.POINTER(c_double)), # imag part of the bispec. Nb. this  this is always passed back unnormalised as normalised version misbehaves as P~0 for a real field
	('BS_P1', ctypes.POINTER(c_double)), # PS for k1
	('BS_P2', ctypes.POINTER(c_double)), # PS for k2
	('BS_P3', ctypes.POINTER(c_double)), # PS for k3
	('BS_Ntri', ctypes.POINTER(c_double)) # Number of triangles
 ];

# Define variables passed in function call
Function_BS_general.argtypes = [ctypes.POINTER(PointBSgen), ctypes.POINTER(c_float), ctypes.POINTER(c_float), c_int, c_int, c_float, c_float, c_double, c_int, c_int, c_int, c_float, c_float, c_float, c_int]


# ------------------------------------------------------------------------------

# Bispectrum cylindrically-averaged C-module initialising for shared libraries and ctype variables
SharedLibraryLocation = SOURCE_FOLDER+"ComputeBS_cylindrically_averaged.so"

Lib_BS_cylin = cdll.LoadLibrary(SharedLibraryLocation)
Function_BS_cylin = Lib_BS_cylin.ComputeBS_cylindrically_averaged

# Data structure for the returned data from the C code
class PointBScylin(ctypes.Structure):
	_fields_ = [('BSbins', c_int),
	('BSData_kperp1', ctypes.POINTER(c_float)),
	('BSData_klos1', ctypes.POINTER(c_float)),
	('BSData_kperp2', ctypes.POINTER(c_float)),
	('BSData_klos2', ctypes.POINTER(c_float)),
	('BSData_k3', ctypes.POINTER(c_double)), # avg k3 closing triangles formed by k1 = (k1perp, k1los) and k2 = (k2perp, k2los)
	('BSData_k4', ctypes.POINTER(c_double)), # avg k4 closing triangles formed by k1' = (k1perp, k1los) and k2' = (k2perp, -k2los)
	('BSData_k5', ctypes.POINTER(c_double)), # avg k5 closing triangles formed by k1'' = (k1perp, -k1los) and k2 = (k2perp, k2los)
	('BSData_k6', ctypes.POINTER(c_double)), # avg k6 closing triangles formed by k1''' = (-k1perp, -k1los) and k2 = (k2perp, k2los)
	('BSData_Bk3', ctypes.POINTER(c_double)), # B(k1, k2, k3)
	('BSData_Bk4', ctypes.POINTER(c_double)), # B(k1', k2, k4)
	('BSData_Bk5', ctypes.POINTER(c_double)), # B(k1'', k2, k5)
	('BSData_Bk6', ctypes.POINTER(c_double)), # B(k1''', k2, k6)
	('BSData_P1', ctypes.POINTER(c_double)), # P(k1)
	('BSData_P2', ctypes.POINTER(c_double)), # P(k2)
	('BSData_P3', ctypes.POINTER(c_double)), # P(k3)
	('BSData_P4', ctypes.POINTER(c_double)), # P(k4)
	('BSData_P5', ctypes.POINTER(c_double)), # P(k5)
	('BSData_P6', ctypes.POINTER(c_double)), # P(k6)
	('BSData_Ntri_k3', ctypes.POINTER(c_double)), # num of triangles that contributed to B(k1, k2, k3)
	('BSData_Ntri_k4', ctypes.POINTER(c_double)), # num of triangles that contributed to B(k1', k2, k4)
	('BSData_Ntri_k5', ctypes.POINTER(c_double)), # num of triangles that contributed to B(k1'', k2, k5)
	('BSData_Ntri_k6', ctypes.POINTER(c_double)) # num of triangles that contributed to B(k1''', k2, k6)
  ];

print("Initiating args for bispec code...")

Function_BS_cylin.restype = None
Function_BS_cylin.argtypes = [ctypes.POINTER(PointBScylin), ctypes.POINTER(c_float), ctypes.POINTER(c_float), c_int, c_double , c_double, c_double, c_double, c_double, c_int, c_int, c_int, c_float, c_float, c_float, c_int]
# ------------------------------------------------------------------------------


def complex_to_two_floats(complex_data):
	'''
		This takes a complex dataset and returns two real 1D arrays, one containing
		the real part, another the complex part
	'''
	data_real = np.zeros( complex_data.size )
	data_imag = np.zeros( complex_data.size )
	index_ct = 0
	for i in range(complex_data.shape[0]):
		for j in range(complex_data.shape[1]):
			for k in range(complex_data.shape[2]):
				data_real[ index_ct ] = complex_data[ i, j, k ].real
				data_imag[ index_ct ] = complex_data[ i, j, k ].imag
				index_ct+=1

	return data_real, data_imag

def calculate_equi_bipec(data=None, DIM=None, LEN=None, K_FAC=1.2, kz_los_cut=0.0, normalise=False, num_th=1):

	'''
	data (real or complex array) = datacuboid
	DIM (int array) = dimension of cube on a side; pass as [DIM_X, DIM_Y, DIM_Z] or just [DIM] for cubic data
	LEN (float array) = length of cube on a side; pass as [LEN_X, LEN_Y, LEN_Z] or just [LEN] for cubic data
	K_FAC (float) [opt - 1.2] = factor to decide courseness of k_mag binning
	kz_los_cut (float) [opt - 0.0] = LoS mode below which to ignore pixel contribution to the power spectrum
	normalise (int) [opt - False] = 1 then normalise as per Watkinson 2018, = 2 then make B(k) absolute
	num_th (int) [opt - 1] = how many threads to use in bispec calculation. For 21cmMC or similar only 1 should be used

	Returns a dictionary containing: "theta_over_pi", "k3", "BS", "BSimag", "k1", "k2", "P1", "P2", "P3", "Ntri" -> theta between k1 and k2, corresponing k3, real part of bispec, imag part of bispec, k1, k2, power spec for all three k modes and the number of triangles that contributed to measurement.

	'''

	if ( (data is None) or (DIM is None) or (LEN is None) ):
		print("bispec_analysis.py (calculate_equi_bipec) -> You have not passed something essential, check usage")
	if not( (len(DIM)==3) or (len(DIM)==1) ):
		print("bispec_analysis.py (calculate_equi_bipec) -> DIM should either be a 3D array or a single number if your data is cubic")
	if not( len(DIM)==len(LEN) ):
		print("bispec_analysis.py (calculate_equi_bipec) -> DIM and LEN should have same format, i.e. the should both be a 3D array or a single number if your data is cubic")

	if (DEBUG >= 1):
		print("bispec_analysis.py (calculate_equi_bipec) -> data shape = ", DIM, "data side length = ", LEN)

	complex_data = data.astype(np.complex)

	# define C float datatype
	c_float_p = ctypes.POINTER(ctypes.c_float)

	# Initialise the memory for the pointer structure that will be passed to bispec C code.
	numbins = 50 # Nb. this needs changing if you increase the theta bins in Compute_21cmBS_isoceles.c
	res=PointBSequi()
	res.BS_k1=(ctypes.c_float*numbins)()
	res.BS_k2=(ctypes.c_float*numbins)()
	res.BS_k3=(ctypes.c_double*numbins)()
	res.BS_theta=(ctypes.c_float*numbins)()
	res.BS=(ctypes.c_double*numbins)()
	res.BSimag=(ctypes.c_double*numbins)()
	res.BS_P1=(ctypes.c_double*numbins)()
	res.BS_P2=(ctypes.c_double*numbins)()
	res.BS_P3=(ctypes.c_double*numbins)()
	res.BS_Ntri=(ctypes.c_double*numbins)()

	# We need to load the box into a 1D array in the format of fftwf_complex (i.e. real_0, imag_0, real_1, imag_1.... real_n, imag_n)
	data_real, data_imag = complex_to_two_floats(complex_data = complex_data)

	#print( "fftw_complex loaded box shape = ", sim_fftwf_real.shape, sim_fftwf_imag.shape )
	real_asfloat = data_real.astype( np.float32 )
	data_ctype_real = real_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code
	imag_asfloat = data_imag.astype( np.float32 )
	data_ctype_imag = imag_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code

	if (len(DIM)==3):
		dim_x = DIM[0]
		dim_y = DIM[1]
		dim_z = DIM[2]
		len_x = LEN[0]
		len_y = LEN[1]
		len_z = LEN[2]
	else:
		dim_x = DIM[0]
		dim_y = DIM[0]
		dim_z = DIM[0]
		len_x = LEN[0]
		len_y = LEN[0]
		len_z = LEN[0]

	Function_BS_equi(ctypes.byref(res), data_ctype_real, data_ctype_imag, normalise, 0, K_FAC, kz_los_cut, dim_x, dim_y, dim_z, len_x, len_y, len_z, num_th)
	del data_ctype_real
	del data_ctype_imag # not sure if I need to explicitly do this, but better safe than sorry

	size = []
	size.append(res.BSbins)

	ptr = ctypes.cast( res.BS_theta, ctypes.POINTER(ctypes.c_float * size[0]) )
	theta_over_pi = np.array( np.frombuffer( ptr.contents, dtype=np.float32 ) )

	ptr = ctypes.cast( res.BS_k1, ctypes.POINTER(ctypes.c_float * size[0]) )
	k_1 = np.array( np.frombuffer(ptr.contents, dtype=np.float32) )

	ptr = ctypes.cast( res.BS_k2, ctypes.POINTER(ctypes.c_float * size[0]) )
	k_2 = np.array( np.frombuffer(ptr.contents, dtype=np.float32) )

	ptr = ctypes.cast( res.BS_k3, ctypes.POINTER(ctypes.c_double * size[0]) )
	k_3 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS, ctypes.POINTER(ctypes.c_double * size[0]) )
	bispec = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BSimag, ctypes.POINTER(ctypes.c_double * size[0]) )
	bispec_im = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P1, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_1 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P2, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_2 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P3, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_3 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_Ntri, ctypes.POINTER(ctypes.c_double * size[0]) )
	Ntri = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	output = {"theta_over_pi":theta_over_pi, "k3":k_3, "BS":bispec, "BSimag": bispec_im, "k1": k_1, "k2": k_2, "P1": P_1, "P2": P_2, "P3": P_3, "Ntri": Ntri}

	del ptr
	del res
	ctypes._reset_cache()

	return output

def calculate_isoc_bipec(data=None, DIM=None, LEN=None, k_1=None, kz_los_cut=0.0, normalise=False, num_th=1):

	'''
	data (real or complex array) = datacuboid
	DIM (int array) = dimension of cube on a side; pass as [DIM_X, DIM_Y, DIM_Z] or just [DIM] for cubic data
	LEN (float array) = length of cube on a side; pass as [LEN_X, LEN_Y, LEN_Z] or just [LEN] for cubic data
	k_1 (float) = the k_1 of the isoceles you want calculating k_1=k_2 with k_3 a function of the angle between these
	kz_los_cut (float) [opt - 0.0] = LoS mode below which to ignore pixel contribution to the bispectrum
	normalise (int) [opt - False] = 1 then normalise as per Watkinson 2018, = 2 then make B(k) absolute
	num_th (int) [opt - 1] = how many threads to use in bispec calculation. For 21cmMC or similar only 1 should be used

	Returns a dictionary containing: "theta_over_pi", "k3", "BS", "BSimag", "k1", "k2", "P1", "P2", "P3", "Ntri" -> theta between k1 and k2, corresponing k3, real part of bispec, imag part of bispec, k1, k2, power spec for all three k modes and the number of triangles that contributed to measurement.

	'''

	if ( (data is None) or (DIM is None) or (LEN is None) or (k_1 is None) ):
		print("bispec_analysis.py (calculate_isoc_bipec) -> You have not passed something essential, check usage")
	if not( (len(DIM)==3) or (len(DIM)==1) ):
		print("bispec_analysis.py (calculate_isoc_bipec) -> DIM should either be a 3D array or a single number if your data is cubic")
	if not( len(DIM)==len(LEN) ):
		print("bispec_analysis.py (calculate_isoc_bipec) -> DIM and LEN should have same format, i.e. the should both be a 3D array or a single number if your data is cubic")

	complex_data = data.astype(np.complex)

	if (DEBUG >= 1):
		print("bispec_analysis.py (calculate_isoc_bipec) -> data shape = ", DIM, "data side length = ", LEN)
	# define C float datatype
	c_float_p = ctypes.POINTER(ctypes.c_float)

	# Initialise the memory for the pointer structure that will be passed to bispec C code.
	numbins = 11 # Nb. this needs changing if you increase the theta bins in Compute_21cmBS_isoceles.c
	res=PointBSisoc()
	res.BS_k1=(ctypes.c_float*numbins)()
	res.BS_k2=(ctypes.c_float*numbins)()
	res.BS_k3=(ctypes.c_double*numbins)()
	res.BS_theta=(ctypes.c_float*numbins)()
	res.BS=(ctypes.c_double*numbins)()
	res.BSimag=(ctypes.c_double*numbins)()
	res.BS_P1=(ctypes.c_double*numbins)()
	res.BS_P2=(ctypes.c_double*numbins)()
	res.BS_P3=(ctypes.c_double*numbins)()
	res.BS_Ntri=(ctypes.c_double*numbins)()

	# We need to load the box into a 1D array in the format of fftwf_complex (i.e. real_0, imag_0, real_1, imag_1.... real_n, imag_n)
	data_real, data_imag = complex_to_two_floats(complex_data = complex_data)

	#print( "fftw_complex loaded box shape = ", sim_fftwf_real.shape, sim_fftwf_imag.shape )
	real_asfloat = data_real.astype( np.float32 )
	data_ctype_real = real_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code
	imag_asfloat = data_imag.astype( np.float32 )
	data_ctype_imag = imag_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code

	if (len(DIM)==3):
		dim_x = DIM[0]
		dim_y = DIM[1]
		dim_z = DIM[2]
		len_x = LEN[0]
		len_y = LEN[1]
		len_z = LEN[2]
	else:
		dim_x = DIM[0]
		dim_y = DIM[0]
		dim_z = DIM[0]
		len_x = LEN[0]
		len_y = LEN[0]
		len_z = LEN[0]

	Function_BS_isoc(ctypes.byref(res), data_ctype_real, data_ctype_imag, normalise, 0, k_1, kz_los_cut, dim_x, dim_y, dim_z, len_x, len_y, len_z, num_th)
	del data_ctype_real
	del data_ctype_imag # not sure if I need to explicitly do this, but better safe than sorry

	size = []
	size.append(res.BSbins)

	ptr = ctypes.cast( res.BS_theta, ctypes.POINTER(ctypes.c_float * size[0]) )
	theta_over_pi = np.array( np.frombuffer( ptr.contents, dtype=np.float32 ) )

	ptr = ctypes.cast( res.BS_k1, ctypes.POINTER(ctypes.c_float * size[0]) )
	k_1 = np.array( np.frombuffer(ptr.contents, dtype=np.float32) )

	ptr = ctypes.cast( res.BS_k2, ctypes.POINTER(ctypes.c_float * size[0]) )
	k_2 = np.array( np.frombuffer(ptr.contents, dtype=np.float32) )

	ptr = ctypes.cast( res.BS_k3, ctypes.POINTER(ctypes.c_double * size[0]) )
	k_3 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS, ctypes.POINTER(ctypes.c_double * size[0]) )
	bispec = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BSimag, ctypes.POINTER(ctypes.c_double * size[0]) )
	bispec_im = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P1, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_1 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P2, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_2 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P3, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_3 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_Ntri, ctypes.POINTER(ctypes.c_double * size[0]) )
	Ntri = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	output = {"theta_over_pi":theta_over_pi, "k3":k_3, "BS":bispec, "BSimag": bispec_im, "k1": k_1, "k2": k_2, "P1": P_1, "P2": P_2, "P3": P_3, "Ntri": Ntri}

	del ptr
	del res
	ctypes._reset_cache()

	return output

def calculate_general_bipec(data=None, DIM=None, LEN=None, k_1=None, k_2=None, kz_los_cut=0.0, normalise=False, num_th=1):

	'''
	data (real or complex array) = datacuboid
	DIM (int array) = dimension of cube on a side; pass as [DIM_X, DIM_Y, DIM_Z] or just [DIM] for cubic data
	LEN (float array) = length of cube on a side; pass as [LEN_X, LEN_Y, LEN_Z] or just [LEN] for cubic data
	k_1 (float) = the k_1 of the triangle you want calculating
	k_2 (float) = the k_2 of the triangle you want calculating (k_3 is a function of the internal angle between k1 and k2)
	kz_los_cut (float) [opt - 0.0] = LoS mode below which to ignore pixel contribution to the bispectrum
	normalise (int) [opt - False] = 1 then normalise as per Watkinson 2018, = 2 then make B(k) absolute
	num_th (int) [opt - 1] = how many threads to use in bispec calculation. For 21cmMC or similar only 1 should be used

	Returns a dictionary containing: "theta_over_pi", "k3", "BS", "BSimag", "k1", "k2", "P1", "P2", "P3", "Ntri" -> theta between k1 and k2, corresponing k3, real part of bispec, imag part of bispec, k1, k2, power spec for all three k modes and the number of triangles that contributed to measurement.

	'''

	if ( (data is None) or (DIM is None) or (LEN is None) or (k_1 is None) or (k_2 is None) ):
		print("bispec_analysis.py (calculate_general_bipec) -> You have not passed something essential, check usage")
	if not( (len(DIM)==3) or (len(DIM)==1) ):
		print("bispec_analysis.py (calculate_general_bipec) -> DIM should either be a 3D array or a single number if your data is cubic")
	if not( len(DIM)==len(LEN) ):
		print("bispec_analysis.py (calculate_general_bipec) -> DIM and LEN should have same format, i.e. the should both be a 3D array or a single number if your data is cubic")

	complex_data =  data.astype(np.complex)

	if (DEBUG >= 1):
		print("bispec_analysis.py (calculate_general_bipec) -> data shape = ", DIM, "data side length = ", LEN)
	# define C float datatype
	c_float_p = ctypes.POINTER(ctypes.c_float)

	# Initialise the memory for the pointer structure that will be passed to bispec C code.
	numbins = 11 # Nb. this needs changing if you increase the theta bins in Compute_21cmBS_general.c
	res=PointBSgen()
	res.BS_k1=(ctypes.c_float*numbins)()
	res.BS_k2=(ctypes.c_float*numbins)()
	res.BS_k3=(ctypes.c_double*numbins)()
	res.BS_theta=(ctypes.c_float*numbins)()
	res.BS=(ctypes.c_double*numbins)()
	res.BSimag=(ctypes.c_double*numbins)()
	res.BS_P1=(ctypes.c_double*numbins)()
	res.BS_P2=(ctypes.c_double*numbins)()
	res.BS_P3=(ctypes.c_double*numbins)()
	res.BS_Ntri=(ctypes.c_double*numbins)()

	# We need to load the box into a 1D array in the format of fftwf_complex (i.e. real_0, imag_0, real_1, imag_1.... real_n, imag_n)
	data_real, data_imag = complex_to_two_floats( complex_data=complex_data )

	#print( "fftw_complex loaded box shape = ", sim_fftwf_real.shape, sim_fftwf_imag.shape )
	real_asfloat = data_real.astype( np.float32 )
	data_ctype_real = real_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code
	imag_asfloat = data_imag.astype( np.float32 )
	data_ctype_imag = imag_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code

	if (len(DIM)==3):
		dim_x = DIM[0]
		dim_y = DIM[1]
		dim_z = DIM[2]
		len_x = LEN[0]
		len_y = LEN[1]
		len_z = LEN[2]
	else:
		dim_x = DIM[0]
		dim_y = DIM[0]
		dim_z = DIM[0]
		len_x = LEN[0]
		len_y = LEN[0]
		len_z = LEN[0]

	Function_BS_general(ctypes.byref(res), data_ctype_real, data_ctype_imag, normalise, 0, k_1, k_2, kz_los_cut, dim_x, dim_y, dim_z, len_x, len_y, len_z, num_th)
	del data_ctype_real
	del data_ctype_imag # not sure if I need to explicitly do this, but better safe than sorry

	size = []
	size.append(res.BSbins)

	ptr = ctypes.cast( res.BS_theta, ctypes.POINTER(ctypes.c_float * size[0]) )
	theta_over_pi = np.array( np.frombuffer( ptr.contents, dtype=np.float32 ) )

	ptr = ctypes.cast( res.BS_k1, ctypes.POINTER(ctypes.c_float * size[0]) )
	k_1 = np.array( np.frombuffer(ptr.contents, dtype=np.float32) )

	ptr = ctypes.cast( res.BS_k2, ctypes.POINTER(ctypes.c_float * size[0]) )
	k_2 = np.array( np.frombuffer(ptr.contents, dtype=np.float32) )

	ptr = ctypes.cast( res.BS_k3, ctypes.POINTER(ctypes.c_double * size[0]) )
	k_3 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS, ctypes.POINTER(ctypes.c_double * size[0]) )
	bispec = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BSimag, ctypes.POINTER(ctypes.c_double * size[0]) )
	bispec_im = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P1, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_1 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P2, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_2 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_P3, ctypes.POINTER(ctypes.c_double * size[0]) )
	P_3 = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	ptr = ctypes.cast( res.BS_Ntri, ctypes.POINTER(ctypes.c_double * size[0]) )
	Ntri = np.array( np.frombuffer(ptr.contents, dtype=np.float64) )

	output = {"theta_over_pi":theta_over_pi, "k3":k_3, "BS":bispec, "BSimag": bispec_im, "k1": k_1, "k2": k_2, "P1": P_1, "P2": P_2, "P3": P_3, "Ntri": Ntri}

	del ptr
	del res
	ctypes._reset_cache()

	return output

def calculate_cylin_bipec(data=None, DIM=None, LEN=None, K1_PERP_LOS=[0.1,0.1], K2_PERP=None, K2_LOS=None,  theta_fac=0.05, klos_min_max=[0.0,10.0], kperp_min_max=[0.0,10.0], kz_los_cut=0.0, normalise=False, num_th=1):
	'''
	data (complex or real array) = datacube (currently only supports cube)
	DIM (int array) = dimension of cube on a side; pass as [DIM_X, DIM_Y, DIM_Z] or just [DIM] for cubic data
	LEN (float array) = length of cube on a side; pass as [LEN_X, LEN_Y, LEN_Z] or just [LEN] for cubic data
	normalise (Bool) [opt] = if True then return normalised bispectrum otherwise returns unnormalised
	K1_PERP_LOS (float 2D array) [opt] = perpendicular and los [perp,los] components of k1 vector (which is fixed in calculation)
	K2_PERP (float array) = perpendicular bins for k2 vectors
	K2_LOS (float array) = LOS bins for k2 vectors
	theta_fac (float) =  binning for angle/pi between k1 and k2, where theta_over_pi12*(1+/-theta_fac)
	klos_min_max (float 2D array) [opt] = min and max for LOS element of k-vectors
	kperp_min_max (float 2D array) [opt] = min and max for perpendicular/accross-the-sky element of k-vectors
	kz_los_cut (float)[opt] = LoS mode below which to ignore pixel contribution to the power spectrum

	Returns a dictionary containing "B3", "B4", "B3", "B4", "k2perp", "k2los", "k3", "k4", "k5", "k6"

	These correspond four bispectra sets corresponding to the closure vector produced by four different projections
	of K1_PERP_LOS. k3 where k1 = K1PERP + K1LOS); k4 where k1 = K1PERP - K1LOS;
	k5 where k1 = - K1PERP + K1LOS; k6 where k1 = - K1PERP - K1LOS, :
	It also contains the vector length |k3| etc for each and the k2 perp and los bins associated with each.
	These are all len(K2_PERP)*len(K2_LOS) sized lists that are easily plotted using seaborn:

		df = pd.DataFrame(dict(	k_perp_2=kperp2_list,
								k_los_2=klos2_list,
								bispec=Bispec_k3_list))
		pivot_table = df.pivot("k_los_2", "k_perp_2", "bispec")
		BS_cbar_str = "$B_{\mathrm{normed}}(k_1, k_2, k_3)$"
		SHRINK=0.82 # tweak to make the colorbar the same size as the square plot
		sns.heatmap(data=pivot_table,
					cmap="Spectral", center=0.0,
					ax=ax2, square=True, linewidths=0.03, linecolor='k',
					cbar_kws={"shrink": SHRINK, 'label': BS_cbar_str},
					annot=True)
	I also added on an arrow to illustrate the k1 vector:
		frac_x_arr = (K2_PERP.index(KPERP1)+0.5)/len(K2_PERP)
		frac_y_arr = (K2_LOS.index(KLOS1)+0.5)/len(K2_LOS)
		XY_POS = (-0.11, -0.12) # tweak this so the arrow looks to come from k2 = (0,0)
		ax2.annotate(r"$\vec{k}_1$", xy=(frac_x_arr, frac_y_arr), xycoords='axes fraction', xytext=XY_POS, size=15, textcoords='axes fraction', arrowprops=dict(arrowstyle='simple,head_length=0.5,head_width=0.3,tail_width=0.1'))
	'''
	if ( (K2_PERP is None) or (K2_LOS is None) ):
		print("bispec_analysis.py (calculate_cylin_bipec) -> you need to pass array's for perp and los bins for k2 (K2_PERP, K2_LOS)")
		return
	if ( (data is None) or (DIM is None) or (LEN is None) ):
		print("bispec_analysis.py (calculate_cylin_bipec) -> You have not passed something essential, check usage")
		return
	if not( (len(DIM)==3) or (len(DIM)==1) ):
		print("bispec_analysis.py (calculate_cylin_bipec) -> DIM should either be a 3D array or a single number if your data is cubic")
		return
	if not( len(DIM)==len(LEN) ):
		print("bispec_analysis.py (calculate_cylin_bipec) -> DIM and LEN should have same format, i.e. the should both be a 3D array or a single number if your data is cubic")
		return

	complex_data = data.astype(np.complex)
	if (DEBUG >= 1):
		print("bispec_analysis.py (calculate_cylin_bipec) -> data shape = ", DIM, "data side length = ", LEN)

	# define C float datatype
	c_float_p = ctypes.POINTER(ctypes.c_float)
	data_real, data_imag = complex_to_two_floats(complex_data = complex_data)

	if (len(DIM)==3):
		dim_x = DIM[0]
		dim_y = DIM[1]
		dim_z = DIM[2]
		len_x = LEN[0]
		len_y = LEN[1]
		len_z = LEN[2]
		if not( dim_x==dim_y ):
			print("bispec_analysis.py (calculate_cylin_bipec) -> DIM[0] and DIM[1] should be the same, i.e. the perpendicular dimension should be square")
			return
	else:
		dim_x = DIM[0]
		dim_y = DIM[0]
		dim_z = DIM[0]
		len_x = LEN[0]
		len_y = LEN[0]
		len_z = LEN[0]

	#print("bispec_analysis.py (calculate_cylin_bipec) -> dimensions pased = ", dim_x, dim_y, dim_z)

	# initialise the various variables upon which our calculation relies
	KPERP1 = K1_PERP_LOS[0]
	KLOS1 = K1_PERP_LOS[1]

	kf_los = 2.0*np.pi/len_z
	kf_perp = 2.0*np.pi/len_x

	# Need to exclude modes that are of order the fundamental k
	knyq_los=1.0/2.0*dim_z*kf_los
	knyq_perp=1.0/2.0*dim_x *kf_perp

	#print("knyq_los = ", knyq_los, ", adjusting KMAX if it is larger than the nyqvuist wavenumber")
	#print("knyq_perp = ", knyq_perp, ", adjusting KMAX if it is larger than the nyqvuist wavenumber")

	KMINlos = klos_min_max[0]
	KMAXlos = klos_min_max[1]
	KMINperp = kperp_min_max[0]
	KMAXperp = kperp_min_max[1]

	if (KMAXlos>knyq_los):
		KMAX_LOS = knyq_los
	else:
		KMAX_LOS = KMAXlos

	if (KMAXperp>knyq_perp):
		KMAX_PERP = knyq_perp
	else:
		KMAX_PERP = KMAXperp

	if ( KMINlos<(2.0*kf_los) ):
		KMIN_LOS = (2.0*kf_los)
	else:
		KMIN_LOS = KMINlos

	if ( KMINperp<(2.0*kf_perp) ):
		KMIN_PERP = (2.0*kf_perp)
	else:
		KMIN_PERP = KMINperp

	kperp2_cat=[]
	klos2_cat=[]

	kperp3=[]
	klos3=[]

	kperp4=[]
	klos4=[]

	kperp5=[]
	klos5=[]

	kperp6=[]
	klos6=[]

	Bispec_k3_num=[]
	Bispec_k4_num=[]
	Bispec_k5_num=[]
	Bispec_k6_num=[]

	Bispec_n3_num=[]
	Bispec_n4_num=[]
	Bispec_n5_num=[]
	Bispec_n6_num=[]

	P1_num=[]
	P2_num=[]
	P3_num=[]
	P4_num=[]
	P5_num=[]
	P6_num=[]

	# Initialise the memory for the pointer structure that will be passed to bispec C code.
	numbins = 1

	## Initialise the return data structure with required bins (size of 1 if you are going to manage binning from this script, can apply more bins if you write a binning script with in bispec C code)
	retdata=PointBScylin() # ret -> return

	retdata.BSbins = (ctypes.c_int)()
	retdata.BSData_kperp1 = (ctypes.c_float*numbins)()
	retdata.BSData_klos1 = (ctypes.c_float*numbins)()
	retdata.BSData_kperp2 = (ctypes.c_float*numbins)()
	retdata.BSData_klos2 = (ctypes.c_float*numbins)()
	retdata.BSData_k3 = (ctypes.c_double*numbins)() # avg k3 closing triangles formed by k1 = (k1perp, k1los) and k2 = (k2perp, k2los)
	retdata.BSData_k4 = (ctypes.c_double*numbins)() # avg k4 closing triangles formed by k1' = (k1perp, -k1los) and k2 = (k2perp, k2los)
	retdata.BSData_k5 = (ctypes.c_double*numbins)() # avg k5 closing triangles formed by k1'' = (-k1perp, k1los) and k2 = (k2perp, k2los)
	retdata.BSData_k6 = (ctypes.c_double*numbins)() # avg k6 closing triangles formed by k1''' = (-k1perp, -k1los) and k2 = (k2perp, k2los)
	retdata.BSData_Bk3 = (ctypes.c_double*numbins)() # B(k1, k2, k3)
	retdata.BSData_Bk4 = (ctypes.c_double*numbins)() # B(k1', k2', k4)
	retdata.BSData_Bk5 = (ctypes.c_double*numbins)() # B(k1'', k2, k5)
	retdata.BSData_Bk6 = (ctypes.c_double*numbins)() # B(k1''', k2, k6)
	retdata.BSData_P1 = (ctypes.c_double*numbins)() # P(k1)
	retdata.BSData_P2 = (ctypes.c_double*numbins)() # P(k2)
	retdata.BSData_P3 = (ctypes.c_double*numbins)() # P(k3)
	retdata.BSData_P4 = (ctypes.c_double*numbins)() # P(k4)
	retdata.BSData_P5 = (ctypes.c_double*numbins)() # P(k5)
	retdata.BSData_P6 = (ctypes.c_double*numbins)() # P(k6)
	retdata.BSData_Ntri_k3 = (ctypes.c_double*numbins)() # num of triangles that contributed to B(k1, k2, k3)
	retdata.BSData_Ntri_k4 = (ctypes.c_double*numbins)() # num of triangles that contributed to B(k1', k2, k4)
	retdata.BSData_Ntri_k5 = (ctypes.c_double*numbins)() # num of triangles that contributed to B(k1'', k2, k5)
	retdata.BSData_Ntri_k6 = (ctypes.c_double*numbins)() # num of triangles that contributed to B(k1''', k2, k6)

	count=0
	for j, kperp2 in enumerate(K2_PERP):
		for k, klos2 in enumerate(K2_LOS):
			# We need to load the box into a 1D array in the format of fftwf_complex (i.e. real_0, imag_0, real_1, imag_1.... real_n, imag_n)

			#print( "fftw_complex loaded box shape = ", sim_fftwf_real.shape, sim_fftwf_imag.shape )
			real_asfloat = data_real.astype( np.float32 )
			data_ctype_real = real_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code

			imag_asfloat = data_imag.astype( np.float32 )
			data_ctype_imag = imag_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code

			k1mag = np.sqrt(KPERP1**2 + KLOS1**2)
			k2mag = np.sqrt(kperp2**2 + klos2**2)

			k3_perp = (KPERP1+kperp2)
			k3_los = (KLOS1+klos2)
			k3mag = np.sqrt( k3_perp**2 + k3_los**2)

			#print("kperp2 = ", kperp2, "klos2 = ", klos2, "kmag2 = ", k2mag)

			#print("where k_f = ", 2*np.pi/UParams.BOX_LEN, " and the k_nyq = ", 1.0/2.0*UParams.HII_DIM*2*np.pi/UParams.BOX_LEN)

			if ( (KPERP1 >= KMIN_PERP) and (kperp2 >= KMIN_PERP) and (k3_perp >= KMIN_PERP) and (KLOS1 >= KMIN_LOS) and (klos2 >= KMIN_LOS) and (k3_los >= KMIN_LOS) and (KPERP1 <= KMAX_PERP) and (kperp2 <= KMAX_PERP) and (k3_perp <= KMAX_PERP) and (KLOS1 <= KMAX_LOS) and (klos2 <= KMAX_LOS) and (k3_los <= KMAX_LOS) ):
				#print("calling C-code")
				Function_BS_cylin( ctypes.byref(retdata), data_ctype_real, data_ctype_imag, normalise, KPERP1, KLOS1, kperp2, klos2, theta_fac, dim_x, dim_y, dim_z, len_x, len_y, len_z, num_th )

				del data_ctype_real
				del data_ctype_imag # not sure if I need to explicitly do this, but better safe than sorry

				k3mag=retdata.BSData_k3[0]
				k4mag=retdata.BSData_k4[0]
				k5mag=retdata.BSData_k5[0]
				k6mag=retdata.BSData_k6[0]

				kperp2_cat.append( float(Decimal(kperp2).quantize(TWOPLACES)) )
				klos2_cat.append( float(Decimal(klos2).quantize(TWOPLACES)) )

				kperp3.append( float(Decimal(kperp2 + KPERP1).quantize(TWOPLACES)) )
				klos3.append( float(Decimal(klos2 + KLOS1).quantize(TWOPLACES)) )

				kperp4.append( float(Decimal(kperp2 + KPERP1).quantize(TWOPLACES)) )
				klos4.append( float(Decimal(klos2 - KLOS1).quantize(TWOPLACES)) )

				kperp5.append( float(Decimal(kperp2 - KPERP1).quantize(TWOPLACES)) )
				klos5.append( float(Decimal(klos2 + KLOS1).quantize(TWOPLACES)) )

				kperp6.append( float(Decimal(kperp2 - KPERP1).quantize(TWOPLACES)) )
				klos6.append( float(Decimal(klos2 - KLOS1).quantize(TWOPLACES)) )

				if (DEBUG > 0):
					print( "Magnitude of k2 (exact) = ", k2mag, "Mag of k2 (stored) ", np.sqrt(kperp2_cat[count]**2 + klos2_cat[count]**2) )
					print( "Magnitude of k3 (exact) = ", np.sqrt(kperp3[count]**2 + klos3[count]**2), " k3 (avg from BS code) =  ", k3mag )
					print( "Magnitude of k4 (exact) = ", np.sqrt(kperp4[count]**2 + klos4[count]**2), " k3 (avg from BS code) =  ", k4mag )
					print( "Magnitude of k5 (exact) = ", np.sqrt(kperp5[count]**2 + klos5[count]**2), " k5 (avg from BS code) =  ", k5mag )
					print( "Magnitude of k6 (exact) = ", np.sqrt(kperp6[count]**2 + klos6[count]**2), " k6 (avg from BS code) =  ", k6mag )

				#print("Which should be similar to: ", np.sqrt(kperp2_cat[count]**2 + klos2_cat[count]**2), np.sqrt(kperp3[count]**2 + klos3[count]**2), np.sqrt(kperp4[count]**2 + klos4[count]**2), np.sqrt(kperp5[count]**2 + klos5[count]**2), np.sqrt(kperp6[count]**2 + klos6[count]**2) )

				Bispec_n3_num.append( retdata.BSData_Ntri_k3[0] )
				Bispec_n4_num.append( retdata.BSData_Ntri_k4[0] )
				Bispec_n5_num.append( retdata.BSData_Ntri_k5[0] )
				Bispec_n6_num.append( retdata.BSData_Ntri_k6[0] )

				P1_num.append( retdata.BSData_P1[0] )
				P2_num.append( retdata.BSData_P2[0] )
				if ( ( np.abs(kperp3[count])>=(2.0*kf_perp) ) and ( np.abs(klos3[count])>=(2.0*kf_los) ) and ( np.abs(kperp3[count])>=KMINperp ) and ( np.abs(kperp3[count])<=KMAX_PERP ) and ( np.abs(klos3[count])>=KMIN_LOS ) and ( np.abs(klos3[count])<=KMAX_LOS ) ):
					Bispec_k3_num.append( retdata.BSData_Bk3[0] )
					P3_num.append( retdata.BSData_P3[0] )
				else:
					Bispec_k3_num.append( float('NaN') )
					P3_num.append( float('NaN') )

				if ( ( np.abs(kperp4[count])>=(2.0*kf_perp) ) and ( np.abs(klos4[count])>=(2.0*kf_los) ) and (np.abs(kperp4[count])>=KMIN_PERP) and (np.abs(kperp4[count])<=KMAX_PERP) and (np.abs(klos4[count])>=KMIN_LOS) and (np.abs(klos4[count])<=KMAX_LOS) ):
					Bispec_k4_num.append( retdata.BSData_Bk4[0] )
					P4_num.append( retdata.BSData_P4[0] )
				else:
					Bispec_k4_num.append( float('NaN') )
					P4_num.append( float('NaN') )

				if ( (np.abs(kperp5[count])>=(2*kf_perp)) and (np.abs(klos5[count])>=(2*kf_los)) and (np.abs(kperp5[count])>=KMIN_PERP) and (np.abs(kperp5[count])<=KMAX_PERP) and (np.abs(klos5[count])>=KMIN_LOS) and (np.abs(klos5[count])<=KMAX_LOS) ):
					Bispec_k5_num.append( retdata.BSData_Bk5[0] )
					P5_num.append( retdata.BSData_P5[0] )
				else:
					Bispec_k5_num.append( float('NaN') )
					P5_num.append( float('NaN') )

				if ( (np.abs(kperp6[count])>=(2*kf_perp)) and (np.abs(klos6[count])>=(2*kf_los)) and (np.abs(kperp6[count])>=KMIN_PERP) and (np.abs(kperp6[count])<=KMAX_PERP) and (np.abs(klos6[count])>=KMIN_LOS) and (np.abs(klos6[count])<=KMAX_LOS) ):
					Bispec_k6_num.append( retdata.BSData_Bk6[0] )
					P6_num.append( retdata.BSData_P6[0] )
				else:
					Bispec_k6_num.append( float('NaN') )
					P6_num.append( float('NaN') )

				'''if (normalise > 0):
					#print("performing normalisation on P(k)...")
					P1_num.append( retdata.BSData_P1[0]*(k1mag**3/(2*np.pi**2)) )
					P2_num.append( retdata.BSData_P2[0]*(k2mag**3/(2*np.pi**2)) )
				else:
					P1_num.append( retdata.BSData_P1[0] )
					P2_num.append( retdata.BSData_P2[0] )

				print( "before normalisation step B = ", retdata.BSData_Bk3[0], "and P1, P2, P3 = ", retdata.BSData_P1[0], retdata.BSData_P2[0], retdata.BSData_P3[0] )

				if ( ( np.abs(kperp3[count])>=(2.0*kf_perp) ) and ( np.abs(klos3[count])>=(2.0*kf_los) ) and ( np.abs(kperp3[count])>=KMINperp ) and ( np.abs(kperp3[count])<=KMAX_PERP ) and ( np.abs(klos3[count])>=KMIN_LOS ) and ( np.abs(klos3[count])<=KMAX_LOS ) ):
					if (normalise > 0):
						print("normalising, kmags = ", k1mag, k2mag, k3mag)
						P3_num.append( retdata.BSData_P3[0]*(k3mag**3/(2*np.pi**2)) )
						if (normalise==1):
							Bispec_k3_num.append( retdata.BSData_Bk3[0]*(k1mag*k2mag*k3mag)**3/(2*np.pi**2)**2 )
						elif (normalise==2):
							Bispec_k3_num.append( retdata.BSData_Bk3[0]/(np.sqrt( (retdata.BSData_P1[0]*retdata.BSData_P2[0]*retdata.BSData_P3[0])/(k1mag*k2mag*k3mag) )) )
					else:
						Bispec_k3_num.append( retdata.BSData_Bk3[0] )
						P3_num.append( retdata.BSData_P3[0] )
					print("after normalisation step B = ",Bispec_k3_num[count], retdata.BSData_Bk3[0])
				else:
					print("k3 either greater than kf or outside the k-range we are considering. k3 = ", kperp3[count], klos3[count])
					Bispec_k3_num.append( float('NaN') )
					P3_num.append( float('NaN') )

				if ( ( np.abs(kperp4[count])>=(2.0*kf_perp) ) and ( np.abs(klos4[count])>=(2.0*kf_los) ) and (np.abs(kperp4[count])>=KMIN_PERP) and (np.abs(kperp4[count])<=KMAX_PERP) and (np.abs(klos4[count])>=KMIN_LOS) and (np.abs(klos4[count])<=KMAX_LOS) ):
					if (normalise > 0):
						P4_num.append( retdata.BSData_P4[0]*(k4mag**3/(2*np.pi**2)) )
						if (normalise==1):
							Bispec_k4_num.append( retdata.BSData_Bk4[0]*(k1mag*k2mag*k4mag)**3/(2*np.pi**2)**2 )
						elif (normalise==2):
							Bispec_k4_num.append( retdata.BSData_Bk4[0]/(np.sqrt( (retdata.BSData_P1[0]*retdata.BSData_P2[0]*retdata.BSData_P4[0])/(k1mag*k2mag*k4mag) )) )
					else:
						print("k4 either greater than kf or outside the k-range we are considering. k4 = ", kperp4[count], klos4[count])
						Bispec_k4_num.append( retdata.BSData_Bk4[0] )
						P4_num.append( retdata.BSData_P4[0] )
				else:
					Bispec_k4_num.append( float('NaN') )
					P4_num.append( float('NaN') )

				if ( (np.abs(kperp5[count])>=(2*kf_perp)) and (np.abs(klos5[count])>=(2*kf_los)) and (np.abs(kperp5[count])>=KMIN_PERP) and (np.abs(kperp5[count])<=KMAX_PERP) and (np.abs(klos5[count])>=KMIN_LOS) and (np.abs(klos5[count])<=KMAX_LOS) ):
					if (normalise > 0):
						P5_num.append( retdata.BSData_P5[0]*(k5mag**3/(2*np.pi**2)) )
						if (normalise==1):
							Bispec_k5_num.append( retdata.BSData_Bk5[0]*(k1mag*k2mag*k5mag)**3/(2*np.pi**2)**2 )
						elif (normalise==2):
							Bispec_k5_num.append( retdata.BSData_Bk5[0]/(np.sqrt( (retdata.BSData_P1[0]*retdata.BSData_P2[0]*retdata.BSData_P5[0])/(k1mag*k2mag*k5mag) )) )
					else:
						Bispec_k5_num.append( retdata.BSData_Bk5[0] )
						P5_num.append( retdata.BSData_P5[0] )
				else:
					print("k5 either greater than kf or outside the k-range we are considering. k5 = ", kperp5[count], klos5[count])
					Bispec_k5_num.append( float('NaN') )
					P5_num.append( float('NaN') )

				if ( (np.abs(kperp6[count])>=(2*kf_perp)) and (np.abs(klos6[count])>=(2*kf_los)) and (np.abs(kperp6[count])>=KMIN_PERP) and (np.abs(kperp6[count])<=KMAX_PERP) and (np.abs(klos6[count])>=KMIN_LOS) and (np.abs(klos6[count])<=KMAX_LOS) ):
					if (normalise > 0):
						P6_num.append( retdata.BSData_P6[0]*(k6mag**3/(2*np.pi**2)) )
						if (normalise==1):
							Bispec_k6_num.append( retdata.BSData_Bk6[0]*(k1mag*k2mag*k6mag)**3/(2*np.pi**2)**2 )
						elif (normalise==2):
							Bispec_k6_num.append( retdata.BSData_Bk6[0]/(np.sqrt( (retdata.BSData_P1[0]*retdata.BSData_P2[0]*retdata.BSData_P6[0])/(k1mag*k2mag*k6mag) )) )
					else:
						Bispec_k6_num.append( retdata.BSData_Bk6[0] )
						P6_num.append( retdata.BSData_P6[0] )
				else:
					print("k6 either greater than kf or outside the k-range we are considering. k6 = ", kperp6[count], klos6[count])
					Bispec_k6_num.append( float('NaN') )
					P6_num.append( float('NaN') )
				'''
				count+=1

			# If even our k1 and k2 los or perp choices are not sensible then append NANs for everything
			else:
				print("not even calling bispec because k2 or k3 are outside the k-range we are considering... ")
				print( "k2perp = ", kperp2, "k2los = ", klos2, "k3perp = ", k3_perp, "k3los = ", k3_los )
				print("which falls outside LOS [min, max] =",KMIN_LOS,  KMAX_LOS, " and PERP [min, max] =", KMIN_PERP,  KMAX_PERP)

				kperp2_cat.append( float(Decimal(kperp2).quantize(TWOPLACES)) )
				klos2_cat.append( float(Decimal(klos2).quantize(TWOPLACES)) )

				kperp3.append( float(Decimal(kperp2 + KPERP1).quantize(TWOPLACES)) )
				klos3.append( float(Decimal(klos2 + KLOS1).quantize(TWOPLACES)) )

				kperp4.append( float(Decimal(kperp2 + KPERP1).quantize(TWOPLACES)) )
				klos4.append( float(Decimal(klos2 - KLOS1).quantize(TWOPLACES)) )

				kperp5.append( float(Decimal(kperp2 - KPERP1).quantize(TWOPLACES)) )
				klos5.append( float(Decimal(klos2 + KLOS1).quantize(TWOPLACES)) )

				kperp6.append( float(Decimal(kperp2 - KPERP1).quantize(TWOPLACES)) )
				klos6.append( float(Decimal(klos2 - KLOS1).quantize(TWOPLACES)) )

				Bispec_k3_num.append( float('NaN') )
				Bispec_k4_num.append( float('NaN') )
				Bispec_k5_num.append( float('NaN') )
				Bispec_k6_num.append( float('NaN') )

				Bispec_n3_num.append( float('NaN') )
				Bispec_n4_num.append( float('NaN') )
				Bispec_n5_num.append( float('NaN') )
				Bispec_n6_num.append( float('NaN') )

				P1_num.append( float('NaN') )
				P2_num.append( float('NaN') )
				P3_num.append( float('NaN') )
				P4_num.append( float('NaN') )
				P5_num.append( float('NaN') )
				P6_num.append( float('NaN') )

				count+=1
	#del retdata
	ctypes._reset_cache()

	output = {"B3":Bispec_k3_num, "B4":Bispec_k4_num, "B5":Bispec_k5_num, "B6":Bispec_k6_num, "k2perp":kperp2_cat, "k2los":klos2_cat, "k3los":klos3, "k4los":klos4, "k5los":klos5, "k6los":klos6, "k3perp":kperp3, "k4perp":kperp4, "k5perp":kperp5, "k6perp":kperp6, "triCt3":Bispec_n3_num, "triCt4":Bispec_n4_num, "triCt5":Bispec_n5_num, "triCt6":Bispec_n6_num, "P1":P1_num, "P2":P2_num,"P3":P3_num,"P4":P4_num,"P5":P5_num, "P6":P6_num}

	return output

def calculate_pspec(data=None, DIM=None, LEN=None, normalise=False, kz_los_cut=0.0):

	'''
	data (real or complex array) = datacuboid
	DIM (int array) = dimension of cube on a side; pass as [DIM_X, DIM_Y, DIM_Z] or just [DIM] for cubic data
	LEN (float array) = length of cube on a side; pass as [LEN_X, LEN_Y, LEN_Z] or just [LEN] for cubic data
	normalise (Bool) [opt] = if True then return dimensionless power spectrum
	klos_min_max (float 2D array)
	kperp_min_max (float 2D array)
	kz_los_cut (float)[opt] = LoS mode below which to ignore pixel contribution to the power spectrum

	Returns a dictionary containing: "k", "Pk", "Pkerr", "PSbins" -> k-bins, power spec, sample var err and num bins
	'''

	if ( (data is None) or (DIM is None) or (LEN is None) ):
		print("bispec_analysis.py (calculate_pspec) -> You have not passed something essential, check usage")
	if not( (len(DIM)==3) or (len(DIM)==1) ):
		print("bispec_analysis.py (calculate_isoc_bipec) -> DIM should either be a 3D array or a single number if your data is cubic")
	if not( len(DIM)==len(LEN) ):
		print("bispec_analysis.py (calculate_isoc_bipec) -> DIM and LEN should have same format, i.e. the should both be a 3D array or a single number if your data is cubic")

	complex_data = data.astype(np.complex)

	if (DEBUG >= 1):
		print( "bispec_analysis.py (calculate_pspec) -> Running power spectrum for DIM = ", DIM, ", box_len = ", LEN )

	# define C float datatype
	c_float_p = ctypes.POINTER(ctypes.c_float)

	# Initialise the memory for the pointer structure that will be passed to bispec C code.
	numbins = 50 # Nb. this needs changing if you increase the number of k bins within Compute_21cmPS.c
	res=PSPoint()
	res.PS_k = (ctypes.c_float*numbins)()
	res.PS = (ctypes.c_float*numbins)()
	res.PS_error = (ctypes.c_float*numbins)()

	# We need to load the box into a 1D array in the format of fftwf_complex (i.e. real_0, imag_0, real_1, imag_1.... real_n, imag_n)
	data_real, data_imag = complex_to_two_floats(complex_data = complex_data)

	#print( "fftw_complex loaded box shape = ", sim_fftwf_real.shape, sim_fftwf_imag.shape )
	real_asfloat = data_real.astype( np.float32 )
	data_ctype_real = real_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code
	imag_asfloat = data_imag.astype( np.float32 )
	data_ctype_imag = imag_asfloat.ctypes.data_as( c_float_p ) # make a ctype float for C bispec code

	if (len(DIM)==3):
		dim_x = DIM[0]
		dim_y = DIM[1]
		dim_z = DIM[2]
		len_x = LEN[0]
		len_y = LEN[1]
		len_z = LEN[2]
	else:
		dim_x = DIM[0]
		dim_y = DIM[0]
		dim_z = DIM[0]
		len_x = LEN[0]
		len_y = LEN[0]
		len_z = LEN[0]

	Function_21cmPS( ctypes.byref(res), data_ctype_real, data_ctype_imag, kz_los_cut, 0, False, dim_x, dim_y, dim_z, len_x, len_y, len_z)

	del data_ctype_real # not sure if we need to explicitly do these, but no harm innit
	del data_ctype_imag

	size = []
	size.append(res.PSbins)

	ptr = ctypes.cast( res.PS_k, ctypes.POINTER(ctypes.c_float * size[0]) )
	PS_k = np.array( np.frombuffer( ptr.contents, dtype=np.float32 ) )

	ptr = ctypes.cast( res.PS, ctypes.POINTER(ctypes.c_float * size[0]) )
	PS = np.array( np.frombuffer(ptr.contents, dtype=np.float32) )

	ptr = ctypes.cast( res.PS_error, ctypes.POINTER(ctypes.c_float * size[0]) )
	PS_error = np.array( np.frombuffer(ptr.contents, dtype=np.float32) )

	if (normalise is True):
		Pk_normed = np.zeros_like(PS)
		err_normed = np.zeros_like(PS_error)
		for i in range( len(PS) ):
			Pk_normed[i] = ( PS[i] * PS_k[i]**3 / (2 * np.pi ** 2) )
			err_normed[i] = (PS_error[i] * PS_k[i]**3 / (2 * np.pi ** 2)  )
	else:
		Pk_normed = PS
		err_normed = PS_error

	output = {"k":PS_k, "Pk":Pk_normed, "Pkerr":err_normed, "PSbins":size[0]}

	del ptr
	del res
	ctypes._reset_cache()

	return output

def noise_err_bispec(k1=None, k2=None, k3=None, Pn1=None, Pn2=None, Pn3=None, binwidth=1, L=None):

	'''
		USAGE: Berr_n = noise_err_bispec(k1, k2, k3, P1, P2, P3, binwidth, L):

		RETURNS:
		error on B(k_1, k_2, k_3)

		ARGS:
		k_1, k_2, k_3 - should be in 1/Mpc

		P1, P2, P3 - noise power spectrum for k_1, k_2, k_3

		binwidth - number of fundamental k-pixels, k_f, that you used a bidwidth in your measurement of B(k1, k2, k3)

		L - Size (in Mpc)of a side for the datacube/FoV from which you are measuring the bispectrum,
			should be a scalar if assuming cubic data or [Lx, Ly, Lz] otherwise.

	'''
	if ( (k1 is None) or (k2 is None) or (k3 is None) or (Pn1 is None) or (Pn2 is None) or (Pn3 is None) or (L is None) ):
		print("BiFFT: bispec_analysis.py -> USAGE ERROR: Berr_n = noise_err_bispec(k1, k2, k3, P1, P2, P3, binwidth, L)")

	if (len(L)==3):
		Lx = L[0]
		Ly = L[1]
		Lz = L[2]
	else:
		Lx = L[0]
		Ly = L[0]
		Lz = L[0]

	kf_x = 2.0*np.pi/float(Lx) # Fundamental k-mode of dataset kf = 2pi/L
	kf_y = 2.0*np.pi/float(Ly)
	kf_z = 2.0*np.pi/float(Lz)

	kf = max( kf_x, kf_y, kf_z )

	# Calculate the number of fundamental triangles in units of kf^3 that will go into the a measurement of B(k1, k2, k3)
	Vb = 8.0*np.pi**2*k1*k2*k3*binwidth**3*(kf_x*kf_y*kf_z)

	# Now work out adjustment to correct for redundant triangles in isoc and equi configs
	if ( (abs(k1-k2)<=(binwidth*kf)) and (abs(k1-k3)<=(binwidth*kf)) and (abs(k3-k2)<=(binwidth*kf))):
		#print "bispec_err.py -> Configuration is equilateral"
		s123 = 6.0
	elif ( (abs(k1-k2)<=(binwidth*kf)) or (abs(k1-k3)<=(binwidth*kf)) or (abs(k3-k2)<=(binwidth*kf))):
		#print "bispec_err.py -> Configuration is isoceles"
		s123 = 2.0
	else:
		#print "bispec_err.py -> Configuration is plain ol' boring, nothing to shout about, triangle :("
		s123 = 1.0

	''' ----------- Calculate the noise STD ----------
		(on the bispectrum associated with k1, k2, k3)
	'''
	# Now calculate the noise STD
	Bvar = (kf_x*kf_y*kf_z)*(s123/Vb)*Pn1*Pn2*Pn3
	Berr = np.sqrt(Bvar)

	return Berr

def main():
	"Main() -> BiFFT can be used to calculate various bispectrum and power spectrum code packaged with BiFFT. \
	See bispec_analysis.py for details on how to use."
	BOX_DIM = 64
	BOX_LEN = BOX_DIM*2.0

	TEST_DATA_FNAME = 'Tests/brightness_temp_test_data.pkl'
	f = open(TEST_DATA_FNAME, 'rb')
	dT_box = np.array( pickle.load(f) )
	f.close()
	dT_complex = dT_box.astype(np.complex)
	BS_isoc_dict = calculate_isoc_bipec(complex_data=dT_complex, DIM=[BOX_DIM], LEN=[BOX_LEN], k_1=0.15, normalise=True)

if __name__ == '__main__':
	main()
