#include <omp.h>
//#include <time.h>
//#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <limits.h>
#include <stdlib.h>
#include <complex.h>
#include <fftw3.h>

#include <gsl/gsl_integration.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_errno.h>

// A module for constructing the 21cm bispectrum using C code directly accessible from Python
// To be written by: Catherine Watkinson (still contains power spectrum 7th March 2019)

#define PI (double) (3.14159265358979323846264338327)
#define TWOPI (double) (2.0*PI)

// For initialising/constructing the power spectrum data
int NUM_BINS;
int X_DIM, Y_DIM, Z_DIM, i, j, k, x, y, z;
unsigned long long X_MID, Y_MID, Z_MID;
int NUM_TH, SCALE_FAC, USE_IND;
unsigned long long TOT_NUM_PIXELS; //, HII_TOT_FFT_NUM_PIXELS, HII_KSPACE_NUM_PIXELS;
float X_LEN, Y_LEN, Z_LEN;
double VOLUME, DELTA_K_X, DELTA_K_Y, DELTA_K_Z, KF_X, KF_Y, KF_Z, KX_NYQ, KY_NYQ, KZ_NYQ, KF, K_NYQ;

fftwf_complex *delta_fft1, *I_fft1, *delta_fft2, *I_fft2, *delta_fft3, *I_fft3;
fftwf_complex *delta_n1, *I_n1, *delta_n2, *I_n2, *delta_n3, *I_n3;
fftwf_complex *box_fft;
double *B, *k3, *Ntri, *pspec;

void init_21cmBS_arrays(int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z, int num_th);
int nbins_isoceles(float K1);
int bispec_k3_bin(fftwf_complex *BOX_FFT1, double K1, double K2, double K3min, double K3max, double k_los_cut, double *PS, double *BS, double *N, double *k3_avg);
double k3_from_theta(double a, double b, double theta);
unsigned long long real_index(int x, int y, int z, int res_y, int res_z);
unsigned long long complex_index(int x, int y, int z, int res_y, int res_z);
void free_21cmBS_arrays();

// Data structure to be returned to Python using Ctypes
struct ReturnData{
    int BSbins;
    float *BSData_k1;
    float *BSData_k2;
    double *BSData_k3;
    float *BSData_theta;
    double *BSData;
    double *BSData_imag; // Note that this is always passed back unnormalised as normalised version misbehaves as P~0 for a real field
    double *BSData_P1;
    double *BSData_P2;
    double *BSData_P3;
    double *BSData_Ntri;
};

void init_21cmBS_arrays(int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z, int num_th) {

  /******************* simulated cube details *********************/
  X_DIM = DIM_X;
  Y_DIM = DIM_Y;
  Z_DIM = DIM_Z;

  X_LEN = L_X;
  Y_LEN = L_Y;
  Z_LEN = L_Z;

  NUM_TH = num_th; // Number of threads to be used

  /************ Indexing definitions (replacing macros from 21cmFAST) *************/
  X_MID = ((X_DIM+0.0)/2.0);
  Y_MID = ((Y_DIM+0.0)/2.0);
  Z_MID = ((Z_DIM+0.0)/2.0);

  TOT_NUM_PIXELS = (X_DIM*Y_DIM*Z_DIM);
  //HII_TOT_FFT_NUM_PIXELS = (X_DIM*Y_DIM*2llu*(Z_MID+1llu)); //cw relics of old FFT version
  //HII_KSPACE_NUM_PIXELS = (X_DIM*Y_DIM*(Z_MID+1llu));

  VOLUME = (X_LEN*Y_LEN*Z_LEN);

  KF_X = ( (2.0*M_PI)/X_LEN );
  KF_Y = ( (2.0*M_PI)/Y_LEN );
  KF_Z = ( (2.0*M_PI)/Z_LEN );
  KF = fmin( fmin(KF_X, KF_Y), fmin(KF_X, KF_Z) );//( (2.0*M_PI)/BOX_LEN );

  // K_NYQ: largest K-mode that can be reasonably sampled according to Nyqvuist theorm/limit
  KX_NYQ = 1.0/2.0*X_DIM*KF_X;
  KY_NYQ = 1.0/2.0*Y_DIM*KF_Y;
  KZ_NYQ = 1.0/2.0*Z_DIM*KF_Z;
  K_NYQ = fmin( fmin(KX_NYQ, KY_NYQ),  fmin(KY_NYQ, KZ_NYQ) );//1.0/2.0*HII_DIM*KF;

  // Number of triangle bins
  //NUM_BINS = 10;

  pspec = (double *) malloc(sizeof(double)*6);
  Ntri = (double *) malloc(sizeof(double)*4);
  B = (double *) malloc(sizeof(double)*2);
  k3 = (double *) malloc(sizeof(double)*1);

  box_fft = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);

  delta_fft1 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS); // replaced HII_KSPACE_NUM_PIXELS in this and following five mallocs
  I_fft1 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);

  delta_fft2 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_fft2 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);

  delta_fft3 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_fft3 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);

  delta_n1 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n1 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);

  delta_n2 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n2 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);

  delta_n3 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
  I_n3 = (fftwf_complex *) fftwf_malloc(sizeof(fftwf_complex)*TOT_NUM_PIXELS);
}

void Compute_21cmBS_isoceles(struct ReturnData *DataToBeReturned, float *box_real, float *box_imag, int NORM, int LightCone, float K1, double K_LOS_CUT, int DIM_X, int DIM_Y, int DIM_Z, float L_X, float L_Y, float L_Z, int num_th)
{
    /*
     Calculates the bispectrum for the isoceles k-triangle configuration as a function of angle betweek k1 and k2
     (float) *box: Cubic box of data from which the 21cm BS is to be constructed. NOTE: last axis should correspond to the LOS or frequency axis
     (int) NORM: Whether to calculate normalised bispectrum (1) or not (0) - see "normalised bispectrum" of Watkinson+ 2018 https://academic.oup.com/mnras/advance-article/doi/10.1093/mnras/sty2740/5129150
     (int) LIGHTCONE: Whether the cubic box is co-eval (0) or light-cone (1)
     (float) K1: decides length of k1=k2 to calculate the bispectrum for.
     (int) DIM_X, *_Y, *_Z: Number of voxels per side length for x, y, z axis
     (float) L_X, *_Y, *_Z: Length of box in Mpc (comoving) for x, y, z axis
     (int) num_th - number of threads to use for fftws
    */

    //cw add in dependence on DIM_X, *_Y, *_Z and LEN_X , *_Y, *_Z
    unsigned long long RSPACE_NUM_PIXELS, KSPACE_NUM_PIXELS, DIM_MID; //TOT_FFT_NUM_PIXELS,

    DIM_MID = DIM_Z/2;

    RSPACE_NUM_PIXELS = (unsigned long long)( DIM_X*DIM_Y*DIM_Z );
    //KSPACE_NUM_PIXELS = ( (unsigned long long)(DIM_BOX*DIM_BOX*(DIM_MID+1llu)) ); // Replaced to work with DFT
    KSPACE_NUM_PIXELS = ( (unsigned long long)(DIM_X*DIM_Y*DIM_Z) );

    double ave;
    int n_x, n_y, n_z;
    float k_x, k_y, k_z, k_mag;
    unsigned long long ct;

    // ------ INITIALISATION -------
    fftwf_plan plan;
    double theta, k3min, k3max, k_min, k_nyq;
    float k1, k2;
    char filename[1000];
    int N_bins, counter;
    float theta_over_pie[11]={0.01, 0.05, 0.1, 0.2, 0.33333333333333333333333, 0.4, 0.5, 0.6, 0.7, 0.85, 0.95};
    N_bins=11;

    // Initialise the power spectrum arrays and the other variables we will use
    init_21cmBS_arrays(DIM_X, DIM_Y, DIM_Z, L_X, L_Y, L_Z, num_th);

    k_min = (KF+0.0);
    k_nyq = (K_NYQ+0.0);

    //fprintf(stderr, "BinBispec::bispec_from_file -> Fourier transforming data to k-space.\n");
    // Set fftw threading parameters
    if (fftwf_init_threads()==0){
      fprintf(stderr, "BinBispec::Compute_21cmBS_isoceles: ERROR: problem initializing fftwf threads...\nAborting...\n");
    }

    omp_set_num_threads(NUM_TH);
    //omp_set_dynamic(false);
    fftwf_plan_with_nthreads(NUM_TH);//*/

    // Fill FFT-array with the fluctuation data
    //fprintf(stderr, "Compute_21cmBS_isoceles: Loading ffted box\n");
    for (i=0; i<DIM_X; i++){
        for (j=0; j<DIM_Y; j++){
            for (k=0; k<DIM_Z; k++){
                box_fft[complex_index(i, j, k, Y_DIM, Z_DIM)] = box_real[complex_index(i, j, k, Y_DIM, Z_DIM)]/(RSPACE_NUM_PIXELS+0.0)+box_imag[complex_index(i, j, k, Y_DIM, Z_DIM)]*I/(RSPACE_NUM_PIXELS+0.0); //cw converted to interpret input box and complex
                // Note: we include a 1/N factor for the scaling after the fft, the V contribution is taken care of in the final normalisation of B and P to avoid numerical noise stopping unsampled pixels being excluded
            }
        }
    }

    // Perform FFT
    // The noise can be complex so we can no longer just deal with real FFTs, hence we now use:
    //fprintf(stderr, "Compute_21cmBS:: performing DFT\n.");
    plan = fftwf_plan_dft_3d(DIM_X, DIM_Y, DIM_Z, (fftwf_complex *)box_fft, (fftwf_complex *)box_fft, FFTW_FORWARD, FFTW_ESTIMATE);
    fftwf_execute(plan);
    fftwf_destroy_plan(plan);

    // ------ OPEN FILE CONTAINING CONFIGS & LOOP OVER LINES-------
    k1=K1;
    k2=K1;
    //fprintf(stderr, "Compute_21cmBS_isoceles: entering binning loop \n");
    counter=0;
    for (i=0; i<N_bins; i++)
      {
        //fprintf(stderr, "BinBispec::Compute_21cmBS_isoceles -> %.2f\t %.2f\t %.2f\t i = %d \n", k1, k2, theta_over_pie[i], i);
        // Calculate the max and min of k3 with a bin of 5% of theta under consideration
        theta = ( theta_over_pie[i] - (theta_over_pie[i]*0.025) )*M_PI; //cw - do we want to expand/reduce this bin at all? Currently 2.5% of a circle. Also, do we want to bin in a different co-ordinate system.
        k3min = k3_from_theta(k1, k2, theta);

        theta = ( theta_over_pie[i] + (theta_over_pie[i]*0.025) )*M_PI; //cw - do we want to expand/reduce this bin at all? Currently 2.5% of a circle. Also, do we want to bin in a different co-ordinate system.
        k3max = k3_from_theta(k1, k2, theta);

        if ( ( (k3min - k_min)>=(1.25*k_min) ) && (k3max < k_nyq) )
          {
            // ------ CALCULATE BISPECTRUM -------
            // Call bispec_k3_bin and output the binned bispectrum (with bin on theta_fac defined by DTHETA_FAC)
            //fprintf(stderr, "Compute_21cmBS_isoceles: calling bispec_k3_bin()\n");
            bispec_k3_bin(box_fft, k1, k2, k3min, k3max, K_LOS_CUT, pspec, B, Ntri, k3);

            DataToBeReturned->BSData_k1[counter] = k1;
            DataToBeReturned->BSData_k2[counter] = k2;
            DataToBeReturned->BSData_k3[counter] = k3[0];
            DataToBeReturned->BSData_theta[counter] = theta_over_pie[i];
            //fprintf( stderr, "BinBispec::Compute_21cmBS_isoceles -> theta = %e\n", DataToBeReturned->BSData_theta[counter]);
            if (NORM==1)
              {
                //fprintf(stderr, "BinBispec::Compute_21cmBS_isoceles: calculating normalised bispectrum.\n");
                DataToBeReturned->BSData[counter] = B[0]/sqrt( pspec[0]*pspec[1]*pspec[2]/(k1*k2*k3[0]) );
                DataToBeReturned->BSData_imag[counter] = B[1];///sqrt( pspec[0]*pspec[1]*pspec[2]/(k1*k2*k3[0]) );///sqrt( pspec[3]*pspec[4]*pspec[5]/(k1*k2*k3[0]) );
              }
            else if (NORM==2) {
                //fprintf(stderr, "BinBispec::Compute_21cmBS_isoceles: calculating absolute bispectrum.\n");
                DataToBeReturned->BSData[counter] = sqrt(B[0]*B[0]); // Fairly non-standard, but the flip from positive to negative B(k) at large k is suppressed and this allows us to evaluate the impact on parameter estimation of this feature
                DataToBeReturned->BSData_imag[counter] = B[1];//sqrt(B[1]*B[1]);

            }
            else
              {
                //fprintf(stderr, "BinBispec::Compute_21cmBS_isoceles: calculating UNnormalised bispectrum.\n");
                DataToBeReturned->BSData[counter] = B[0];
                DataToBeReturned->BSData_imag[counter] = B[1];
              }
            DataToBeReturned->BSData_P1[counter] = pspec[0];
            DataToBeReturned->BSData_P2[counter] = pspec[1];
            DataToBeReturned->BSData_P3[counter] = pspec[2];

            DataToBeReturned->BSData_Ntri[counter] = Ntri[3];
            //fprintf( stderr, "Compute_21cmBS_isoceles -> counter = %d, NUM_BINS = %d \n", counter, NUM_BINS );
            counter+=1;
          }

      }
    //fprintf( stderr, "Compute_21cmBS_isoceles -> outside of loop 1" );
    DataToBeReturned->BSbins = counter;

    //fprintf( stderr, "Compute_21cmBS_isoceles -> clean up pointers etc \n" );

    // ------ CLEAN-UP -------
    fftwf_cleanup_threads();
    fftwf_cleanup();

    //fprintf(stderr, "Compute_21cmBS_isoceles: calling free_21cmBS_arrays()\n");
    free_21cmBS_arrays();
}

int nbins_isoceles(float K1)
  {
    int NUM_BINS, N_bins, i;
    double k_nyq, k_min;
    double theta, k3min, k3max;
    float k1, k2;
    float theta_over_pie[11]={0.01, 0.05, 0.1, 0.2, 0.33333333333333333333333, 0.4, 0.5, 0.6, 0.7, 0.85, 0.95};

    //init_21cmBS_arrays(DIM_X, DIM_Y, DIM_Z, L_X, L_Y, L_Z, num_th, bin_ct);

    N_bins=11;

    k_min = (KF+0.0);
    k_nyq = (K_NYQ+0.0);

    k1=K1;
    k2=K1;
    for (i=0; i<N_bins; i++)
      {
        theta = ( theta_over_pie[i] - (theta_over_pie[i]*0.025) )*M_PI; //cw - do we want to expand/reduce this bin at all? Currently 2.5% of a circle. Also, do we want to bin in a different co-ordinate system.
        k3min = k3_from_theta(k1, k2, theta);

        theta = ( theta_over_pie[i] + (theta_over_pie[i]*0.025) )*M_PI; //cw - do we want to expand/reduce this bin at all? Currently 2.5% of a circle. Also, do we want to bin in a different co-ordinate system.
        k3max = k3_from_theta(k1, k2, theta);
        //fprintf(stderr, "k3 min = %.4f,  k3 max = %.4f,  lower_lim = %.4f,  upper_lim = %.4f", k3min, k3max, 2.0*k_min, k_nyq);
        if ( ( (k3min - k_min)>=(1.25*k_min) ) && (k3max < k_nyq) )
          {
            NUM_BINS+=1;
          }
      }
    ///free_21cmBS_arrays();
    return NUM_BINS;
  }

//int bispec_k3_bin(fftwf_complex *BOX_FFT1, fftwf_complex *BOX_FFT2, fftwf_complex *BOX_FFT3, int OBS_SIM, double K1, double K2, double K3min, double K3max, double *PS, double *BS, double *N, double *k3_avg) //cw
int bispec_k3_bin(fftwf_complex *BOX_FFT1, double K1, double K2, double K3min, double K3max, double k_los_cut, double *PS, double *BS, double *N, double *k3_avg)
{
  /*
    Calculates the bispectrum with a bin on k3.

    INPUT VARIABLES:
    <BOX_FFT1> Fourier dataset (for auto bispec, pass all the same), NOTE: last axis should correspond to the LOS or frequency axis
    <K1>, <K2> are magnitudes of first two k-vectors
    <K3min>, <K3max> are the min and max on the magnitude of the third k-vector

    OUTPUT VARIABLES (these will be loaded and passed back with the outputs of module):
    <*PS> for the power spectrum for P(k1), P(k2), P(k3); allocate with (double *) malloc(sizeof(double)*3);
    <&BS> for the bispectrum; declare as e.g. double B (0.0)
    <*N> for number of modes or triangles in P1, P2, P3, B; allocate with (double *) malloc(sizeof(double)*4);
    <&k3_avg> for the avg value of k3 in bin; declare as e.g. double k3 (0.0)

  */
  unsigned int k3_ct, n_x, n_y, n_z, x, y, z, i, k;

  double nx, ny, nz;
  double nxp1, nyp1, nzp1;
  double kpix_mag;//, indexk;
  double px_min, px_max;
  double P, bug_flag;
  unsigned long long indx;

  //cw std::vector<double> pix_cnrs(8);
  double pix_cnrs[8], sum_I[4], sum_deltan[4];
  fftwf_complex complex_bi, complex_P1, complex_P2, complex_P3;
  fftwf_plan plan;

  // ------- INITIALISATION --------
  k3_avg[0] = 0.0;
  k3_ct = 0.0;
  bug_flag = 0;

  //fprintf(stderr, "Compute_21cmBS_isoceles: In bispec_k3_bin() - initialising summation stores.\n");
  for (i=0; i<4; ++i)
      {
	       sum_I[i] = 0.0;
	       sum_deltan[i] = 0.0;
      }
  complex_bi = 0.0 + I*0.0;
  complex_P1 = 0.0 + I*0.0;
  complex_P2 = 0.0 + I*0.0;
  complex_P3 = 0.0 + I*0.0;

  unsigned int midd_ind = (((double) Z_DIM)/2.0);

  //fprintf(stderr, "Compute_21cmBS_isoceles: In bispec_k3_bin() - entering loop over pixels.\n");
  // ------- MAIN CALCULATION --------
  // For k1 and k2, we have to work out the max and min corners of our pixel +/- 1 pixel
  for  (n_x=0; n_x<X_DIM; n_x++)
    {
      for  (n_y=0; n_y<Y_DIM; n_y++)
	     {
	        for  (n_z=0; n_z<Z_DIM; n_z++) // midd_ind because fftw only stores half the volume
	         {
	            // adjust n_x and n_y to allow for the fact that fftw
	            // stores negative k for n_x between midd_ind and res_
	            if (n_x>X_MID)
		            nx = ((double) n_x-X_DIM);
	            else
		            nx = ((double) n_x);
	            if (n_y>Y_MID)
		            ny = ((double) n_y-Y_DIM);
	            else
		            ny = ((double) n_y);
              if (n_z>Z_MID)
  		          nz = ((double) n_z-Z_DIM);
  	          else
  		          nz = ((double) n_z);

              kpix_mag = sqrt( nx*nx*pow(KF_X,2) + ny*ny*pow(KF_Y,2)  + nz*nz*pow(KF_Z,2) );

              //To make the binning match the original method, want to place a virtual pixel around this co-ordinates
              // Create a new origin and then work out the magnitudes associated with each corner
              nx-=1.051;
              ny-=1.051;
              nz-=1.051;

              // Convert into k rather than dimensionless pixel co-ordinates
              nxp1 = (nx+2.051)*(KF_X+0.0);
              nyp1 = (ny+2.051)*(KF_Y+0.0);
              nzp1 = (nz+2.051)*(KF_Z+0.0);

              nx*=(KF_X+0.0);
              ny*=(KF_Y+0.0);
              nz*=(KF_Z+0.0);

              pix_cnrs[0] = sqrt( pow(nx, 2) + pow(ny, 2) + pow(nz, 2) );
              pix_cnrs[1] = sqrt( pow(nxp1, 2) + pow(ny, 2) + pow(nz, 2) );
              pix_cnrs[2] = sqrt( pow(nx, 2) + pow(nyp1, 2) + pow(nz, 2) );
              pix_cnrs[3] = sqrt( pow(nx, 2) + pow(ny, 2) + pow(nzp1, 2) );
              pix_cnrs[4] = sqrt( pow(nxp1, 2) + pow(nyp1, 2) + pow(nz, 2) );
              pix_cnrs[5] = sqrt( pow(nxp1, 2) + pow(ny, 2) + pow(nzp1, 2) );
              pix_cnrs[6] = sqrt( pow(nx, 2) + pow(nyp1, 2) + pow(nzp1, 2) );
              pix_cnrs[7] = sqrt( pow(nxp1, 2) + pow(nyp1, 2) + pow(nzp1, 2) );

              // Work out the maximum and minimum magnitude associated with the pixel
              px_min = 1.0e300;
              px_max = -1.0e300;
              for (i = 0; i < 8; i++)
                {
                  if (pix_cnrs[i] < px_min)
                    px_min = ((double) pix_cnrs[i]);
                  if (pix_cnrs[i] > px_max)
                    px_max = ((double) pix_cnrs[i]);
                }

              // These expresions exclude unsampled pixels: && ( cabs(creal(BOX_FFT[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 ) && ( cabs(cimag(BOX_FFT[complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM)])) > 0.00 )
              // Also only fills pixels for which nz > k_los_cut (which we use to exclude small los modes that always get removed with the FG's)
              indx = complex_index(n_x, n_y, n_z, Y_DIM, Z_DIM);
              if (  ( (px_min<=K1)&&(K1<=px_max) ) && (fabs(nz) >= k_los_cut) && (fabs(nzp1) >= k_los_cut)  )
  		           {
  		             //load the delta_k values into delta_fft associated with
  		             delta_fft1[indx]  = BOX_FFT1[indx];
  		             I_fft1[indx]  = 1.0 + I*0.0;
  		           }
  	           else
  		           {
  		             delta_fft1[indx] = 0.0 + I*0.0;
  		             I_fft1[indx]  = 0.0 + I*0.0;
  		           }
               if (  ( (px_min<=K2)&&(K2<=px_max) ) && (fabs(nz) >= k_los_cut) && (fabs(nzp1) >= k_los_cut)  )
  		           {
  		             //load the delta_k values into delta_fft associated with
  		             //delta_fft2[indx]  = BOX_FFT2[indx]; // cw
                   delta_fft2[indx]  = BOX_FFT1[indx];
  		             I_fft2[indx]  = 1.0 + I*0.0;
  		           }
                else
   		            {
   		              delta_fft2[indx]  = 0.0 + I*0.0;
   		              I_fft2[indx]  = 0.0 + I*0.0;
   		            }

                if (  ( ((px_max>=K3max) &&  (px_min<=K3max)) ||  ((px_max<=K3max) && (px_min>=K3min)) || ((px_max>=K3min) &&  (px_min<=K3min)) ) && (fabs(nz) >= k_los_cut) && (fabs(nzp1) >= k_los_cut)  )
                  {
                    delta_fft3[indx]  = BOX_FFT1[indx]; //cw
                    I_fft3[indx]  = 1.0 + I*0.0;
                    k3_avg[0]+=kpix_mag;
                    k3_ct+=1;
                  }
                else
                  {
                    delta_fft3[indx]  = 0.0 + I*0.0;
                    I_fft3[indx]  = 0.0 + I*0.0;
                  }

            } // z-loop end
        } // y-loop end
    } // x-loop end

  k3_avg[0] /= k3_ct;

  // Set fftw threading parameters
  if (fftwf_init_threads()==0){
    fprintf(stderr, "BinBispec::bispec_k3_bin -> ERROR: problem initializing fftwf threads\nAborting\n.");
    return -1;
  }
  //fprintf(stderr, "Compute_21cmBS_isoceles: In bispec_k3_bin() - Fourier Transforming c2r (full DFT still though).\n");
  omp_set_num_threads(NUM_TH);
  //omp_set_dynamic(false);
  fftwf_plan_with_nthreads(NUM_TH);//*/

  //FFT complex to real delta_fft //cw fftwf_plan_dft_c2r_3d -> fftwf_plan_dft_3d and added FFTW_BACKWARD flag
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft1, (fftwf_complex *)delta_n1, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/
  fftwf_cleanup();
  fftwf_cleanup_threads();

  //FFT complex to real I_fft
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft1, (fftwf_complex *)I_n1, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/
  fftwf_cleanup();
  fftwf_cleanup_threads();

  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft2, (fftwf_complex *)delta_n2, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/
  fftwf_cleanup();
  fftwf_cleanup_threads();

  //FFT complex to real I_fft
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft2, (fftwf_complex *)I_n2, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/
  fftwf_cleanup();
  fftwf_cleanup_threads();

  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)delta_fft3, (fftwf_complex *)delta_n3, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/
  fftwf_cleanup();
  fftwf_cleanup_threads();

  //FFT complex to real I_fft
  plan = fftwf_plan_dft_3d(X_DIM, Y_DIM, Z_DIM, (fftwf_complex *)I_fft3, (fftwf_complex *)I_n3, FFTW_BACKWARD, FFTW_ESTIMATE); //no normalisation as taken care of in definition of estimator
  fftwf_execute(plan);
  fftwf_destroy_plan(plan);//*/
  fftwf_cleanup();

  //fprintf(stderr, "Compute_21cmBS_isoceles: In bispec_k3_bin() - cleaning up threads.\n");
  fftwf_cleanup_threads();

  //fprintf(stderr, "Compute_21cmBS_isoceles: In bispec_k3_bin() - summing over products to get power spectrum and bispectrum.\n");
  // CALCULATE SUMS IN EQN 5.23 AND 5.28 (WATKINSON 2015 THESIS)
  // 1. Make sum over delta_nr and the mode count.
  int ind;
  for (x=0; x<X_DIM; x++)
    {
      for (y=0; y<Y_DIM; y++)
        {
          for (z=0; z<Z_DIM; z++)
            {

              ind = real_index(x, y, z, Y_DIM, Z_DIM);

	            //sum_deltan[0] += ( delta_n1[ind]*delta_n1[ind] );
              complex_P1 += ( delta_n1[ind]*delta_n1[ind] );
	            sum_I[0] += ( I_n1[ind]*I_n1[ind] );

	            //sum_deltan[1] += ( delta_n2[ind]*delta_n2[ind] );
              complex_P2 += ( delta_n2[ind]*delta_n2[ind] );
	            sum_I[1] += ( I_n2[ind]*I_n2[ind] );

	            //sum_deltan[2] += ( delta_n3[ind]*delta_n3[ind] );
              complex_P3 += ( delta_n3[ind]*delta_n3[ind] );
	            sum_I[2] += ( I_n3[ind]*I_n3[ind] );

	            //sum_deltan[3] += ( delta_n1[ind]*delta_n2[ind]*delta_n3[ind] );
              complex_bi += ( delta_n1[ind]*delta_n2[ind]*delta_n3[ind] );
	            sum_I[3] += ( I_n1[ind]*I_n2[ind]*I_n3[ind] );

	            if (!((delta_n1[ind]==delta_n2[ind])&&(delta_n3[ind]==delta_n2[ind])&&(delta_n1[ind]==delta_n3[ind])))
	             {
	                bug_flag+=1;
	                //std::cout<<"ERROR: As we are TESTING the equilateral configuration these should all be the same, but are not, values are: \t"<<j<<"\t"<<delta_n1[j]<<"\t"<<delta_n2[j]<<"\t"<<delta_n3[j]<<std::endl;
	             }
            }
        }
    }
  //fprintf( stderr, "BinBispec::bispec_k3_bin -> %e, delta1*delta2*delta3 = %f %+fi\n.", sum_deltan[3], creal(complex_bi), cimag(complex_bi) );
    //std::cout<<"The delta's have been different n times, where n = "<<bug_flag<<std::endl;

  //fprintf(stderr, "Compute_21cmBS_isoceles: In bispec_k3_bin() - performing normalisations of pspec and bspec.\n");
  // MAKE NORMALISATIONS FOR SUMMATIONS AND CALCULATE 5.27 (WATKINSON 2015 THESIS)
  // 2a. perform normalisations for power spectra
  for (k=0; k<3; ++k){
      N[k] = (crealf(sum_I[k])/((double) TOT_NUM_PIXELS));
  }
  P = VOLUME/((double) TOT_NUM_PIXELS); //pow( ((double) TOT_NUM_PIXELS), 3.0 ); // Fourier norm.
  //P /= (sum_I[k]/((double) TOT_NUM_PIXELS)); //number of modes. Alternatively: //P1 /= ( 4.0*PI*BINFAC*pow( (n1_+0.0),2 ) ); //analytical est. of mode num. (integral approx)

  // Factor in power
  PS[0] = crealf(complex_P1)*P/N[0]; //( crealf(sum_I[0])/((double) TOT_NUM_PIXELS) ); //sum_deltan[0]
  PS[3] = cimagf(complex_P1)*P/N[0]; //( crealf(sum_I[0])/((double) TOT_NUM_PIXELS) );

  PS[1] = crealf(complex_P2)*P/N[1]; //( crealf(sum_I[1])/((double) TOT_NUM_PIXELS) ); //sum_deltan[1]
  PS[4] = cimagf(complex_P2)*P/N[1]; //( crealf(sum_I[1])/((double) TOT_NUM_PIXELS) );

  PS[2] = crealf(complex_P3)*P/N[2]; //( crealf(sum_I[2])/((double) TOT_NUM_PIXELS) ); //sum_deltan[2]
  PS[5] = cimagf(complex_P3)*P/N[2]; //( crealf(sum_I[2])/((double) TOT_NUM_PIXELS) );

      // Factor in power for real part
      //P *= sum_deltan[k];
      //PS[k] = P;
    //}
  // MAKE NORMALISATIONS FOR SUMMATIONS AND CALCULATE 5.28 (WATKINSON 2015 THESIS)
  // 2b. perform Fourier convention normalisations for bi-spectrum
  N[3] = ( crealf(sum_I[3])/((double) TOT_NUM_PIXELS) );

  BS[0] = pow(VOLUME, 2)/((double) TOT_NUM_PIXELS); // /pow( ((double) TOT_NUM_PIXELS),4.0 ); // Fourier norm + 1/Npix from delta function, that cancels in the paper equations
  BS[1] = BS[0]/(sum_I[3]/((double) TOT_NUM_PIXELS))*cimagf(complex_bi); // Imaginary part which should be zero or at least small for the noise

  BS[0] /= (crealf(sum_I[3])/((double) TOT_NUM_PIXELS)); //number of triangles. Alternatively: //P1 /= ( 8.0*pow(PI,2)*pow(BINFAC,3)*n1_*n2_*n3_ ); //analytical est. of tri. num. (approx)
  BS[0] *= crealf(complex_bi); //sum_deltan[3]; // Factor in power (sumover delta_1*delta_2*delta_3

  return 1;
}

double k3_from_theta(double a, double b, double theta)
{
  /*
    Calculates length of 3rd side for a triangle formed
    of sides a, b and c where a and b are separated by an angle
    of theta (in radians). 0<theta<pi
   */

   /*if ( (theta<=0.0)||(theta>=M_PI) )
    {
      fprintf(stderr,"****** ERROR in k3_from_theta !! *******\n The value of theta is outside of the physically restricted range 0<theta<PI/n Aborting........\n");
      return -1;
    }*/
  double c, c2;
  c2 = pow(a,2.0) + pow(b,2.0) - 2.0*b*a*cos(theta);
  c = sqrt(c2);
  return c;
}

unsigned long long real_index(int x, int y, int z, int res_y, int res_z)
  {
    // Returns the 1D index storage location of a 3D indexed box element: Unpadded real box.
    return ( (z) + res_z*((y) + res_y*x) );
  }

unsigned long long complex_index(int x, int y, int z, int res_y, int res_z)
  {
    // returns 1D index for a 3D complex array
    /*int MID;
    MID = ((res_z+0.0)/2.0);
    return ( (z)+(MID+1llu)*((y)+res_y*(x)) );*/ //cw adjusted as now we are using DFT's so no longer work in half plane only
    return ( (z) + res_z*((y) + res_y*x) );
  }

void free_21cmBS_arrays()
  {
    fftwf_cleanup_threads();
    fftwf_cleanup();
    free(pspec);
    free(Ntri);
    free(B);
    free(k3);
    fftwf_free(box_fft);
    fftwf_free(delta_fft1);
    fftwf_free(I_fft1);
    fftwf_free(delta_fft2);
    fftwf_free(I_fft2);
    fftwf_free(delta_fft3);
    fftwf_free(I_fft3);
    fftwf_free(delta_n1);
    fftwf_free(I_n1);
    fftwf_free(delta_n2);
    fftwf_free(I_n2);
    fftwf_free(delta_n3);
    fftwf_free(I_n3);
  }
