'''
Created on 24 April 2020
@author: Catherine Allin Watkinson
Setup script
'''

#from setuptools import setup, find_packages
from distutils.core import setup


setup(name='bifft',
      version='1.0',
      author='Catherine Allin Watkinson',
      author_email='catherine.watkinson@gmail.com',
      package_dir = {'bifft' : 'src'},
      packages=['bifft'],
      package_data={'share':['*'],},
      install_requires=['numpy','scipy'],
      #include_package_data=True,
)
